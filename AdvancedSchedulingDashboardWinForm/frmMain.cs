﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
//using AdvSchedDashWinForm;

namespace AdvSchedDashWinForm
{
    public partial class frmMain : Form
    {
        MainBO objMain = new MainBO();
        public int CurrentTabIdx { get; set; }

        public frmMain()
        {
            InitializeComponent();

#if DEBUG
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database. " + ex);
            }
#endif       
            this.Location = new Point(0, 0);
            PopulateAllScreens(true);

            Timer MyTimer = new Timer();
            MyTimer.Interval = (1 * 60 * 60000); //  60 mins (Every 60 minutes this timer will refresh the screen.
            MyTimer.Tick += new EventHandler(MyTimer_Tick);
            MyTimer.Start();
        }

        #region Events
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tabIdx = tabControlMain.SelectedIndex;

            switch (tabIdx)
            {
                case 0:
                    lbArea.Text = "Heat Pump Units";                    
                    break;
                case 1:
                    lbArea.Text = "Aux Box Units";                   
                    break;
                case 2:
                    lbArea.Text = "Poly Iso";                   
                    break;
                case 3:
                    lbArea.Text = "Special Units";                    
                    break;
                default:
                    break;
            }

            CurrentTabIdx = tabIdx;
        }
        #endregion

        #region Special Events
        private void dgvSpecial_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvSpecial.CurrentRow.Index;

            displaySpecialSelectedValues(rowIdx);
        }
        #endregion

        #region AuxBox Events
        private void dgvAuxBox_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvAuxBox.CurrentRow.Index;

            displayAuxBoxSelectedValues(rowIdx);
        }
        private void dgvAuxBox_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string startDateStr = "";
            string currntJobNum = "";            

            Color currentRowColor = Color.DarkCyan;
            Color nextRowColor = Color.Teal;
            Color prevRowColor = Color.DarkCyan;

            DateTime startDateDT = DateTime.Today;
            DateTime lookAheadDateDT = startDateDT.AddDays(7);           

            foreach (DataGridViewRow row in dgvAuxBox.Rows)
            {
                startDateStr = (row.Cells["OrigStartDate"].Value ?? string.Empty).ToString();
                startDateDT = Convert.ToDateTime(startDateStr);

                if (row.Cells["JobNum"].Value.ToString() != currntJobNum)
                {
                    currntJobNum = row.Cells["JobNum"].Value.ToString();
                    prevRowColor = currentRowColor;
                    currentRowColor = nextRowColor;
                    nextRowColor = prevRowColor;
                }

                if (startDateDT >= DateTime.Today && startDateDT <= lookAheadDateDT) // If start date is in the 3 day look ahead window then paint row
                {
                    colorCodeCells(row, currentRowColor, Color.White);                    
                }
            }           
        }

        private void btnAuxBoxDispCritCompRpt_Click(object sender, EventArgs e)
        {
            displayCriticalCompRpt(lbAuxBoxJobNum.Text, lbAuxBoxModelNo1.Text, lbAuxBoxCustName.Text);
        }
        #endregion

        #region PolyIso Events
        private void dgvPolyIso_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvPolyIso.CurrentRow.Index;

            displayPolyIsoSelectedValues(rowIdx);
        }

        private void dgvPolyIso_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string startDateStr = "";
            string currntJobNum = "";
            string tempQty = "";

            decimal polyIso = 0;
            decimal polyIsoA = 0;
            

            Color currentRowColor = Color.DimGray;
            Color nextRowColor = Color.Gray;
            Color prevRowColor = Color.DimGray;

            DateTime startDateDT = DateTime.Today;
            DateTime lookAheadDateDT = startDateDT.AddDays(3);

            if (startDateDT.DayOfWeek == DayOfWeek.Thursday || startDateDT.DayOfWeek == DayOfWeek.Friday)
            {
                lookAheadDateDT = startDateDT.AddDays(5);
            }
           
            foreach (DataGridViewRow row in dgvPolyIso.Rows)
            {
                startDateStr = (row.Cells["StartDate"].Value ?? string.Empty).ToString();
                startDateDT = Convert.ToDateTime(startDateStr);
            
                if (row.Cells["JobNum"].Value.ToString() != currntJobNum)
                {
                    currntJobNum = row.Cells["JobNum"].Value.ToString();
                    prevRowColor = currentRowColor;
                    currentRowColor = nextRowColor;
                    nextRowColor = prevRowColor;
                }             

                if (startDateDT >= DateTime.Today && startDateDT <= lookAheadDateDT) // If start date is in the 3 day look ahead window then paint row
                {
                    colorCodeCells(row, currentRowColor, Color.White);

                    if (row.Cells["PartNum"].Value.ToString() == "INS2.00X2.10LBS-PIC")
                    {
                        tempQty = row.Cells["RequiredQty"].Value.ToString();
                        polyIso += decimal.Parse(tempQty);
                    }
                    else if (row.Cells["PartNum"].Value.ToString() == "INS2.00X2.10LBS-PIC-A")
                    {
                        tempQty = row.Cells["RequiredQty"].Value.ToString();
                        polyIsoA += decimal.Parse(tempQty);
                    }
                }
            }
            this.lbPolyIsoTotal.Text = (polyIso / 28).ToString("N2") + " Sheets";
            this.lbPolyIsoATotal.Text = (polyIso / 40).ToString("N2") + " Sheets";

            string curJobNum = "";

            foreach (DataGridViewRow dr in dgvPolyIso.Rows)
            {
                if (curJobNum != dr.Cells["JobNum"].Value.ToString())
                {
                    curJobNum = dr.Cells["JobNum"].Value.ToString();
                }
                else
                {
                    dgvPolyIso.Rows.Remove(dr);
                }                   
            }
        }
        #endregion

        #region HeatPump Events
        private void dgvHeatPump_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvHeatPump.CurrentRow.Index;

            displayHeatPumpSelectedValues(rowIdx, false);  // Pass row index of grid and false indicating that this is not the initial display.

            DataTable dt = objMain.GetHeatPumpJobs();
            dgvHeatPump.DataSource = dt;
            dgvHeatPump.Refresh();
        }

        private void dgvHeatPump_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string startDateStr = "";
            string currntJobNum = "";           

            Color currentRowColor = Color.DimGray;
            Color nextRowColor = Color.Gray;
            Color prevRowColor = Color.DimGray;

            DateTime startDateDT = DateTime.Today;
            DateTime lookAheadDateDT = startDateDT.AddDays(35);

            foreach (DataGridViewRow row in dgvHeatPump.Rows)
            {
                startDateStr = (row.Cells["OrigStartDate"].Value ?? string.Empty).ToString();
                startDateDT = Convert.ToDateTime(startDateStr);

                if (row.Cells["JobNum"].Value.ToString() != currntJobNum)
                {
                    currntJobNum = row.Cells["JobNum"].Value.ToString();
                    prevRowColor = currentRowColor;
                    currentRowColor = nextRowColor;
                    nextRowColor = prevRowColor;
                }

                if (startDateDT >= DateTime.Today && startDateDT <= lookAheadDateDT) // If start date is in the 3 day look ahead window then paint row
                {
                    colorCodeCells(row, currentRowColor, Color.White);
                }
            }
        }

        private void btnDisplayRfgCompRpt_Click(object sender, EventArgs e)
        {
            displayRfgCompRpt(lbHeatPumpJobNum.Text, lbHeatPumpModelNo1.Text, lbHeatPumpModelNo2.Text);            
        }
        #endregion

        #region SubAssembly Events
        private void dgvSubAsmSchd_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int asmStatus = 0;
            int rowIdx = dgvSubAsmSchd.CurrentRow.Index;            

            string currenStatus = "";

            bool jobFound = false;

            frmSubAsmStatus frmStat = new frmSubAsmStatus();

            objMain.JobNum = dgvSubAsmSchd.Rows[rowIdx].Cells["JobNum"].Value.ToString();

            frmStat.lbJobNum.Text = objMain.JobNum;
            frmStat.lbModelNo.Text = dgvSubAsmSchd.Rows[rowIdx].Cells["ModelNo"].Value.ToString();
            frmStat.lbCustName.Text = dgvSubAsmSchd.Rows[rowIdx].Cells["CustomerName"].Value.ToString();

            DataTable dt = objMain.GetSubAssemblyStatusByJob();
            DataRow dr = null;
            if (dt.Rows.Count > 0)
            {
                jobFound = true;
                dr = dt.Rows[0];
                frmStat.rtbOtherComments.Text = dr["Other"].ToString();
            }

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["CoolingWalls"];
                objMain.SubAssemblyName = "CoolingWalls";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
                
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnCoolingWallsStart.Enabled = true;
                frmStat.btnCoolingWallsStart.BackColor = Color.Lime;
                frmStat.btnCoolingWallsPause.Enabled = false;
                frmStat.btnCoolingWallsPause.BackColor = Color.Gainsboro;
                frmStat.btnCoolingWallsComplete.Enabled = false;
                frmStat.btnCoolingWallsComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbCoolingWallsCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnCoolingWallsStart.Enabled = false;
                frmStat.btnCoolingWallsStart.BackColor = Color.Gainsboro;
                frmStat.btnCoolingWallsPause.Enabled = true;
                frmStat.btnCoolingWallsPause.BackColor = Color.Yellow;
                frmStat.btnCoolingWallsComplete.Enabled = true;
                frmStat.btnCoolingWallsComplete.BackColor = Color.Red;
                frmStat.lbCoolingWallsCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnCoolingWallsStart.Enabled = false;
                frmStat.btnCoolingWallsStart.BackColor = Color.Gainsboro;
                frmStat.btnCoolingWallsPause.Enabled = false;
                frmStat.btnCoolingWallsPause.BackColor = Color.Gainsboro;
                frmStat.btnCoolingWallsComplete.Enabled = false;
                frmStat.btnCoolingWallsComplete.BackColor = Color.Gainsboro;
                frmStat.lbCoolingWallsCurStatus.BackColor = Color.Red;
            }
            frmStat.lbCoolingWallsCurStatus.Text  = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["Heater"];
                objMain.SubAssemblyName = "Heater";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnHeaterStart.Enabled = true;
                frmStat.btnHeaterStart.BackColor = Color.Lime;
                frmStat.btnHeaterPause.Enabled = false;
                frmStat.btnHeaterPause.BackColor = Color.Gainsboro;
                frmStat.btnHeaterComplete.Enabled = false;
                frmStat.btnHeaterComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbHeaterCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnHeaterStart.Enabled = false;
                frmStat.btnHeaterStart.BackColor = Color.Gainsboro;
                frmStat.btnHeaterPause.Enabled = true;
                frmStat.btnHeaterPause.BackColor = Color.Yellow;
                frmStat.btnHeaterComplete.Enabled = true;
                frmStat.btnHeaterComplete.BackColor = Color.Red;
                frmStat.lbHeaterCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnHeaterStart.Enabled = false;
                frmStat.btnHeaterStart.BackColor = Color.Gainsboro;
                frmStat.btnHeaterPause.Enabled = false;
                frmStat.btnHeaterPause.BackColor = Color.Gainsboro;
                frmStat.btnHeaterComplete.Enabled = false;
                frmStat.btnHeaterComplete.BackColor = Color.Gainsboro;
                frmStat.lbHeaterCurStatus.BackColor = Color.Red;
            }
            frmStat.lbHeaterCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["DoorPanels"];
                objMain.SubAssemblyName = "DoorPanels";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnDoorPanelStart.Enabled = true;
                frmStat.btnDoorPanelStart.BackColor = Color.Lime;
                frmStat.btnDoorPanelPause.Enabled = false;
                frmStat.btnDoorPanelPause.BackColor = Color.Gainsboro;
                frmStat.btnDoorPanelComplete.Enabled = false;
                frmStat.btnDoorPanelComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbDoorPanelsCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnDoorPanelStart.Enabled = false;
                frmStat.btnDoorPanelStart.BackColor = Color.Gainsboro;
                frmStat.btnDoorPanelPause.Enabled = true;
                frmStat.btnDoorPanelPause.BackColor = Color.Yellow;
                frmStat.btnDoorPanelComplete.Enabled = true;
                frmStat.btnDoorPanelComplete.BackColor = Color.Red;
                frmStat.lbDoorPanelsCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnDoorPanelStart.Enabled = false;
                frmStat.btnDoorPanelStart.BackColor = Color.Gainsboro;
                frmStat.btnDoorPanelPause.Enabled = false;
                frmStat.btnDoorPanelPause.BackColor = Color.Gainsboro;
                frmStat.btnDoorPanelComplete.Enabled = false;
                frmStat.btnDoorPanelComplete.BackColor = Color.Gainsboro;
                frmStat.lbDoorPanelsCurStatus.BackColor = Color.Red;
            }
            frmStat.lbDoorPanelsCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["AuxBox"];
                objMain.SubAssemblyName = "AuxBox";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnAuxBoxStart.Enabled = true;
                frmStat.btnAuxBoxStart.BackColor = Color.Lime;
                frmStat.btnAuxBoxPause.Enabled = false;
                frmStat.btnAuxBoxPause.BackColor = Color.Gainsboro;
                frmStat.btnAuxBoxComplete.Enabled = false;
                frmStat.btnAuxBoxComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbAuxBoxCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnAuxBoxStart.Enabled = false;
                frmStat.btnAuxBoxStart.BackColor = Color.Gainsboro;
                frmStat.btnAuxBoxPause.Enabled = true;
                frmStat.btnAuxBoxPause.BackColor = Color.Yellow;
                frmStat.btnAuxBoxComplete.Enabled = true;
                frmStat.btnAuxBoxComplete.BackColor = Color.Red;
                frmStat.lbAuxBoxCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnAuxBoxStart.Enabled = false;
                frmStat.btnAuxBoxStart.BackColor = Color.Gainsboro;
                frmStat.btnAuxBoxPause.Enabled = false;
                frmStat.btnAuxBoxPause.BackColor = Color.Gainsboro;
                frmStat.btnAuxBoxComplete.Enabled = false;
                frmStat.btnAuxBoxComplete.BackColor = Color.Gainsboro;
                frmStat.lbAuxBoxCurStatus.BackColor = Color.Red;
            }
            frmStat.lbAuxBoxCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["Intake"];
                objMain.SubAssemblyName = "Intake";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnIntakeStart.Enabled = true;
                frmStat.btnIntakeStart.BackColor = Color.Lime;
                frmStat.btnIntakePause.Enabled = false;
                frmStat.btnIntakePause.BackColor = Color.Gainsboro;
                frmStat.btnIntakeComplete.Enabled = false;
                frmStat.btnIntakeComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbIntakeCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnIntakeStart.Enabled = false;
                frmStat.btnIntakeStart.BackColor = Color.Gainsboro;
                frmStat.btnIntakePause.Enabled = true;
                frmStat.btnIntakePause.BackColor = Color.Yellow;
                frmStat.btnIntakeComplete.Enabled = true;
                frmStat.btnIntakeComplete.BackColor = Color.Red;
                frmStat.lbIntakeCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnIntakeStart.Enabled = false;
                frmStat.btnIntakeStart.BackColor = Color.Gainsboro;
                frmStat.btnIntakePause.Enabled = false;
                frmStat.btnIntakePause.BackColor = Color.Gainsboro;
                frmStat.btnIntakeComplete.Enabled = false;
                frmStat.btnIntakeComplete.BackColor = Color.Gainsboro;
                frmStat.lbIntakeCurStatus.BackColor = Color.Red;
            }
            frmStat.lbIntakeCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["Breaker"];
                objMain.SubAssemblyName = "Breaker";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnBreakerStart.Enabled = true;
                frmStat.btnBreakerStart.BackColor = Color.Lime;
                frmStat.btnBreakerPause.Enabled = false;
                frmStat.btnBreakerPause.BackColor = Color.Gainsboro;
                frmStat.btnBreakerComplete.Enabled = false;
                frmStat.btnBreakerComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbBreakerCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnBreakerStart.Enabled = false;
                frmStat.btnBreakerStart.BackColor = Color.Gainsboro;
                frmStat.btnBreakerPause.Enabled = true;
                frmStat.btnBreakerPause.BackColor = Color.Yellow;
                frmStat.btnBreakerComplete.Enabled = true;
                frmStat.btnBreakerComplete.BackColor = Color.Red;
                frmStat.lbBreakerCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnBreakerStart.Enabled = false;
                frmStat.btnBreakerStart.BackColor = Color.Gainsboro;
                frmStat.btnBreakerPause.Enabled = false;
                frmStat.btnBreakerPause.BackColor = Color.Gainsboro;
                frmStat.btnBreakerComplete.Enabled = false;
                frmStat.btnBreakerComplete.BackColor = Color.Gainsboro;
                frmStat.lbBreakerCurStatus.BackColor = Color.Red;
            }
            frmStat.lbBreakerCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["Transformer"];
                objMain.SubAssemblyName = "Transformer";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnTransStart.Enabled = true;
                frmStat.btnTransStart.BackColor = Color.Lime;
                frmStat.btnTransPause.Enabled = false;
                frmStat.btnTransPause.BackColor = Color.Gainsboro;
                frmStat.btnTransComplete.Enabled = false;
                frmStat.btnTransComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbTransformerCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnTransStart.Enabled = false;
                frmStat.btnTransStart.BackColor = Color.Gainsboro;
                frmStat.btnTransPause.Enabled = true;
                frmStat.btnTransPause.BackColor = Color.Yellow;
                frmStat.btnTransComplete.Enabled = true;
                frmStat.btnTransComplete.BackColor = Color.Red;
                frmStat.lbTransformerCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnTransStart.Enabled = false;
                frmStat.btnTransStart.BackColor = Color.Gainsboro;
                frmStat.btnTransPause.Enabled = false;
                frmStat.btnTransPause.BackColor = Color.Gainsboro;
                frmStat.btnTransComplete.Enabled = false;
                frmStat.btnTransComplete.BackColor = Color.Gainsboro;
                frmStat.lbTransformerCurStatus.BackColor = Color.Red;
            }
            frmStat.lbTransformerCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["IndoorFanMotor"];
                objMain.SubAssemblyName = "IndoorFanMotor";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnInFanMtrStart.Enabled = true;
                frmStat.btnInFanMtrStart.BackColor = Color.Lime;
                frmStat.btnInFanMtrPause.Enabled = false;
                frmStat.btnInFanMtrPause.BackColor = Color.Gainsboro;
                frmStat.btnInFanMtrComplete.Enabled = false;
                frmStat.btnInFanMtrComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbIndoorFanMtrCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnInFanMtrStart.Enabled = false;
                frmStat.btnInFanMtrStart.BackColor = Color.Gainsboro;
                frmStat.btnInFanMtrPause.Enabled = true;
                frmStat.btnInFanMtrPause.BackColor = Color.Yellow;
                frmStat.btnInFanMtrComplete.Enabled = true;
                frmStat.btnInFanMtrComplete.BackColor = Color.Red;
                frmStat.lbIndoorFanMtrCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnInFanMtrStart.Enabled = false;
                frmStat.btnInFanMtrStart.BackColor = Color.Gainsboro;
                frmStat.btnInFanMtrPause.Enabled = false;
                frmStat.btnInFanMtrPause.BackColor = Color.Gainsboro;
                frmStat.btnInFanMtrComplete.Enabled = false;
                frmStat.btnInFanMtrComplete.BackColor = Color.Gainsboro;
                frmStat.lbIndoorFanMtrCurStatus.BackColor = Color.Red;
            }
            frmStat.lbIndoorFanMtrCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["PoweredExhaustMotor"];
                objMain.SubAssemblyName = "PoweredExhaustMotor";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;               
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnPwrExMotorStart.Enabled = true;
                frmStat.btnPwrExMotorStart.BackColor = Color.Lime;
                frmStat.btnPwrExMotorPause.Enabled = false;
                frmStat.btnPwrExMotorPause.BackColor = Color.Gainsboro;
                frmStat.btnPwrExMotorComplete.Enabled = false;
                frmStat.btnPwrExMotorComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbPwrExMotorCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnPwrExMotorStart.Enabled = false;
                frmStat.btnPwrExMotorStart.BackColor = Color.Gainsboro;
                frmStat.btnPwrExMotorPause.Enabled = true;
                frmStat.btnPwrExMotorPause.BackColor = Color.Yellow;
                frmStat.btnPwrExMotorComplete.Enabled = true;
                frmStat.btnPwrExMotorComplete.BackColor = Color.Red;
                frmStat.lbPwrExMotorCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnPwrExMotorStart.Enabled = false;
                frmStat.btnPwrExMotorStart.BackColor = Color.Gainsboro;
                frmStat.btnPwrExMotorPause.Enabled = false;
                frmStat.btnPwrExMotorPause.BackColor = Color.Gainsboro;
                frmStat.btnPwrExMotorComplete.Enabled = false;
                frmStat.btnPwrExMotorComplete.BackColor = Color.Gainsboro;
                frmStat.lbPwrExMotorCurStatus.BackColor = Color.Red;
            }
            frmStat.lbPwrExMotorCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["CondenserFanMotor"];
                objMain.SubAssemblyName = "CondenserFanMotor";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;                
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnCondFanMotorStart.Enabled = true;
                frmStat.btnCondFanMotorStart.BackColor = Color.Lime;
                frmStat.btnCondFanMotorPause.Enabled = false;
                frmStat.btnCondFanMotorPause.BackColor = Color.Gainsboro;
                frmStat.btnCondFanMotorComplete.Enabled = false;
                frmStat.btnCondFanMotorComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbCondFanMotorCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnCondFanMotorStart.Enabled = false;
                frmStat.btnCondFanMotorStart.BackColor = Color.Gainsboro;
                frmStat.btnCondFanMotorPause.Enabled = true;
                frmStat.btnCondFanMotorPause.BackColor = Color.Yellow;
                frmStat.btnCondFanMotorComplete.Enabled = true;
                frmStat.btnCondFanMotorComplete.BackColor = Color.Red;
                frmStat.lbCondFanMotorCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnCondFanMotorStart.Enabled = false;
                frmStat.btnCondFanMotorStart.BackColor = Color.Gainsboro;
                frmStat.btnCondFanMotorPause.Enabled = false;
                frmStat.btnCondFanMotorPause.BackColor = Color.Gainsboro;
                frmStat.btnCondFanMotorComplete.Enabled = false;
                frmStat.btnCondFanMotorComplete.BackColor = Color.Gainsboro;
                frmStat.lbCondFanMotorCurStatus.BackColor = Color.Red;
            }
            frmStat.lbCondFanMotorCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["ElectricalKit"];
                objMain.SubAssemblyName = "ElectricalKit";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;               
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnElecKitStart.Enabled = true;
                frmStat.btnElecKitStart.BackColor = Color.Lime;
                frmStat.btnElecKitPause.Enabled = false;
                frmStat.btnElecKitPause.BackColor = Color.Gainsboro;
                frmStat.btnElecKitComplete.Enabled = false;
                frmStat.btnElecKitComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbElecKitsCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnElecKitStart.Enabled = false;
                frmStat.btnElecKitStart.BackColor = Color.Gainsboro;
                frmStat.btnElecKitPause.Enabled = true;
                frmStat.btnElecKitPause.BackColor = Color.Yellow;
                frmStat.btnElecKitComplete.Enabled = true;
                frmStat.btnElecKitComplete.BackColor = Color.Red;
                frmStat.lbElecKitsCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnElecKitStart.Enabled = false;
                frmStat.btnElecKitStart.BackColor = Color.Gainsboro;
                frmStat.btnElecKitPause.Enabled = false;
                frmStat.btnElecKitPause.BackColor = Color.Gainsboro;
                frmStat.btnElecKitComplete.Enabled = false;
                frmStat.btnElecKitComplete.BackColor = Color.Gainsboro;
                frmStat.lbElecKitsCurStatus.BackColor = Color.Red;
            }
            frmStat.lbElecKitsCurStatus.Text = currenStatus;

            currenStatus = "";
            if (jobFound == true)
            {
                asmStatus = (int)dr["RefrigerationKit"];
                objMain.SubAssemblyName = "RefrigerationKit";
                DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
                if (dtHist.Rows.Count > 0)
                {
                    DataRow drHist = dtHist.Rows[0];
                    currenStatus = drHist["TransType"].ToString();
                    currenStatus += " " + drHist["TransDate"].ToString();
                }
            }
            else
            {
                asmStatus = 0;               
            }
            
            if (asmStatus == 0 || asmStatus == 2)
            {
                frmStat.btnRfgKitStart.Enabled = true;
                frmStat.btnRfgKitStart.BackColor = Color.Lime;
                frmStat.btnRfgKitPause.Enabled = false;
                frmStat.btnRfgKitPause.BackColor = Color.Gainsboro;
                frmStat.btnRfgKitComplete.Enabled = false;
                frmStat.btnRfgKitComplete.BackColor = Color.Gainsboro;
                if (asmStatus == 2)
                {
                    frmStat.lbRfgKitsCurStatus.BackColor = Color.Yellow;
                }
            }
            else if (asmStatus == 1)
            {
                frmStat.btnRfgKitStart.Enabled = false;
                frmStat.btnRfgKitStart.BackColor = Color.Gainsboro;
                frmStat.btnRfgKitPause.Enabled = true;
                frmStat.btnRfgKitPause.BackColor = Color.Yellow;
                frmStat.btnRfgKitComplete.Enabled = true;
                frmStat.btnRfgKitComplete.BackColor = Color.Red;
                frmStat.lbRfgKitsCurStatus.BackColor = Color.Lime;
            }
            else
            {
                frmStat.btnRfgKitStart.Enabled = false;
                frmStat.btnRfgKitStart.BackColor = Color.Gainsboro;
                frmStat.btnRfgKitPause.Enabled = false;
                frmStat.btnRfgKitPause.BackColor = Color.Gainsboro;
                frmStat.btnRfgKitComplete.Enabled = false;
                frmStat.btnRfgKitComplete.BackColor = Color.Gainsboro;
                frmStat.lbRfgKitsCurStatus.BackColor = Color.Red;
            }
            frmStat.lbRfgKitsCurStatus.Text = currenStatus;

            frmStat.ShowDialog();

            dt = objMain.GetSubAssemblyList();
            dgvSubAsmSchd.DataSource = dt;
            dgvSubAsmSchd.Refresh();

        }
        #endregion

        #region Other Methods
        private void PopulateAllScreens(bool firstPass)
        {
            //MessageBox.Show("Get HeatPump");
            PopulateHeatPumpGrid();
            //MessageBox.Show("HeatPump Good");
            PopulateAuxBoxGrid();
            //MessageBox.Show("AuxBox Good");
            PopulatePolyIsoGrid();
            PopulateSpecialGrid();
            PopulateSubAssemblyGrid();
        }

        private void PopulateHeatPumpGrid()
        {          
            DataTable dt = objMain.GetHeatPumpJobs();            
            dgvHeatPump.DataSource = dt;

            dgvHeatPump.Columns["JobNum"].Width = 75;            // JobNum
            dgvHeatPump.Columns["JobNum"].HeaderText = "JobNum";
            dgvHeatPump.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvHeatPump.Columns["JobNum"].DisplayIndex = 0;           
            dgvHeatPump.Columns["HeatPump"].Width = 90;           // Heat Pump
            dgvHeatPump.Columns["HeatPump"].HeaderText = "Heat Pump";
            dgvHeatPump.Columns["HeatPump"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvHeatPump.Columns["HeatPump"].DisplayIndex = 1;
            dgvHeatPump.Columns["OrigStartDate"].Width = 75;           // Start Date
            dgvHeatPump.Columns["OrigStartDate"].HeaderText = "Start Date";
            dgvHeatPump.Columns["OrigStartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["OrigStartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvHeatPump.Columns["OrigStartDate"].DisplayIndex = 2;
            dgvHeatPump.Columns["ProdLine"].Width = 40;           // Prod Line   
            dgvHeatPump.Columns["ProdLine"].HeaderText = "Prod\nLine";
            dgvHeatPump.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvHeatPump.Columns["ProdLine"].DisplayIndex = 3;
            dgvHeatPump.Columns["StartPos"].Width = 40;           // StartPos   
            dgvHeatPump.Columns["StartPos"].HeaderText = "Start\nOrder";
            dgvHeatPump.Columns["StartPos"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvHeatPump.Columns["StartPos"].DisplayIndex = 4;
            dgvHeatPump.Columns["CurrentStatus"].Width = 125;           // Current Status
            dgvHeatPump.Columns["CurrentStatus"].HeaderText = "Current\nStatus";
            dgvHeatPump.Columns["CurrentStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvHeatPump.Columns["CurrentStatus"].DisplayIndex = 5;
            dgvHeatPump.Columns["Metal"].Width = 40;
            dgvHeatPump.Columns["Metal"].HeaderText = "Metal";
            dgvHeatPump.Columns["Metal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["Metal"].DisplayIndex = 6;
            dgvHeatPump.Columns["Coils"].Width = 40;
            dgvHeatPump.Columns["Coils"].HeaderText = "Coils";
            dgvHeatPump.Columns["Coils"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["Coils"].DisplayIndex = 7;
            dgvHeatPump.Columns["OtherPurchParts"].Width = 45;
            dgvHeatPump.Columns["OtherPurchParts"].HeaderText = "Other\nPurch\nParts";
            dgvHeatPump.Columns["OtherPurchParts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["OtherPurchParts"].DisplayIndex = 8;
            dgvHeatPump.Columns["PartsMadeInHouse"].Width = 50;
            dgvHeatPump.Columns["PartsMadeInHouse"].HeaderText = "Parts\nMade\nInHouse";
            dgvHeatPump.Columns["PartsMadeInHouse"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["PartsMadeInHouse"].DisplayIndex = 9;
            dgvHeatPump.Columns["PrefabFittingBrazing"].Width = 50;
            dgvHeatPump.Columns["PrefabFittingBrazing"].HeaderText = "Prefab\nFitttins\nBrazing";
            dgvHeatPump.Columns["PrefabFittingBrazing"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["PrefabFittingBrazing"].DisplayIndex = 10;
            dgvHeatPump.Columns["Insulation"].Width = 45;
            dgvHeatPump.Columns["Insulation"].HeaderText = "Insul";
            dgvHeatPump.Columns["Insulation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["Insulation"].DisplayIndex = 11;
            dgvHeatPump.Columns["Storage"].Width = 50;
            dgvHeatPump.Columns["Storage"].HeaderText = "Storage";
            dgvHeatPump.Columns["Storage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["Storage"].DisplayIndex = 12;
            dgvHeatPump.Columns["DeliveredToLine"].Width = 50;
            dgvHeatPump.Columns["DeliveredToLine"].HeaderText = "Deliver\nToLine";
            dgvHeatPump.Columns["DeliveredToLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvHeatPump.Columns["DeliveredToLine"].DisplayIndex = 13;
            dgvHeatPump.Columns["ModelNo"].Visible = false;            // ModelNo
            dgvHeatPump.Columns["Tonage"].Visible = false;
            dgvHeatPump.Columns["StartDate"].Visible = false;
            displayHeatPumpSelectedValues(0, true);
        }

        private void PopulateAuxBoxGrid()
        {
            DataTable dt = objMain.GetAuxBoxJobs();
            dgvAuxBox.DataSource = dt;

            dgvAuxBox.Columns["JobNum"].Width = 80;            // JobNum
            dgvAuxBox.Columns["JobNum"].HeaderText = "JobNum";
            dgvAuxBox.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAuxBox.Columns["JobNum"].DisplayIndex = 0;
            dgvAuxBox.Columns["ModelNo"].Width = 275;            // ModelNo
            dgvAuxBox.Columns["ModelNo"].HeaderText = "ModelNo";
            dgvAuxBox.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAuxBox.Columns["ModelNo"].DisplayIndex = 1;
            dgvAuxBox.Columns["UnitType"].Width = 45;            // UnitType
            dgvAuxBox.Columns["UnitType"].HeaderText = "Unit\nType";
            dgvAuxBox.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvAuxBox.Columns["UnitType"].DisplayIndex = 2;
            dgvAuxBox.Columns["AuxBox"].Width = 100;           // Secondary Mod
            dgvAuxBox.Columns["AuxBox"].HeaderText = "AuxBox/Sec Mod";
            dgvAuxBox.Columns["AuxBox"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAuxBox.Columns["AuxBox"].DisplayIndex = 3;
            dgvAuxBox.Columns["OrigStartDate"].Width = 80;           // Start Date
            dgvAuxBox.Columns["OrigStartDate"].HeaderText = "Start Date";
            dgvAuxBox.Columns["OrigStartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvAuxBox.Columns["OrigStartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvAuxBox.Columns["OrigStartDate"].DisplayIndex = 4;           
            dgvAuxBox.Columns["ProdLine"].Width = 45;           // Prod Line   
            dgvAuxBox.Columns["ProdLine"].HeaderText = "Prod\nLine";
            dgvAuxBox.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAuxBox.Columns["ProdLine"].DisplayIndex = 5;
            dgvAuxBox.Columns["StartPos"].Width = 40;           // StartPos   
            dgvAuxBox.Columns["StartPos"].HeaderText = "Start\nOrder";
            dgvAuxBox.Columns["StartPos"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAuxBox.Columns["StartPos"].DisplayIndex = 6;
            dgvAuxBox.Columns["CurrentStatus"].Width = 150;           // Current Status
            dgvAuxBox.Columns["CurrentStatus"].HeaderText = "Current\nStatus";
            dgvAuxBox.Columns["CurrentStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAuxBox.Columns["CurrentStatus"].DisplayIndex = 7;
            dgvAuxBox.Columns["CustomerName"].Visible = false;

            displayAuxBoxSelectedValues(0);
        }

        private void PopulatePolyIsoGrid()
        {
            DataTable dt = objMain.GetPolyIsoJobs();
            dgvPolyIso.DataSource = dt;
            dgvPolyIso.AutoGenerateColumns = false;

            dgvPolyIso.Columns["JobNum"].Width = 80;            // JobNum
            dgvPolyIso.Columns["JobNum"].HeaderText = "JobNum";
            dgvPolyIso.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPolyIso.Columns["JobNum"].DisplayIndex = 0;
            dgvPolyIso.Columns["UnitType"].Width = 75;           // UnitType
            dgvPolyIso.Columns["UnitType"].HeaderText = "UnitType";
            dgvPolyIso.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPolyIso.Columns["UnitType"].DisplayIndex = 1;
            dgvPolyIso.Columns["AuxBox"].Width = 200;           // AuxBoxSecondary Mod
            dgvPolyIso.Columns["AuxBox"].HeaderText = "AuxBox/Sec Mod";
            dgvPolyIso.Columns["AuxBox"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPolyIso.Columns["AuxBox"].DisplayIndex = 2;
            dgvPolyIso.Columns["OrigStartDate"].Width = 80;           // Start Date
            dgvPolyIso.Columns["OrigStartDate"].HeaderText = "Start Date";
            dgvPolyIso.Columns["OrigStartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPolyIso.Columns["OrigStartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvPolyIso.Columns["OrigStartDate"].DisplayIndex = 3;            
            dgvPolyIso.Columns["ProdLine"].Width = 45;           // Prod Line   
            dgvPolyIso.Columns["ProdLine"].HeaderText = "Prod\nLine";
            dgvPolyIso.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPolyIso.Columns["ProdLine"].DisplayIndex = 4;
            dgvPolyIso.Columns["StartPos"].Width = 40;           // StartPos   
            dgvPolyIso.Columns["StartPos"].HeaderText = "Start\nOrder";
            dgvPolyIso.Columns["StartPos"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPolyIso.Columns["StartPos"].DisplayIndex = 5;
            dgvPolyIso.Columns["CurrentStatus"].Width = 150;           // Current Status
            dgvPolyIso.Columns["CurrentStatus"].HeaderText = "Current Status";
            dgvPolyIso.Columns["CurrentStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPolyIso.Columns["CurrentStatus"].DisplayIndex = 6;            
            dgvPolyIso.Columns["Description"].Visible = false;
            dgvPolyIso.Columns["ModelNo"].Visible = false;
            dgvPolyIso.Columns["PartNum"].Visible = false;            
            dgvPolyIso.Columns["RequiredQty"].Visible = false;
            dgvPolyIso.Columns["StartDate"].Visible = false;           

            displayPolyIsoSelectedValues(0);
        }

        private void PopulateSpecialGrid()
        {
            DataTable dt = objMain.Get65SCCRJobs();
            dgvSpecial.DataSource = dt;

            dgvSpecial.Columns["JobNum"].Width = 80;            // JobNum
            dgvSpecial.Columns["JobNum"].HeaderText = "JobNum";
            dgvSpecial.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSpecial.Columns["JobNum"].DisplayIndex = 0;
            dgvSpecial.Columns["ModelNo"].Width = 250;            // ModelNo
            dgvSpecial.Columns["ModelNo"].HeaderText = "ModelNo";
            dgvSpecial.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSpecial.Columns["ModelNo"].DisplayIndex = 1;
            dgvSpecial.Columns["Special"].Width = 100;           // Heat Pump
            dgvSpecial.Columns["Special"].HeaderText = "Heat Pump";
            dgvSpecial.Columns["Special"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSpecial.Columns["Special"].DisplayIndex = 2;
            dgvSpecial.Columns["OrigStartDate"].Width = 80;           // Start Date
            dgvSpecial.Columns["OrigStartDate"].HeaderText = "Start Date";
            dgvSpecial.Columns["OrigStartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSpecial.Columns["OrigStartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvSpecial.Columns["OrigStartDate"].DisplayIndex = 3;
            dgvSpecial.Columns["ProdLine"].Width = 45;           // Prod Line   
            dgvSpecial.Columns["ProdLine"].HeaderText = "Prod\nLine";
            dgvSpecial.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSpecial.Columns["ProdLine"].DisplayIndex = 4;
            dgvSpecial.Columns["StartPos"].Width = 40;           // StartPos   
            dgvSpecial.Columns["StartPos"].HeaderText = "Start\nOrder";
            dgvSpecial.Columns["StartPos"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvSpecial.Columns["StartPos"].DisplayIndex = 5;
            dgvSpecial.Columns["CurrentStatus"].Width = 150;           // Current Status
            dgvSpecial.Columns["CurrentStatus"].HeaderText = "Current\nStatus";
            dgvSpecial.Columns["CurrentStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSpecial.Columns["CurrentStatus"].DisplayIndex = 6;           
            dgvSpecial.Columns["StartDate"].Visible = false;
            displaySpecialSelectedValues(0);
        }

        private void PopulateSubAssemblyGrid()
        {
            DataTable dt = objMain.GetSubAssemblyList();
            dgvSubAsmSchd.DataSource = dt;

            dgvSubAsmSchd.Columns["JobNum"].Width = 80;            // JobNum
            dgvSubAsmSchd.Columns["JobNum"].HeaderText = "JobNum";
            dgvSubAsmSchd.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubAsmSchd.Columns["JobNum"].DisplayIndex = 0;
            dgvSubAsmSchd.Columns["CustomerName"].Width = 150;            // CustomerName
            dgvSubAsmSchd.Columns["CustomerName"].HeaderText = "CustomerName";
            dgvSubAsmSchd.Columns["CustomerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubAsmSchd.Columns["CustomerName"].DisplayIndex = 1;
            dgvSubAsmSchd.Columns["UnitType"].Width = 45;            // UnitType
            dgvSubAsmSchd.Columns["UnitType"].HeaderText = "Unit\nType";
            dgvSubAsmSchd.Columns["UnitType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["UnitType"].DisplayIndex = 2;
            dgvSubAsmSchd.Columns["AuxBox"].Width = 100;           // Secondary Mod
            dgvSubAsmSchd.Columns["AuxBox"].HeaderText = "AuxBox/Sec Mod";
            dgvSubAsmSchd.Columns["AuxBox"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubAsmSchd.Columns["AuxBox"].DisplayIndex = 3;
            dgvSubAsmSchd.Columns["OrigStartDate"].Width = 80;           // Start Date
            dgvSubAsmSchd.Columns["OrigStartDate"].HeaderText = "Start Date";
            dgvSubAsmSchd.Columns["OrigStartDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["OrigStartDate"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvSubAsmSchd.Columns["OrigStartDate"].DisplayIndex = 4;
            dgvSubAsmSchd.Columns["ProdLine"].Width = 45;           // Prod Line   
            dgvSubAsmSchd.Columns["ProdLine"].HeaderText = "Prod\nLine";
            dgvSubAsmSchd.Columns["ProdLine"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubAsmSchd.Columns["ProdLine"].DisplayIndex = 5;
            dgvSubAsmSchd.Columns["StartPos"].Width = 40;           // StartPos   
            dgvSubAsmSchd.Columns["StartPos"].HeaderText = "Start\nOrder";
            dgvSubAsmSchd.Columns["StartPos"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvSubAsmSchd.Columns["StartPos"].DisplayIndex = 6;
            dgvSubAsmSchd.Columns["CurrentStatus"].Width = 140;           // Current Status
            dgvSubAsmSchd.Columns["CurrentStatus"].HeaderText = "Current Status";
            dgvSubAsmSchd.Columns["CurrentStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvSubAsmSchd.Columns["CurrentStatus"].DisplayIndex = 7;
            dgvSubAsmSchd.Columns["CoolingWalls"].Width = 50;           // CoolingWalls   
            dgvSubAsmSchd.Columns["CoolingWalls"].HeaderText = "Cooling\nWalls";
            dgvSubAsmSchd.Columns["CoolingWalls"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["CoolingWalls"].DisplayIndex = 8;
            dgvSubAsmSchd.Columns["Heater"].Width = 50;           // Heater   
            dgvSubAsmSchd.Columns["Heater"].HeaderText = "Heater";
            dgvSubAsmSchd.Columns["Heater"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["Heater"].DisplayIndex = 9;
            dgvSubAsmSchd.Columns["DoorPanels"].Width = 50;           // DoorPanels   
            dgvSubAsmSchd.Columns["DoorPanels"].HeaderText = "Door\nPanels";
            dgvSubAsmSchd.Columns["DoorPanels"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["DoorPanels"].DisplayIndex = 10;
            dgvSubAsmSchd.Columns["AuxBoxStat"].Width = 50;           // AuxBoxStat   
            dgvSubAsmSchd.Columns["AuxBoxStat"].HeaderText = "AuxBox\nStatus";
            dgvSubAsmSchd.Columns["AuxBoxStat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["AuxBoxStat"].DisplayIndex = 11;
            dgvSubAsmSchd.Columns["Intake"].Width = 50;           // Intake   
            dgvSubAsmSchd.Columns["Intake"].HeaderText = "Intake";
            dgvSubAsmSchd.Columns["Intake"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["Intake"].DisplayIndex = 12;
            dgvSubAsmSchd.Columns["Breaker"].Width = 50;           // Breaker   
            dgvSubAsmSchd.Columns["Breaker"].HeaderText = "Breaker";
            dgvSubAsmSchd.Columns["Breaker"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["Breaker"].DisplayIndex = 13;
            dgvSubAsmSchd.Columns["Transformer"].Width = 50;           // Transformer   
            dgvSubAsmSchd.Columns["Transformer"].HeaderText = "Trans";
            dgvSubAsmSchd.Columns["Transformer"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["Transformer"].DisplayIndex = 14;
            dgvSubAsmSchd.Columns["IndoorFanMotor"].Width = 50;           // IndoorFanMotor   
            dgvSubAsmSchd.Columns["IndoorFanMotor"].HeaderText = "IndFan\nMotor";
            dgvSubAsmSchd.Columns["IndoorFanMotor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["IndoorFanMotor"].DisplayIndex = 15;
            dgvSubAsmSchd.Columns["PoweredExhaustMotor"].Width = 50;           // IndoorFanMotor   
            dgvSubAsmSchd.Columns["PoweredExhaustMotor"].HeaderText = "PwrExh\nMotor";
            dgvSubAsmSchd.Columns["PoweredExhaustMotor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["PoweredExhaustMotor"].DisplayIndex = 16;
            dgvSubAsmSchd.Columns["CondenserFanMotor"].Width = 50;           // CondenserFanMotor   
            dgvSubAsmSchd.Columns["CondenserFanMotor"].HeaderText = "CondFan\nMotor";
            dgvSubAsmSchd.Columns["CondenserFanMotor"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["CondenserFanMotor"].DisplayIndex = 17;
            dgvSubAsmSchd.Columns["ElectricalKit"].Width = 50;           // ElectricalKit   
            dgvSubAsmSchd.Columns["ElectricalKit"].HeaderText = "ElecKit";
            dgvSubAsmSchd.Columns["ElectricalKit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["ElectricalKit"].DisplayIndex = 18;
            dgvSubAsmSchd.Columns["RefrigerationKit"].Width = 50;           // RefrigerationKit   
            dgvSubAsmSchd.Columns["RefrigerationKit"].HeaderText = "Refrig\nKit";
            dgvSubAsmSchd.Columns["RefrigerationKit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["RefrigerationKit"].DisplayIndex = 19;
            dgvSubAsmSchd.Columns["Other"].Width = 100;           // Other   
            dgvSubAsmSchd.Columns["Other"].HeaderText = "Other";
            dgvSubAsmSchd.Columns["Other"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvSubAsmSchd.Columns["Other"].DisplayIndex = 20;
            dgvSubAsmSchd.Columns["ModelNo"].Visible = false;
        }

        private void MyTimer_Tick(object sender, EventArgs e)
        {            
            PopulateAllScreens(false);                         
        }

        private void displayHeatPumpSelectedValues(int rowIdx, bool initalDisplay)
        {           
            string modelNo = dgvHeatPump.Rows[rowIdx].Cells["ModelNo"].Value.ToString();
            string unitType = modelNo.Substring(0, 3);
            string heatPumpType = "";           

            bool jobFound = false;

            int hpStatus = 0;

            if (modelNo.Length > 39)
            {
                lbHeatPumpModelNo1.Text = modelNo.Substring(0, 40);
                lbHeatPumpModelNo2.Text = modelNo.Substring(40, 29);
                lbHeatPumpModelNo2.Visible = true;
            }
            else
            {
                lbHeatPumpModelNo1.Text = modelNo;
                lbHeatPumpModelNo2.Visible = false;
            }

            objMain.JobNum = dgvHeatPump.Rows[rowIdx].Cells["JobNum"].Value.ToString();
            lbHeatPumpJobNum.Text = objMain.JobNum;
            lbHeatPumpTonage.Text = unitType + " - " + dgvHeatPump.Rows[rowIdx].Cells["Tonage"].Value.ToString();
            heatPumpType = dgvHeatPump.Rows[rowIdx].Cells["HeatPump"].Value.ToString();

            if (heatPumpType.Contains("Outdoor") == true)
            {
                if (heatPumpType.Contains("WSHP") == true)
                {
                    lbHeatPumpType.Text = "Outdoor Water Source Heat Pump";
                }
                else
                {
                    lbHeatPumpType.Text = "Outdoor Air Source Heat Pump";
                }
            }
            else
            {
                lbHeatPumpType.Text = "Indoor Water Source Heat Pump";
            }
            
            lbHeatPumpStartDate.Text = "Start Date: " + dgvHeatPump.Rows[rowIdx].Cells["StartDate"].Value.ToString();
            lbHeatPumpProdLine.Text = "Prod Line: " + dgvHeatPump.Rows[rowIdx].Cells["ProdLine"].Value.ToString();
            lbHeatPumpCurrentStatus.Text = dgvHeatPump.Rows[rowIdx].Cells["CurrentStatus"].Value.ToString();

            if (initalDisplay == false)
            {
                frmHeatPumpAreaStatus frmHPS = new frmHeatPumpAreaStatus();

                frmHPS.lbJobNum.Text = objMain.JobNum;
                DataTable dt = objMain.GetHeatPumpAreaStatusByJob();
                DataRow dr = null;
                if (dt.Rows.Count > 0)
                {
                    jobFound = true;
                    dr = dt.Rows[0];
                }

                if (jobFound == true)
                {
                    hpStatus = (int)dr["Metal"];
                    objMain.SubAssemblyName = "Metal";
                    colorCodeButtons(frmHPS.btnMetalOrdered, frmHPS.btnMetalInProgress, frmHPS.btnMetalComplete,
                                     frmHPS.txtMetalNotes, frmHPS.lbMetalCurStatus, hpStatus, dr["MetalNotes"].ToString());

                    hpStatus = (int)dr["Coils"];
                    objMain.SubAssemblyName = "Coils";
                    colorCodeButtons(frmHPS.btnCoilsOrdered, frmHPS.btnCoilsInProgress, frmHPS.btnCoilsComplete,
                                     frmHPS.txtCoilsNotes, frmHPS.lbCoilsCurStatus, hpStatus, dr["CoilNotes"].ToString());

                    hpStatus = (int)dr["OtherPurchParts"];
                    objMain.SubAssemblyName = "OtherPurchParts";
                    colorCodeButtons(frmHPS.btnOtherPP_Ordered, frmHPS.btnOtherPP_InProgress, frmHPS.btnOtherPP_Complete,
                                     frmHPS.txtOtherPP_Notes, frmHPS.lbOtherPP_CurStatus, hpStatus, dr["OtherPP_Notes"].ToString());

                    hpStatus = (int)dr["PartsMadeInHouse"];
                    objMain.SubAssemblyName = "PartsMadeInHouse";
                    colorCodeButtons(frmHPS.btnPartsMadeInHouseOrdered, frmHPS.btnPartsMadeInHouseInProgress, frmHPS.btnPartsMadeInHouseComplete,
                                     frmHPS.txtPartsMadeInHouseNotes, frmHPS.lbPartsMadeInHouseStatus, hpStatus, dr["PartsMadeInHouseNotes"].ToString());

                    hpStatus = (int)dr["PrefabFittingBrazing"];
                    objMain.SubAssemblyName = "PrefabFittingBrazing";
                    colorCodeButtons(frmHPS.btnPrefabFitBrazOrdered, frmHPS.btnPrefabFitBrazInProgress, frmHPS.btnPrefabFitBrazComplete,
                                     frmHPS.txtPrefabFitBrazNotes, frmHPS.lbPrefabFitBrazCurStatus, hpStatus, dr["PrefabNotes"].ToString());

                    hpStatus = (int)dr["Insulation"];
                    objMain.SubAssemblyName = "Insulation";
                    colorCodeButtons(frmHPS.btnInsulationOrdered, frmHPS.btnInsulationInProgress, frmHPS.btnInsulationComplete,
                                     frmHPS.txtInsulationNotes, frmHPS.lbInsulationCurStatus, hpStatus, dr["InsulationNotes"].ToString());

                    hpStatus = (int)dr["Storage"];
                    objMain.SubAssemblyName = "Storage";
                    colorCodeButtons(frmHPS.btnStorageOrdered, frmHPS.btnStorageInProgress, frmHPS.btnStorageComplete,
                                     frmHPS.txtStorageNotes, frmHPS.lbStorageCurStatus, hpStatus, dr["StorageNotes"].ToString());

                    hpStatus = (int)dr["DeliveredToLine"];
                    objMain.SubAssemblyName = "DeliveredToLine";
                    colorCodeButtons(frmHPS.btnDeliveredToLineOrdered, frmHPS.btnDeliveredToLineInProgress, frmHPS.btnDeliveredToLineComplete,
                                     frmHPS.txtDeliveredToLineNotes, frmHPS.lbDeliveredToLineCurStatus, hpStatus, dr["DeliveredToLineNotes"].ToString());
                }
                else
                {
                    hpStatus = 0;
                }

                frmHPS.ShowDialog();                
            }           
        }

        private void displayRfgCompRpt(string jobNumStr, string modelNo1, string modelNo2)
        {            
            string modelTypeStr = "REV5";            
            string modelNo = modelNo1 + modelNo2;           
            string circuit1Charge = "";
            string circuit2Charge = "";

            string digit3 = modelNo.Substring(2, 1);
            string digit4 = modelNo.Substring(3, 1);
            string digit567 = modelNo.Substring(4, 3);
            string digit11 = modelNo.Substring(10, 1);

            string circuit1Suction = "";
            string circuit1LiquidLine = "";
            string circuit1DischargeLine = "";
            string circuit2Suction = "";
            string circuit2LiquidLine = "";
            string circuit2DischargeLine = "";

            frmPrintRpt frmPrint = new frmPrintRpt();

            ReportDocument cryRpt = new ReportDocument();

            if (modelNo.Length == 39)
            {
                digit4 = "A";
            }

            DataTable dtLineSize = objMain.GetLineSize(digit3, digit4, digit567, digit11);

            if (dtLineSize.Rows.Count > 0)
            {
                DataRow row2 = dtLineSize.Rows[0];

                circuit1Suction = row2["Circuit1Suction"].ToString();
                circuit1LiquidLine = row2["Circuit1LiquidLine"].ToString();
                circuit1DischargeLine = row2["Circuit1DischargeLine"].ToString();

                if (row2["Circuit2Suction"] != null)
                {
                    circuit2Suction = row2["Circuit2Suction"].ToString();
                    circuit2LiquidLine = row2["Circuit2LiquidLine"].ToString();
                    circuit2DischargeLine = row2["Circuit2DischargeLine"].ToString();
                }
            }

            if (modelNo.Length == 69)
            {
                modelTypeStr = "REV6";
            }

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OA_Rev";
            pdv2.Value = modelTypeStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNo;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@Cir1Chrg";
            pdv4.Value = circuit1Charge;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Cir2Chrg";
            pdv5.Value = circuit2Charge;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Circuit1Suction";
            pdv6.Value = circuit1Suction;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Circuit1LiquidLine";
            pdv7.Value = circuit1LiquidLine;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@Circuit1DischargeLine";
            pdv8.Value = circuit1DischargeLine;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@Circuit2Suction";
            pdv9.Value = circuit2Suction;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            ParameterField pf10 = new ParameterField();
            ParameterDiscreteValue pdv10 = new ParameterDiscreteValue();
            pf10.Name = "@Circuit2LiquidLine";
            pdv10.Value = circuit2LiquidLine;
            pf10.CurrentValues.Add(pdv10);

            paramFields.Add(pf10);

            ParameterField pf11 = new ParameterField();
            ParameterDiscreteValue pdv11 = new ParameterDiscreteValue();
            pf11.Name = "@Circuit2DischargeLine";
            pdv11.Value = circuit2DischargeLine;
            pf11.CurrentValues.Add(pdv11);

            paramFields.Add(pf11);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_RefrigerationComp.v2.rpt");

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.Show();
            frmPrint.crystalReportViewer1.Refresh();

        }

        private void displayCriticalCompRpt(string jobNum, string modelNo1, string custName)
        {
            string modelNoStr =modelNo1;

            shortModelNumCritCompBOM(jobNum, modelNoStr, custName);          
        }

        private void shortModelNumCritCompBOM(string jobNumStr, string modelNoStr, string custNameStr)
        {
            string jobNameStr = custNameStr; ;
            string notesStr = "";
            string orderNumStr = jobNumStr.Substring(0, jobNumStr.LastIndexOf('-'));
            string tonageStr = modelNoStr.Substring(4, 3);
            string voltageStr = "";
            string OAUTypeCode = "OAU123DKN";
            string digitValStr = "";
            string digitDescStr = "";
            string heatType = "NA";
            string refCir1Chrg = "";
            string refCir2Chrg = "";

            int tonageInt = Int32.Parse(tonageStr) / 12;

            tonageStr = tonageInt.ToString() + " Tons";

            if (modelNoStr.Substring(2, 1) == "B" || modelNoStr.Substring(2, 1) == "G")
            {
                OAUTypeCode = "OALBG";
            }

            digitValStr = modelNoStr.Substring(7, 1); // Digit 8
            DataTable dt = objMain.GetRev5ModelNoDigitDesc(8, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr = "8 Minor Design: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(8, 1); // Digit 9
            dt = objMain.GetRev5ModelNoDigitDesc(9, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                voltageStr = digitDescStr;
            }

            digitValStr = modelNoStr.Substring(16, 1); // Digit 17
            dt = objMain.GetRev5ModelNoDigitDesc(17, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n17 Indoor Fan Wheel: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(17, 1); // Digit 18
            dt = objMain.GetRev5ModelNoDigitDesc(18, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n18 Indoor Fan Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(24, 2); // Digit 25 & 26
            dt = objMain.GetRev5ModelNoDigitDesc(2526, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n26 Unit Controls: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(27, 1); // Digit 28
            dt = objMain.GetRev5ModelNoDigitDesc(28, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n28 Pwr Exh Wheel: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(28, 1); // Digit 29
            dt = objMain.GetRev5ModelNoDigitDesc(29, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n29 Pwr Exh Mtr HP: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(31, 1); // Digit 32
            dt = objMain.GetRev5ModelNoDigitDesc(32, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n32 ERV Size: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(33, 1); // Digit 34
            dt = objMain.GetRev5ModelNoDigitDesc(34, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n34 Filter Options: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(34, 1); // Digit 35
            dt = objMain.GetRev5ModelNoDigitDesc(35, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n35 Smoke Detector: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(35, 1); // Digit 36
            dt = objMain.GetRev5ModelNoDigitDesc(36, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n36 Elec Options: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(36, 1); // Digit 37
            dt = objMain.GetRev5ModelNoDigitDesc(37, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n37 Airflow Monitoring: " + digitDescStr;
            }

            digitValStr = modelNoStr.Substring(37, 1); // Digit 38
            dt = objMain.GetRev5ModelNoDigitDesc(38, digitValStr, heatType, OAUTypeCode);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                digitDescStr = dr["DigitDescription"].ToString();
                notesStr += "\n38 Hailgaurds: " + digitDescStr;
            }
            
            frmPrintRpt frmPrint = new frmPrintRpt();

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@JobNum";
            pdv1.Value = jobNumStr;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@OrderNum";
            pdv2.Value = orderNumStr;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@ModelNo";
            pdv3.Value = modelNoStr;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@JobName";
            pdv4.Value = jobNameStr;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@Tonage";
            pdv5.Value = tonageStr;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@Voltage";
            pdv6.Value = voltageStr;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            ParameterField pf7 = new ParameterField();
            ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            pf7.Name = "@Notes";
            pdv7.Value = notesStr;
            pf7.CurrentValues.Add(pdv7);

            paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@RefCir1Chrg";
            pdv8.Value = refCir1Chrg;
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@RefCir2Chrg";
            pdv9.Value = refCir2Chrg;
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            frmPrint.crystalReportViewer1.ParameterFieldInfo = paramFields;

#if DEBUG
            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompBOM.rpt");
#else
            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompBOM.rpt");
#endif
            //cryRpt.Load(@"C:\Users\tonyt\Documents\Visual Studio 2013\Projects\OAUConfig\CriticalCompBOM.rpt");

            frmPrint.crystalReportViewer1.ReportSource = cryRpt;
            frmPrint.Show();
            frmPrint.crystalReportViewer1.Refresh();
        }        

        private void displayAuxBoxSelectedValues(int rowIdx)
        {
            string modelNo = dgvAuxBox.Rows[rowIdx].Cells["ModelNo"].Value.ToString();   
            string unitType = modelNo.Substring(0, 3);
            
            lbAuxBoxModelNo1.Text = modelNo;                                    
            lbAuxBoxCustName.Text = dgvAuxBox.Rows[rowIdx].Cells["CustomerName"].Value.ToString();
            lbAuxBoxJobNum.Text = dgvAuxBox.Rows[rowIdx].Cells["JobNum"].Value.ToString();
            lbAuxBoxUnitType.Text = unitType;
            lbAuxBoxSecMod.Text = dgvAuxBox.Rows[rowIdx].Cells["AuxBox"].Value.ToString();
            lbAuxBoxStartDate.Text = "Start Date: " + dgvAuxBox.Rows[rowIdx].Cells["OrigStartDate"].Value.ToString().Substring(0, 10);
            lbAuxBoxProdLine.Text = "Prod Line: " + dgvAuxBox.Rows[rowIdx].Cells["ProdLine"].Value.ToString();
            lbAuxBoxCurStatus.Text = dgvAuxBox.Rows[rowIdx].Cells["CurrentStatus"].Value.ToString();
        }

        private void displayPolyIsoSelectedValues(int rowIdx)
        {
            string modelNo = dgvPolyIso.Rows[rowIdx].Cells["ModelNo"].Value.ToString();
            string unitType = modelNo.Substring(0, 3);

            lbPolyIsoJobNum.Text = dgvPolyIso.Rows[rowIdx].Cells["JobNum"].Value.ToString();
            lbPolyIsoUnitType.Text = unitType;
            lbPolyIsoAuxBox.Text = dgvPolyIso.Rows[rowIdx].Cells["AuxBox"].Value.ToString();

            if (modelNo.Length > 39)
            {
                lbPolyIsoModelNo1.Text = modelNo.Substring(0, 40);
                lbPolyIsoModelNo2.Text = modelNo.Substring(40, 29);
                lbPolyIsoModelNo2.Visible = true;
            }
            else
            {
                lbPolyIsoModelNo1.Text = modelNo;
                lbPolyIsoModelNo2.Visible = false;
            }

            lbPolyIsoStartDate.Text = "Start Date: " + dgvPolyIso.Rows[rowIdx].Cells["StartDate"].Value.ToString();
            lbPolyIsoProdLine.Text = "Prod Line: " + dgvPolyIso.Rows[rowIdx].Cells["ProdLine"].Value.ToString();
            lbPolyIsoCurStatus.Text = dgvPolyIso.Rows[rowIdx].Cells["CurrentStatus"].Value.ToString();
        }

        private void displaySpecialSelectedValues(int rowIdx)
        {
            string modelNo = dgvSpecial.Rows[rowIdx].Cells["ModelNo"].Value.ToString();

            if (modelNo.Length > 39)
            {
                lbSpecialModelNo1.Text = modelNo.Substring(0, 40);
                lbSpecialModelNo2.Text = modelNo.Substring(40, 29);
                lbSpecialModelNo2.Visible = true;
            }
            else
            {
                lbSpecialModelNo1.Text = modelNo;
                lbSpecialModelNo2.Visible = false;
            }

            lbSpecialJobNum.Text = dgvSpecial.Rows[rowIdx].Cells["JobNum"].Value.ToString();                              
            lbSpecialStartDate.Text = "Start Date: " + dgvSpecial.Rows[rowIdx].Cells["StartDate"].Value.ToString();
            lbSpecialProdLine.Text = "Prod Line: " + dgvSpecial.Rows[rowIdx].Cells["ProdLine"].Value.ToString();
            lbSpecialCurStatus.Text = dgvSpecial.Rows[rowIdx].Cells["CurrentStatus"].Value.ToString();
        }

        private void colorCodeCells( DataGridViewRow row, Color backGround, Color foreGround)
        {
            int cellCnt = row.Cells.Count;

            DataGridViewCellStyle style = new DataGridViewCellStyle();

            for (int x = 0; x < cellCnt; ++x)
            {
                style.BackColor = backGround;
                style.ForeColor = foreGround;
                row.Cells[x].Style = style;
            }
        }

        private void colorCodeButtons(Button btnOrdered, Button btnInProgress, Button btnComplete, 
                                      TextBox txtNotes, Label lbCurStat, int hpStatus, string notesStr)
        {
            string currentStatus = "";

            DataTable dtHist = objMain.GetSubAssemblyHistoryByJob();
            if (dtHist.Rows.Count > 0)
            {
                DataRow drHist = dtHist.Rows[0];
                currentStatus = drHist["TransType"].ToString();
                currentStatus += " " + drHist["TransDate"].ToString();            
            }

            if (hpStatus == 0)
            {
                btnOrdered.Enabled = true;
                btnOrdered.BackColor = Color.Lime;
                btnInProgress.Enabled = false;
                btnInProgress.BackColor = Color.Gainsboro;
                btnComplete.Enabled = false;
                btnComplete.BackColor = Color.Gainsboro;
            }
            else if (hpStatus == 3) // Status equals Complete
            {
                btnOrdered.Enabled = false;
                btnOrdered.BackColor = Color.Gainsboro;
                btnInProgress.Enabled = false;
                btnInProgress.BackColor = Color.Gainsboro;
                btnComplete.Enabled = false;
                btnComplete.BackColor = Color.Gainsboro;

                lbCurStat.BackColor = Color.Red;
                txtNotes.BackColor = Color.Red;
            }
            else if (hpStatus == 4) // Status equals Ordered
            {
                btnOrdered.Enabled = false;
                btnOrdered.BackColor = Color.Gainsboro;
                btnInProgress.Enabled = true;
                btnInProgress.BackColor = Color.Yellow;
                btnComplete.Enabled = true;
                btnComplete.BackColor = Color.Red;

                lbCurStat.BackColor = Color.Lime;
                txtNotes.BackColor = Color.Lime;
            }
            else // Status equals In Progress
            {
                btnOrdered.Enabled = false;
                btnOrdered.BackColor = Color.Gainsboro;
                btnInProgress.Enabled = false;
                btnInProgress.BackColor = Color.Gainsboro;
                btnComplete.Enabled = true;
                btnComplete.BackColor = Color.Red;

                lbCurStat.BackColor = Color.Yellow;
                txtNotes.BackColor = Color.Yellow;
            }

            lbCurStat.Text = currentStatus;
            txtNotes.Text = notesStr;
        }


        #endregion

       
    }
}

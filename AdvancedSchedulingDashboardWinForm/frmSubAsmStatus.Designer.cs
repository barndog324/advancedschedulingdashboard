﻿namespace AdvSchedDashWinForm
{
    partial class frmSubAsmStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbJobNum = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCoolingWallsStart = new System.Windows.Forms.Button();
            this.btnCoolingWallsPause = new System.Windows.Forms.Button();
            this.btnCoolingWallsComplete = new System.Windows.Forms.Button();
            this.lbCoolingWallsCurStatus = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbProdLine = new System.Windows.Forms.Label();
            this.lbHeaterCurStatus = new System.Windows.Forms.Label();
            this.btnHeaterComplete = new System.Windows.Forms.Button();
            this.btnHeaterPause = new System.Windows.Forms.Button();
            this.btnHeaterStart = new System.Windows.Forms.Button();
            this.lbDoorPanelsCurStatus = new System.Windows.Forms.Label();
            this.btnDoorPanelComplete = new System.Windows.Forms.Button();
            this.btnDoorPanelPause = new System.Windows.Forms.Button();
            this.btnDoorPanelStart = new System.Windows.Forms.Button();
            this.lbAuxBoxCurStatus = new System.Windows.Forms.Label();
            this.btnAuxBoxComplete = new System.Windows.Forms.Button();
            this.btnAuxBoxPause = new System.Windows.Forms.Button();
            this.btnAuxBoxStart = new System.Windows.Forms.Button();
            this.lbIntakeCurStatus = new System.Windows.Forms.Label();
            this.btnIntakeComplete = new System.Windows.Forms.Button();
            this.btnIntakePause = new System.Windows.Forms.Button();
            this.btnIntakeStart = new System.Windows.Forms.Button();
            this.lbBreakerCurStatus = new System.Windows.Forms.Label();
            this.btnBreakerComplete = new System.Windows.Forms.Button();
            this.btnBreakerPause = new System.Windows.Forms.Button();
            this.btnBreakerStart = new System.Windows.Forms.Button();
            this.lbTransformerCurStatus = new System.Windows.Forms.Label();
            this.btnTransComplete = new System.Windows.Forms.Button();
            this.btnTransPause = new System.Windows.Forms.Button();
            this.btnTransStart = new System.Windows.Forms.Button();
            this.lbIndoorFanMtrCurStatus = new System.Windows.Forms.Label();
            this.btnInFanMtrComplete = new System.Windows.Forms.Button();
            this.btnInFanMtrPause = new System.Windows.Forms.Button();
            this.btnInFanMtrStart = new System.Windows.Forms.Button();
            this.lbPwrExMotorCurStatus = new System.Windows.Forms.Label();
            this.btnPwrExMotorComplete = new System.Windows.Forms.Button();
            this.btnPwrExMotorPause = new System.Windows.Forms.Button();
            this.btnPwrExMotorStart = new System.Windows.Forms.Button();
            this.lbCondFanMotorCurStatus = new System.Windows.Forms.Label();
            this.btnCondFanMotorComplete = new System.Windows.Forms.Button();
            this.btnCondFanMotorPause = new System.Windows.Forms.Button();
            this.btnCondFanMotorStart = new System.Windows.Forms.Button();
            this.lbElecKitsCurStatus = new System.Windows.Forms.Label();
            this.btnElecKitComplete = new System.Windows.Forms.Button();
            this.btnElecKitPause = new System.Windows.Forms.Button();
            this.btnElecKitStart = new System.Windows.Forms.Button();
            this.lbRfgKitsCurStatus = new System.Windows.Forms.Label();
            this.btnRfgKitComplete = new System.Windows.Forms.Button();
            this.btnRfgKitPause = new System.Windows.Forms.Button();
            this.btnRfgKitStart = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.rtbOtherComments = new System.Windows.Forms.RichTextBox();
            this.btnModelNoConfig = new System.Windows.Forms.Button();
            this.lbModelNo = new System.Windows.Forms.Label();
            this.lbCustName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbJobNum.Location = new System.Drawing.Point(319, 58);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(236, 39);
            this.lbJobNum.TabIndex = 27;
            this.lbJobNum.Text = "Job Number";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(12, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 22);
            this.label1.TabIndex = 28;
            this.label1.Text = "Cooling Walls:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(12, 153);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 22);
            this.label2.TabIndex = 29;
            this.label2.Text = "Heater:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(12, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 22);
            this.label3.TabIndex = 31;
            this.label3.Text = "Aux Box:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(12, 193);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 22);
            this.label4.TabIndex = 30;
            this.label4.Text = "Door Panels:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(12, 393);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 22);
            this.label5.TabIndex = 35;
            this.label5.Text = "Indoor Fan Motors:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(12, 353);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 22);
            this.label6.TabIndex = 34;
            this.label6.Text = "Transformer:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(12, 313);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 22);
            this.label7.TabIndex = 33;
            this.label7.Text = "Breaker:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(12, 273);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(210, 22);
            this.label8.TabIndex = 32;
            this.label8.Text = "Intake:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(12, 553);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 22);
            this.label9.TabIndex = 39;
            this.label9.Text = "Refrigeration Kits:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(12, 513);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(210, 22);
            this.label10.TabIndex = 38;
            this.label10.Text = "Electrical Kits:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(12, 473);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 22);
            this.label11.TabIndex = 37;
            this.label11.Text = "Condenser Fan Motors:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(12, 433);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(210, 22);
            this.label12.TabIndex = 36;
            this.label12.Text = "Powered Exhaust Motors:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCoolingWallsStart
            // 
            this.btnCoolingWallsStart.BackColor = System.Drawing.Color.Lime;
            this.btnCoolingWallsStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoolingWallsStart.ForeColor = System.Drawing.Color.Black;
            this.btnCoolingWallsStart.Location = new System.Drawing.Point(230, 113);
            this.btnCoolingWallsStart.Name = "btnCoolingWallsStart";
            this.btnCoolingWallsStart.Size = new System.Drawing.Size(80, 25);
            this.btnCoolingWallsStart.TabIndex = 40;
            this.btnCoolingWallsStart.Text = "Start";
            this.btnCoolingWallsStart.UseVisualStyleBackColor = false;
            this.btnCoolingWallsStart.Click += new System.EventHandler(this.btnCoolingWallsStart_Click);
            // 
            // btnCoolingWallsPause
            // 
            this.btnCoolingWallsPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCoolingWallsPause.Enabled = false;
            this.btnCoolingWallsPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoolingWallsPause.ForeColor = System.Drawing.Color.Black;
            this.btnCoolingWallsPause.Location = new System.Drawing.Point(355, 113);
            this.btnCoolingWallsPause.Name = "btnCoolingWallsPause";
            this.btnCoolingWallsPause.Size = new System.Drawing.Size(80, 25);
            this.btnCoolingWallsPause.TabIndex = 41;
            this.btnCoolingWallsPause.Text = "Pause";
            this.btnCoolingWallsPause.UseVisualStyleBackColor = false;
            this.btnCoolingWallsPause.Click += new System.EventHandler(this.btnCoolingWallsPause_Click);
            // 
            // btnCoolingWallsComplete
            // 
            this.btnCoolingWallsComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCoolingWallsComplete.Enabled = false;
            this.btnCoolingWallsComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoolingWallsComplete.ForeColor = System.Drawing.Color.Black;
            this.btnCoolingWallsComplete.Location = new System.Drawing.Point(480, 113);
            this.btnCoolingWallsComplete.Name = "btnCoolingWallsComplete";
            this.btnCoolingWallsComplete.Size = new System.Drawing.Size(80, 25);
            this.btnCoolingWallsComplete.TabIndex = 42;
            this.btnCoolingWallsComplete.Text = "Complete";
            this.btnCoolingWallsComplete.UseVisualStyleBackColor = false;
            this.btnCoolingWallsComplete.Click += new System.EventHandler(this.btnCoolingWallsComplete_Click);
            // 
            // lbCoolingWallsCurStatus
            // 
            this.lbCoolingWallsCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbCoolingWallsCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCoolingWallsCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCoolingWallsCurStatus.Location = new System.Drawing.Point(611, 113);
            this.lbCoolingWallsCurStatus.Name = "lbCoolingWallsCurStatus";
            this.lbCoolingWallsCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbCoolingWallsCurStatus.TabIndex = 43;
            this.lbCoolingWallsCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(611, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(226, 23);
            this.label13.TabIndex = 44;
            this.label13.Text = "Current Status";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(13, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 23);
            this.label14.TabIndex = 45;
            this.label14.Text = "Production Line:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbProdLine
            // 
            this.lbProdLine.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdLine.Location = new System.Drawing.Point(151, 86);
            this.lbProdLine.Name = "lbProdLine";
            this.lbProdLine.Size = new System.Drawing.Size(113, 23);
            this.lbProdLine.TabIndex = 46;
            this.lbProdLine.Text = "Prod Line";
            this.lbProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbHeaterCurStatus
            // 
            this.lbHeaterCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbHeaterCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbHeaterCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeaterCurStatus.Location = new System.Drawing.Point(611, 153);
            this.lbHeaterCurStatus.Name = "lbHeaterCurStatus";
            this.lbHeaterCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbHeaterCurStatus.TabIndex = 50;
            this.lbHeaterCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnHeaterComplete
            // 
            this.btnHeaterComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnHeaterComplete.Enabled = false;
            this.btnHeaterComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHeaterComplete.ForeColor = System.Drawing.Color.Black;
            this.btnHeaterComplete.Location = new System.Drawing.Point(480, 153);
            this.btnHeaterComplete.Name = "btnHeaterComplete";
            this.btnHeaterComplete.Size = new System.Drawing.Size(80, 25);
            this.btnHeaterComplete.TabIndex = 49;
            this.btnHeaterComplete.Text = "Complete";
            this.btnHeaterComplete.UseVisualStyleBackColor = false;
            this.btnHeaterComplete.Click += new System.EventHandler(this.btnHeaterComplete_Click);
            // 
            // btnHeaterPause
            // 
            this.btnHeaterPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnHeaterPause.Enabled = false;
            this.btnHeaterPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHeaterPause.ForeColor = System.Drawing.Color.Black;
            this.btnHeaterPause.Location = new System.Drawing.Point(355, 153);
            this.btnHeaterPause.Name = "btnHeaterPause";
            this.btnHeaterPause.Size = new System.Drawing.Size(80, 25);
            this.btnHeaterPause.TabIndex = 48;
            this.btnHeaterPause.Text = "Pause";
            this.btnHeaterPause.UseVisualStyleBackColor = false;
            this.btnHeaterPause.Click += new System.EventHandler(this.btnHeaterPause_Click);
            // 
            // btnHeaterStart
            // 
            this.btnHeaterStart.BackColor = System.Drawing.Color.Lime;
            this.btnHeaterStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHeaterStart.ForeColor = System.Drawing.Color.Black;
            this.btnHeaterStart.Location = new System.Drawing.Point(230, 153);
            this.btnHeaterStart.Name = "btnHeaterStart";
            this.btnHeaterStart.Size = new System.Drawing.Size(80, 25);
            this.btnHeaterStart.TabIndex = 47;
            this.btnHeaterStart.Text = "Start";
            this.btnHeaterStart.UseVisualStyleBackColor = false;
            this.btnHeaterStart.Click += new System.EventHandler(this.btnHeaterStart_Click);
            // 
            // lbDoorPanelsCurStatus
            // 
            this.lbDoorPanelsCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbDoorPanelsCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbDoorPanelsCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDoorPanelsCurStatus.Location = new System.Drawing.Point(611, 193);
            this.lbDoorPanelsCurStatus.Name = "lbDoorPanelsCurStatus";
            this.lbDoorPanelsCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbDoorPanelsCurStatus.TabIndex = 54;
            this.lbDoorPanelsCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDoorPanelComplete
            // 
            this.btnDoorPanelComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnDoorPanelComplete.Enabled = false;
            this.btnDoorPanelComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoorPanelComplete.ForeColor = System.Drawing.Color.Black;
            this.btnDoorPanelComplete.Location = new System.Drawing.Point(480, 193);
            this.btnDoorPanelComplete.Name = "btnDoorPanelComplete";
            this.btnDoorPanelComplete.Size = new System.Drawing.Size(80, 25);
            this.btnDoorPanelComplete.TabIndex = 53;
            this.btnDoorPanelComplete.Text = "Complete";
            this.btnDoorPanelComplete.UseVisualStyleBackColor = false;
            this.btnDoorPanelComplete.Click += new System.EventHandler(this.btnDoorPanelComplete_Click);
            // 
            // btnDoorPanelPause
            // 
            this.btnDoorPanelPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnDoorPanelPause.Enabled = false;
            this.btnDoorPanelPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoorPanelPause.ForeColor = System.Drawing.Color.Black;
            this.btnDoorPanelPause.Location = new System.Drawing.Point(355, 193);
            this.btnDoorPanelPause.Name = "btnDoorPanelPause";
            this.btnDoorPanelPause.Size = new System.Drawing.Size(80, 25);
            this.btnDoorPanelPause.TabIndex = 52;
            this.btnDoorPanelPause.Text = "Pause";
            this.btnDoorPanelPause.UseVisualStyleBackColor = false;
            this.btnDoorPanelPause.Click += new System.EventHandler(this.btnDoorPanelPause_Click);
            // 
            // btnDoorPanelStart
            // 
            this.btnDoorPanelStart.BackColor = System.Drawing.Color.Lime;
            this.btnDoorPanelStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDoorPanelStart.ForeColor = System.Drawing.Color.Black;
            this.btnDoorPanelStart.Location = new System.Drawing.Point(230, 193);
            this.btnDoorPanelStart.Name = "btnDoorPanelStart";
            this.btnDoorPanelStart.Size = new System.Drawing.Size(80, 25);
            this.btnDoorPanelStart.TabIndex = 51;
            this.btnDoorPanelStart.Text = "Start";
            this.btnDoorPanelStart.UseVisualStyleBackColor = false;
            this.btnDoorPanelStart.Click += new System.EventHandler(this.btnDoorPanelStart_Click);
            // 
            // lbAuxBoxCurStatus
            // 
            this.lbAuxBoxCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbAuxBoxCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAuxBoxCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxCurStatus.Location = new System.Drawing.Point(611, 233);
            this.lbAuxBoxCurStatus.Name = "lbAuxBoxCurStatus";
            this.lbAuxBoxCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbAuxBoxCurStatus.TabIndex = 58;
            this.lbAuxBoxCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAuxBoxComplete
            // 
            this.btnAuxBoxComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnAuxBoxComplete.Enabled = false;
            this.btnAuxBoxComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuxBoxComplete.ForeColor = System.Drawing.Color.Black;
            this.btnAuxBoxComplete.Location = new System.Drawing.Point(480, 233);
            this.btnAuxBoxComplete.Name = "btnAuxBoxComplete";
            this.btnAuxBoxComplete.Size = new System.Drawing.Size(80, 25);
            this.btnAuxBoxComplete.TabIndex = 57;
            this.btnAuxBoxComplete.Text = "Complete";
            this.btnAuxBoxComplete.UseVisualStyleBackColor = false;
            this.btnAuxBoxComplete.Click += new System.EventHandler(this.btnAuxBoxComplete_Click);
            // 
            // btnAuxBoxPause
            // 
            this.btnAuxBoxPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnAuxBoxPause.Enabled = false;
            this.btnAuxBoxPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuxBoxPause.ForeColor = System.Drawing.Color.Black;
            this.btnAuxBoxPause.Location = new System.Drawing.Point(355, 233);
            this.btnAuxBoxPause.Name = "btnAuxBoxPause";
            this.btnAuxBoxPause.Size = new System.Drawing.Size(80, 25);
            this.btnAuxBoxPause.TabIndex = 56;
            this.btnAuxBoxPause.Text = "Pause";
            this.btnAuxBoxPause.UseVisualStyleBackColor = false;
            this.btnAuxBoxPause.Click += new System.EventHandler(this.btnAuxBoxPause_Click);
            // 
            // btnAuxBoxStart
            // 
            this.btnAuxBoxStart.BackColor = System.Drawing.Color.Lime;
            this.btnAuxBoxStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuxBoxStart.ForeColor = System.Drawing.Color.Black;
            this.btnAuxBoxStart.Location = new System.Drawing.Point(230, 233);
            this.btnAuxBoxStart.Name = "btnAuxBoxStart";
            this.btnAuxBoxStart.Size = new System.Drawing.Size(80, 25);
            this.btnAuxBoxStart.TabIndex = 55;
            this.btnAuxBoxStart.Text = "Start";
            this.btnAuxBoxStart.UseVisualStyleBackColor = false;
            this.btnAuxBoxStart.Click += new System.EventHandler(this.btnAuxBoxStart_Click);
            // 
            // lbIntakeCurStatus
            // 
            this.lbIntakeCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbIntakeCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbIntakeCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIntakeCurStatus.Location = new System.Drawing.Point(611, 273);
            this.lbIntakeCurStatus.Name = "lbIntakeCurStatus";
            this.lbIntakeCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbIntakeCurStatus.TabIndex = 62;
            this.lbIntakeCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnIntakeComplete
            // 
            this.btnIntakeComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnIntakeComplete.Enabled = false;
            this.btnIntakeComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIntakeComplete.ForeColor = System.Drawing.Color.Black;
            this.btnIntakeComplete.Location = new System.Drawing.Point(480, 273);
            this.btnIntakeComplete.Name = "btnIntakeComplete";
            this.btnIntakeComplete.Size = new System.Drawing.Size(80, 25);
            this.btnIntakeComplete.TabIndex = 61;
            this.btnIntakeComplete.Text = "Complete";
            this.btnIntakeComplete.UseVisualStyleBackColor = false;
            this.btnIntakeComplete.Click += new System.EventHandler(this.btnIntakeComplete_Click);
            // 
            // btnIntakePause
            // 
            this.btnIntakePause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnIntakePause.Enabled = false;
            this.btnIntakePause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIntakePause.ForeColor = System.Drawing.Color.Black;
            this.btnIntakePause.Location = new System.Drawing.Point(355, 273);
            this.btnIntakePause.Name = "btnIntakePause";
            this.btnIntakePause.Size = new System.Drawing.Size(80, 25);
            this.btnIntakePause.TabIndex = 60;
            this.btnIntakePause.Text = "Pause";
            this.btnIntakePause.UseVisualStyleBackColor = false;
            this.btnIntakePause.Click += new System.EventHandler(this.btnIntakePause_Click);
            // 
            // btnIntakeStart
            // 
            this.btnIntakeStart.BackColor = System.Drawing.Color.Lime;
            this.btnIntakeStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIntakeStart.ForeColor = System.Drawing.Color.Black;
            this.btnIntakeStart.Location = new System.Drawing.Point(230, 273);
            this.btnIntakeStart.Name = "btnIntakeStart";
            this.btnIntakeStart.Size = new System.Drawing.Size(80, 25);
            this.btnIntakeStart.TabIndex = 59;
            this.btnIntakeStart.Text = "Start";
            this.btnIntakeStart.UseVisualStyleBackColor = false;
            this.btnIntakeStart.Click += new System.EventHandler(this.btnIntakeStart_Click);
            // 
            // lbBreakerCurStatus
            // 
            this.lbBreakerCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbBreakerCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbBreakerCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBreakerCurStatus.Location = new System.Drawing.Point(611, 313);
            this.lbBreakerCurStatus.Name = "lbBreakerCurStatus";
            this.lbBreakerCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbBreakerCurStatus.TabIndex = 66;
            this.lbBreakerCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBreakerComplete
            // 
            this.btnBreakerComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnBreakerComplete.Enabled = false;
            this.btnBreakerComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBreakerComplete.ForeColor = System.Drawing.Color.Black;
            this.btnBreakerComplete.Location = new System.Drawing.Point(480, 313);
            this.btnBreakerComplete.Name = "btnBreakerComplete";
            this.btnBreakerComplete.Size = new System.Drawing.Size(80, 25);
            this.btnBreakerComplete.TabIndex = 65;
            this.btnBreakerComplete.Text = "Complete";
            this.btnBreakerComplete.UseVisualStyleBackColor = false;
            this.btnBreakerComplete.Click += new System.EventHandler(this.btnBreakerComplete_Click);
            // 
            // btnBreakerPause
            // 
            this.btnBreakerPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnBreakerPause.Enabled = false;
            this.btnBreakerPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBreakerPause.ForeColor = System.Drawing.Color.Black;
            this.btnBreakerPause.Location = new System.Drawing.Point(355, 313);
            this.btnBreakerPause.Name = "btnBreakerPause";
            this.btnBreakerPause.Size = new System.Drawing.Size(80, 25);
            this.btnBreakerPause.TabIndex = 64;
            this.btnBreakerPause.Text = "Pause";
            this.btnBreakerPause.UseVisualStyleBackColor = false;
            this.btnBreakerPause.Click += new System.EventHandler(this.btnBreakerPause_Click);
            // 
            // btnBreakerStart
            // 
            this.btnBreakerStart.BackColor = System.Drawing.Color.Lime;
            this.btnBreakerStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBreakerStart.ForeColor = System.Drawing.Color.Black;
            this.btnBreakerStart.Location = new System.Drawing.Point(230, 313);
            this.btnBreakerStart.Name = "btnBreakerStart";
            this.btnBreakerStart.Size = new System.Drawing.Size(80, 25);
            this.btnBreakerStart.TabIndex = 63;
            this.btnBreakerStart.Text = "Start";
            this.btnBreakerStart.UseVisualStyleBackColor = false;
            this.btnBreakerStart.Click += new System.EventHandler(this.btnBreakerStart_Click);
            // 
            // lbTransformerCurStatus
            // 
            this.lbTransformerCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbTransformerCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTransformerCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTransformerCurStatus.Location = new System.Drawing.Point(611, 353);
            this.lbTransformerCurStatus.Name = "lbTransformerCurStatus";
            this.lbTransformerCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbTransformerCurStatus.TabIndex = 70;
            this.lbTransformerCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnTransComplete
            // 
            this.btnTransComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnTransComplete.Enabled = false;
            this.btnTransComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransComplete.ForeColor = System.Drawing.Color.Black;
            this.btnTransComplete.Location = new System.Drawing.Point(480, 353);
            this.btnTransComplete.Name = "btnTransComplete";
            this.btnTransComplete.Size = new System.Drawing.Size(80, 25);
            this.btnTransComplete.TabIndex = 69;
            this.btnTransComplete.Text = "Complete";
            this.btnTransComplete.UseVisualStyleBackColor = false;
            this.btnTransComplete.Click += new System.EventHandler(this.btnTransComplete_Click);
            // 
            // btnTransPause
            // 
            this.btnTransPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnTransPause.Enabled = false;
            this.btnTransPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransPause.ForeColor = System.Drawing.Color.Black;
            this.btnTransPause.Location = new System.Drawing.Point(355, 353);
            this.btnTransPause.Name = "btnTransPause";
            this.btnTransPause.Size = new System.Drawing.Size(80, 25);
            this.btnTransPause.TabIndex = 68;
            this.btnTransPause.Text = "Pause";
            this.btnTransPause.UseVisualStyleBackColor = false;
            this.btnTransPause.Click += new System.EventHandler(this.btnTransPause_Click);
            // 
            // btnTransStart
            // 
            this.btnTransStart.BackColor = System.Drawing.Color.Lime;
            this.btnTransStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransStart.ForeColor = System.Drawing.Color.Black;
            this.btnTransStart.Location = new System.Drawing.Point(230, 353);
            this.btnTransStart.Name = "btnTransStart";
            this.btnTransStart.Size = new System.Drawing.Size(80, 25);
            this.btnTransStart.TabIndex = 67;
            this.btnTransStart.Text = "Start";
            this.btnTransStart.UseVisualStyleBackColor = false;
            this.btnTransStart.Click += new System.EventHandler(this.btnTransStart_Click);
            // 
            // lbIndoorFanMtrCurStatus
            // 
            this.lbIndoorFanMtrCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbIndoorFanMtrCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbIndoorFanMtrCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIndoorFanMtrCurStatus.Location = new System.Drawing.Point(611, 393);
            this.lbIndoorFanMtrCurStatus.Name = "lbIndoorFanMtrCurStatus";
            this.lbIndoorFanMtrCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbIndoorFanMtrCurStatus.TabIndex = 74;
            this.lbIndoorFanMtrCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnInFanMtrComplete
            // 
            this.btnInFanMtrComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnInFanMtrComplete.Enabled = false;
            this.btnInFanMtrComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInFanMtrComplete.ForeColor = System.Drawing.Color.Black;
            this.btnInFanMtrComplete.Location = new System.Drawing.Point(480, 393);
            this.btnInFanMtrComplete.Name = "btnInFanMtrComplete";
            this.btnInFanMtrComplete.Size = new System.Drawing.Size(80, 25);
            this.btnInFanMtrComplete.TabIndex = 73;
            this.btnInFanMtrComplete.Text = "Complete";
            this.btnInFanMtrComplete.UseVisualStyleBackColor = false;
            this.btnInFanMtrComplete.Click += new System.EventHandler(this.btnInFanMtrComplete_Click);
            // 
            // btnInFanMtrPause
            // 
            this.btnInFanMtrPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnInFanMtrPause.Enabled = false;
            this.btnInFanMtrPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInFanMtrPause.ForeColor = System.Drawing.Color.Black;
            this.btnInFanMtrPause.Location = new System.Drawing.Point(355, 393);
            this.btnInFanMtrPause.Name = "btnInFanMtrPause";
            this.btnInFanMtrPause.Size = new System.Drawing.Size(80, 25);
            this.btnInFanMtrPause.TabIndex = 72;
            this.btnInFanMtrPause.Text = "Pause";
            this.btnInFanMtrPause.UseVisualStyleBackColor = false;
            this.btnInFanMtrPause.Click += new System.EventHandler(this.btnInFanMtrPause_Click);
            // 
            // btnInFanMtrStart
            // 
            this.btnInFanMtrStart.BackColor = System.Drawing.Color.Lime;
            this.btnInFanMtrStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInFanMtrStart.ForeColor = System.Drawing.Color.Black;
            this.btnInFanMtrStart.Location = new System.Drawing.Point(230, 393);
            this.btnInFanMtrStart.Name = "btnInFanMtrStart";
            this.btnInFanMtrStart.Size = new System.Drawing.Size(80, 25);
            this.btnInFanMtrStart.TabIndex = 71;
            this.btnInFanMtrStart.Text = "Start";
            this.btnInFanMtrStart.UseVisualStyleBackColor = false;
            this.btnInFanMtrStart.Click += new System.EventHandler(this.btnInFanMtrStart_Click);
            // 
            // lbPwrExMotorCurStatus
            // 
            this.lbPwrExMotorCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbPwrExMotorCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPwrExMotorCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPwrExMotorCurStatus.Location = new System.Drawing.Point(611, 433);
            this.lbPwrExMotorCurStatus.Name = "lbPwrExMotorCurStatus";
            this.lbPwrExMotorCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbPwrExMotorCurStatus.TabIndex = 78;
            this.lbPwrExMotorCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPwrExMotorComplete
            // 
            this.btnPwrExMotorComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPwrExMotorComplete.Enabled = false;
            this.btnPwrExMotorComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPwrExMotorComplete.ForeColor = System.Drawing.Color.Black;
            this.btnPwrExMotorComplete.Location = new System.Drawing.Point(480, 433);
            this.btnPwrExMotorComplete.Name = "btnPwrExMotorComplete";
            this.btnPwrExMotorComplete.Size = new System.Drawing.Size(80, 25);
            this.btnPwrExMotorComplete.TabIndex = 77;
            this.btnPwrExMotorComplete.Text = "Complete";
            this.btnPwrExMotorComplete.UseVisualStyleBackColor = false;
            this.btnPwrExMotorComplete.Click += new System.EventHandler(this.btnPwrExMotorComplete_Click);
            // 
            // btnPwrExMotorPause
            // 
            this.btnPwrExMotorPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPwrExMotorPause.Enabled = false;
            this.btnPwrExMotorPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPwrExMotorPause.ForeColor = System.Drawing.Color.Black;
            this.btnPwrExMotorPause.Location = new System.Drawing.Point(355, 433);
            this.btnPwrExMotorPause.Name = "btnPwrExMotorPause";
            this.btnPwrExMotorPause.Size = new System.Drawing.Size(80, 25);
            this.btnPwrExMotorPause.TabIndex = 76;
            this.btnPwrExMotorPause.Text = "Pause";
            this.btnPwrExMotorPause.UseVisualStyleBackColor = false;
            this.btnPwrExMotorPause.Click += new System.EventHandler(this.btnPwrExMotorPause_Click);
            // 
            // btnPwrExMotorStart
            // 
            this.btnPwrExMotorStart.BackColor = System.Drawing.Color.Lime;
            this.btnPwrExMotorStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPwrExMotorStart.ForeColor = System.Drawing.Color.Black;
            this.btnPwrExMotorStart.Location = new System.Drawing.Point(230, 433);
            this.btnPwrExMotorStart.Name = "btnPwrExMotorStart";
            this.btnPwrExMotorStart.Size = new System.Drawing.Size(80, 25);
            this.btnPwrExMotorStart.TabIndex = 75;
            this.btnPwrExMotorStart.Text = "Start";
            this.btnPwrExMotorStart.UseVisualStyleBackColor = false;
            this.btnPwrExMotorStart.Click += new System.EventHandler(this.btnPwrExMotorStart_Click);
            // 
            // lbCondFanMotorCurStatus
            // 
            this.lbCondFanMotorCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbCondFanMotorCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCondFanMotorCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCondFanMotorCurStatus.Location = new System.Drawing.Point(611, 473);
            this.lbCondFanMotorCurStatus.Name = "lbCondFanMotorCurStatus";
            this.lbCondFanMotorCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbCondFanMotorCurStatus.TabIndex = 82;
            this.lbCondFanMotorCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCondFanMotorComplete
            // 
            this.btnCondFanMotorComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCondFanMotorComplete.Enabled = false;
            this.btnCondFanMotorComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCondFanMotorComplete.ForeColor = System.Drawing.Color.Black;
            this.btnCondFanMotorComplete.Location = new System.Drawing.Point(480, 473);
            this.btnCondFanMotorComplete.Name = "btnCondFanMotorComplete";
            this.btnCondFanMotorComplete.Size = new System.Drawing.Size(80, 25);
            this.btnCondFanMotorComplete.TabIndex = 81;
            this.btnCondFanMotorComplete.Text = "Complete";
            this.btnCondFanMotorComplete.UseVisualStyleBackColor = false;
            this.btnCondFanMotorComplete.Click += new System.EventHandler(this.btnCondFanMotorComplete_Click);
            // 
            // btnCondFanMotorPause
            // 
            this.btnCondFanMotorPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCondFanMotorPause.Enabled = false;
            this.btnCondFanMotorPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCondFanMotorPause.ForeColor = System.Drawing.Color.Black;
            this.btnCondFanMotorPause.Location = new System.Drawing.Point(355, 473);
            this.btnCondFanMotorPause.Name = "btnCondFanMotorPause";
            this.btnCondFanMotorPause.Size = new System.Drawing.Size(80, 25);
            this.btnCondFanMotorPause.TabIndex = 80;
            this.btnCondFanMotorPause.Text = "Pause";
            this.btnCondFanMotorPause.UseVisualStyleBackColor = false;
            this.btnCondFanMotorPause.Click += new System.EventHandler(this.btnCondFanMotorPause_Click);
            // 
            // btnCondFanMotorStart
            // 
            this.btnCondFanMotorStart.BackColor = System.Drawing.Color.Lime;
            this.btnCondFanMotorStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCondFanMotorStart.ForeColor = System.Drawing.Color.Black;
            this.btnCondFanMotorStart.Location = new System.Drawing.Point(230, 473);
            this.btnCondFanMotorStart.Name = "btnCondFanMotorStart";
            this.btnCondFanMotorStart.Size = new System.Drawing.Size(80, 25);
            this.btnCondFanMotorStart.TabIndex = 79;
            this.btnCondFanMotorStart.Text = "Start";
            this.btnCondFanMotorStart.UseVisualStyleBackColor = false;
            this.btnCondFanMotorStart.Click += new System.EventHandler(this.btnCondFanMotorStart_Click);
            // 
            // lbElecKitsCurStatus
            // 
            this.lbElecKitsCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbElecKitsCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbElecKitsCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbElecKitsCurStatus.Location = new System.Drawing.Point(611, 513);
            this.lbElecKitsCurStatus.Name = "lbElecKitsCurStatus";
            this.lbElecKitsCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbElecKitsCurStatus.TabIndex = 86;
            this.lbElecKitsCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnElecKitComplete
            // 
            this.btnElecKitComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnElecKitComplete.Enabled = false;
            this.btnElecKitComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElecKitComplete.ForeColor = System.Drawing.Color.Black;
            this.btnElecKitComplete.Location = new System.Drawing.Point(480, 513);
            this.btnElecKitComplete.Name = "btnElecKitComplete";
            this.btnElecKitComplete.Size = new System.Drawing.Size(80, 25);
            this.btnElecKitComplete.TabIndex = 85;
            this.btnElecKitComplete.Text = "Complete";
            this.btnElecKitComplete.UseVisualStyleBackColor = false;
            this.btnElecKitComplete.Click += new System.EventHandler(this.btnElecKitComplete_Click);
            // 
            // btnElecKitPause
            // 
            this.btnElecKitPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnElecKitPause.Enabled = false;
            this.btnElecKitPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElecKitPause.ForeColor = System.Drawing.Color.Black;
            this.btnElecKitPause.Location = new System.Drawing.Point(355, 513);
            this.btnElecKitPause.Name = "btnElecKitPause";
            this.btnElecKitPause.Size = new System.Drawing.Size(80, 25);
            this.btnElecKitPause.TabIndex = 84;
            this.btnElecKitPause.Text = "Pause";
            this.btnElecKitPause.UseVisualStyleBackColor = false;
            this.btnElecKitPause.Click += new System.EventHandler(this.btnElecKitPause_Click);
            // 
            // btnElecKitStart
            // 
            this.btnElecKitStart.BackColor = System.Drawing.Color.Lime;
            this.btnElecKitStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnElecKitStart.ForeColor = System.Drawing.Color.Black;
            this.btnElecKitStart.Location = new System.Drawing.Point(230, 513);
            this.btnElecKitStart.Name = "btnElecKitStart";
            this.btnElecKitStart.Size = new System.Drawing.Size(80, 25);
            this.btnElecKitStart.TabIndex = 83;
            this.btnElecKitStart.Text = "Start";
            this.btnElecKitStart.UseVisualStyleBackColor = false;
            this.btnElecKitStart.Click += new System.EventHandler(this.btnElecKitStart_Click);
            // 
            // lbRfgKitsCurStatus
            // 
            this.lbRfgKitsCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbRfgKitsCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbRfgKitsCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRfgKitsCurStatus.Location = new System.Drawing.Point(611, 553);
            this.lbRfgKitsCurStatus.Name = "lbRfgKitsCurStatus";
            this.lbRfgKitsCurStatus.Size = new System.Drawing.Size(226, 23);
            this.lbRfgKitsCurStatus.TabIndex = 90;
            this.lbRfgKitsCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRfgKitComplete
            // 
            this.btnRfgKitComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnRfgKitComplete.Enabled = false;
            this.btnRfgKitComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRfgKitComplete.ForeColor = System.Drawing.Color.Black;
            this.btnRfgKitComplete.Location = new System.Drawing.Point(480, 553);
            this.btnRfgKitComplete.Name = "btnRfgKitComplete";
            this.btnRfgKitComplete.Size = new System.Drawing.Size(80, 25);
            this.btnRfgKitComplete.TabIndex = 89;
            this.btnRfgKitComplete.Text = "Complete";
            this.btnRfgKitComplete.UseVisualStyleBackColor = false;
            this.btnRfgKitComplete.Click += new System.EventHandler(this.btnRfgKitComplete_Click);
            // 
            // btnRfgKitPause
            // 
            this.btnRfgKitPause.BackColor = System.Drawing.Color.Gainsboro;
            this.btnRfgKitPause.Enabled = false;
            this.btnRfgKitPause.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRfgKitPause.ForeColor = System.Drawing.Color.Black;
            this.btnRfgKitPause.Location = new System.Drawing.Point(355, 553);
            this.btnRfgKitPause.Name = "btnRfgKitPause";
            this.btnRfgKitPause.Size = new System.Drawing.Size(80, 25);
            this.btnRfgKitPause.TabIndex = 88;
            this.btnRfgKitPause.Text = "Pause";
            this.btnRfgKitPause.UseVisualStyleBackColor = false;
            this.btnRfgKitPause.Click += new System.EventHandler(this.btnRfgKitPause_Click);
            // 
            // btnRfgKitStart
            // 
            this.btnRfgKitStart.BackColor = System.Drawing.Color.Lime;
            this.btnRfgKitStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRfgKitStart.ForeColor = System.Drawing.Color.Black;
            this.btnRfgKitStart.Location = new System.Drawing.Point(230, 553);
            this.btnRfgKitStart.Name = "btnRfgKitStart";
            this.btnRfgKitStart.Size = new System.Drawing.Size(80, 25);
            this.btnRfgKitStart.TabIndex = 87;
            this.btnRfgKitStart.Text = "Start";
            this.btnRfgKitStart.UseVisualStyleBackColor = false;
            this.btnRfgKitStart.Click += new System.EventHandler(this.btnRfgKitStart_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(718, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(119, 23);
            this.btnExit.TabIndex = 91;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(166, 12);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(546, 39);
            this.label26.TabIndex = 92;
            this.label26.Text = "SubAssembly Status";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(12, 593);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(210, 22);
            this.label15.TabIndex = 93;
            this.label15.Text = "Other Comments:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rtbOtherComments
            // 
            this.rtbOtherComments.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbOtherComments.Location = new System.Drawing.Point(230, 593);
            this.rtbOtherComments.Name = "rtbOtherComments";
            this.rtbOtherComments.Size = new System.Drawing.Size(607, 83);
            this.rtbOtherComments.TabIndex = 94;
            this.rtbOtherComments.Text = "";
            this.rtbOtherComments.Leave += new System.EventHandler(this.rtbOtherComments_Leave);
            // 
            // btnModelNoConfig
            // 
            this.btnModelNoConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoConfig.ForeColor = System.Drawing.Color.Blue;
            this.btnModelNoConfig.Location = new System.Drawing.Point(718, 41);
            this.btnModelNoConfig.Name = "btnModelNoConfig";
            this.btnModelNoConfig.Size = new System.Drawing.Size(119, 23);
            this.btnModelNoConfig.TabIndex = 95;
            this.btnModelNoConfig.Text = "ModelNo Config";
            this.btnModelNoConfig.UseVisualStyleBackColor = true;
            this.btnModelNoConfig.Click += new System.EventHandler(this.btnModelNoConfig_Click);
            // 
            // lbModelNo
            // 
            this.lbModelNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNo.Location = new System.Drawing.Point(41, 26);
            this.lbModelNo.Name = "lbModelNo";
            this.lbModelNo.Size = new System.Drawing.Size(113, 23);
            this.lbModelNo.TabIndex = 96;
            this.lbModelNo.Text = "ModelNo";
            this.lbModelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbModelNo.Visible = false;
            // 
            // lbCustName
            // 
            this.lbCustName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCustName.Location = new System.Drawing.Point(41, 46);
            this.lbCustName.Name = "lbCustName";
            this.lbCustName.Size = new System.Drawing.Size(113, 29);
            this.lbCustName.TabIndex = 97;
            this.lbCustName.Text = "CustomerName";
            this.lbCustName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbCustName.Visible = false;
            // 
            // frmSubAsmStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 688);
            this.Controls.Add(this.lbCustName);
            this.Controls.Add(this.lbModelNo);
            this.Controls.Add(this.btnModelNoConfig);
            this.Controls.Add(this.rtbOtherComments);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbRfgKitsCurStatus);
            this.Controls.Add(this.btnRfgKitComplete);
            this.Controls.Add(this.btnRfgKitPause);
            this.Controls.Add(this.btnRfgKitStart);
            this.Controls.Add(this.lbElecKitsCurStatus);
            this.Controls.Add(this.btnElecKitComplete);
            this.Controls.Add(this.btnElecKitPause);
            this.Controls.Add(this.btnElecKitStart);
            this.Controls.Add(this.lbCondFanMotorCurStatus);
            this.Controls.Add(this.btnCondFanMotorComplete);
            this.Controls.Add(this.btnCondFanMotorPause);
            this.Controls.Add(this.btnCondFanMotorStart);
            this.Controls.Add(this.lbPwrExMotorCurStatus);
            this.Controls.Add(this.btnPwrExMotorComplete);
            this.Controls.Add(this.btnPwrExMotorPause);
            this.Controls.Add(this.btnPwrExMotorStart);
            this.Controls.Add(this.lbIndoorFanMtrCurStatus);
            this.Controls.Add(this.btnInFanMtrComplete);
            this.Controls.Add(this.btnInFanMtrPause);
            this.Controls.Add(this.btnInFanMtrStart);
            this.Controls.Add(this.lbTransformerCurStatus);
            this.Controls.Add(this.btnTransComplete);
            this.Controls.Add(this.btnTransPause);
            this.Controls.Add(this.btnTransStart);
            this.Controls.Add(this.lbBreakerCurStatus);
            this.Controls.Add(this.btnBreakerComplete);
            this.Controls.Add(this.btnBreakerPause);
            this.Controls.Add(this.btnBreakerStart);
            this.Controls.Add(this.lbIntakeCurStatus);
            this.Controls.Add(this.btnIntakeComplete);
            this.Controls.Add(this.btnIntakePause);
            this.Controls.Add(this.btnIntakeStart);
            this.Controls.Add(this.lbAuxBoxCurStatus);
            this.Controls.Add(this.btnAuxBoxComplete);
            this.Controls.Add(this.btnAuxBoxPause);
            this.Controls.Add(this.btnAuxBoxStart);
            this.Controls.Add(this.lbDoorPanelsCurStatus);
            this.Controls.Add(this.btnDoorPanelComplete);
            this.Controls.Add(this.btnDoorPanelPause);
            this.Controls.Add(this.btnDoorPanelStart);
            this.Controls.Add(this.lbHeaterCurStatus);
            this.Controls.Add(this.btnHeaterComplete);
            this.Controls.Add(this.btnHeaterPause);
            this.Controls.Add(this.btnHeaterStart);
            this.Controls.Add(this.lbProdLine);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lbCoolingWallsCurStatus);
            this.Controls.Add(this.btnCoolingWallsComplete);
            this.Controls.Add(this.btnCoolingWallsPause);
            this.Controls.Add(this.btnCoolingWallsStart);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbJobNum);
            this.Name = "frmSubAsmStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SubAssembly Status";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbProdLine;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.RichTextBox rtbOtherComments;
        public System.Windows.Forms.Button btnCoolingWallsStart;
        public System.Windows.Forms.Button btnCoolingWallsPause;
        public System.Windows.Forms.Button btnCoolingWallsComplete;
        public System.Windows.Forms.Button btnHeaterComplete;
        public System.Windows.Forms.Button btnHeaterPause;
        public System.Windows.Forms.Button btnHeaterStart;
        public System.Windows.Forms.Button btnDoorPanelComplete;
        public System.Windows.Forms.Button btnDoorPanelPause;
        public System.Windows.Forms.Button btnDoorPanelStart;
        public System.Windows.Forms.Button btnAuxBoxComplete;
        public System.Windows.Forms.Button btnAuxBoxPause;
        public System.Windows.Forms.Button btnAuxBoxStart;
        public System.Windows.Forms.Button btnIntakeComplete;
        public System.Windows.Forms.Button btnIntakePause;
        public System.Windows.Forms.Button btnIntakeStart;
        public System.Windows.Forms.Button btnBreakerComplete;
        public System.Windows.Forms.Button btnBreakerPause;
        public System.Windows.Forms.Button btnBreakerStart;
        public System.Windows.Forms.Button btnTransComplete;
        public System.Windows.Forms.Button btnTransPause;
        public System.Windows.Forms.Button btnTransStart;
        public System.Windows.Forms.Button btnInFanMtrComplete;
        public System.Windows.Forms.Button btnInFanMtrPause;
        public System.Windows.Forms.Button btnInFanMtrStart;
        public System.Windows.Forms.Button btnPwrExMotorComplete;
        public System.Windows.Forms.Button btnPwrExMotorPause;
        public System.Windows.Forms.Button btnPwrExMotorStart;
        public System.Windows.Forms.Button btnCondFanMotorComplete;
        public System.Windows.Forms.Button btnCondFanMotorPause;
        public System.Windows.Forms.Button btnCondFanMotorStart;
        public System.Windows.Forms.Button btnElecKitComplete;
        public System.Windows.Forms.Button btnElecKitPause;
        public System.Windows.Forms.Button btnElecKitStart;
        public System.Windows.Forms.Button btnRfgKitComplete;
        public System.Windows.Forms.Button btnRfgKitPause;
        public System.Windows.Forms.Button btnRfgKitStart;
        public System.Windows.Forms.Label lbCoolingWallsCurStatus;
        public System.Windows.Forms.Label lbHeaterCurStatus;
        public System.Windows.Forms.Label lbDoorPanelsCurStatus;
        public System.Windows.Forms.Label lbAuxBoxCurStatus;
        public System.Windows.Forms.Label lbIntakeCurStatus;
        public System.Windows.Forms.Label lbBreakerCurStatus;
        public System.Windows.Forms.Label lbTransformerCurStatus;
        public System.Windows.Forms.Label lbIndoorFanMtrCurStatus;
        public System.Windows.Forms.Label lbPwrExMotorCurStatus;
        public System.Windows.Forms.Label lbCondFanMotorCurStatus;
        public System.Windows.Forms.Label lbElecKitsCurStatus;
        public System.Windows.Forms.Label lbRfgKitsCurStatus;
        private System.Windows.Forms.Button btnModelNoConfig;
        public System.Windows.Forms.Label lbModelNo;
        public System.Windows.Forms.Label lbCustName;
    }
}
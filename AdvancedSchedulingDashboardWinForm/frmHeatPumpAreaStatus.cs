﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvSchedDashWinForm
{
    public partial class frmHeatPumpAreaStatus : Form
    {
        MainBO objMain = new MainBO();
        bool JobExist = false;

        public frmHeatPumpAreaStatus()
        {           
            InitializeComponent();            
        }

        private void frmHeatPumpAreaStatus_Load(object sender, EventArgs e)
        {
            objMain.JobNum = lbJobNum.Text;

            JobExist = GetHeatPumpAreaStatusData();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Metal
        private void btnMetalOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
           
            DateTime transDateTime = DateTime.Now;

            btnMetalOrdered.Enabled = false;
            btnMetalOrdered.BackColor = Color.Gainsboro;
            btnMetalInProgress.Enabled = true;
            btnMetalInProgress.BackColor = Color.Yellow;
            btnMetalComplete.Enabled = true;
            btnMetalComplete.BackColor = Color.Red;

            lbMetalCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbMetalCurStatus.BackColor = Color.Lime;
           
            txtMetalNotes.Text = objMain.MetalNotes;           
            txtMetalNotes.BackColor = Color.Lime;           

            objMain.Metal = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Metal";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnMetalInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
            
            DateTime transDateTime = DateTime.Now;
           
            btnMetalOrdered.Enabled = false;
            btnMetalOrdered.BackColor = Color.Gainsboro;
            btnMetalInProgress.Enabled = false;
            btnMetalInProgress.BackColor = Color.Gainsboro;
            btnMetalComplete.Enabled = true;
            btnMetalComplete.BackColor = Color.Red;

            lbMetalCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbMetalCurStatus.BackColor = Color.Yellow;

            txtMetalNotes.Text = objMain.MetalNotes;
            txtMetalNotes.BackColor = Color.Yellow;           

            objMain.Metal = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Metal";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnMetalComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
            
            DateTime transDateTime = DateTime.Now;

            btnMetalOrdered.Enabled = false;
            btnMetalOrdered.BackColor = Color.Gainsboro;
            btnMetalInProgress.Enabled = false;
            btnMetalInProgress.BackColor = Color.Gainsboro;
            btnMetalComplete.Enabled = false;
            btnMetalComplete.BackColor = Color.Gainsboro;

            lbMetalCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbMetalCurStatus.BackColor = Color.Red;

            txtMetalNotes.Text = objMain.MetalNotes;
            txtMetalNotes.BackColor = Color.Red;
            
            objMain.Metal = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Metal";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtMetalNotes_Leave(object sender, EventArgs e)
        {                       
            objMain.MetalNotes = txtMetalNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }       

        #endregion

        #region Coils
        private void btnCoilsOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }            

            DateTime transDateTime = DateTime.Now;

            btnCoilsOrdered.Enabled = false;
            btnCoilsOrdered.BackColor = Color.Gainsboro;
            btnCoilsInProgress.Enabled = true;
            btnCoilsInProgress.BackColor = Color.Yellow;
            btnCoilsComplete.Enabled = true;
            btnCoilsComplete.BackColor = Color.Red;            

            lbCoilsCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbCoilsCurStatus.BackColor = Color.Lime;

            txtCoilsNotes.Text = objMain.CoilNotes;
            txtCoilsNotes.BackColor = Color.Lime;            

            objMain.Coils = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Coils";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnCoilsInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnCoilsOrdered.Enabled = false;
            btnCoilsOrdered.BackColor = Color.Gainsboro;
            btnCoilsInProgress.Enabled = false;
            btnCoilsInProgress.BackColor = Color.Gainsboro;
            btnCoilsComplete.Enabled = true;
            btnCoilsComplete.BackColor = Color.Red;           

            lbCoilsCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbCoilsCurStatus.BackColor = Color.Yellow;

            txtCoilsNotes.Text = objMain.CoilNotes;
            txtCoilsNotes.BackColor = Color.Yellow;           

            objMain.Coils = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Coils";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnCoilsComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }          

            DateTime transDateTime = DateTime.Now;

            btnCoilsOrdered.Enabled = false;
            btnCoilsOrdered.BackColor = Color.Gainsboro;
            btnCoilsInProgress.Enabled = false;
            btnCoilsInProgress.BackColor = Color.Gainsboro;
            btnCoilsComplete.Enabled = false;
            btnCoilsComplete.BackColor = Color.Gainsboro;          

            lbCoilsCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbCoilsCurStatus.BackColor = Color.Red;

            txtCoilsNotes.Text = objMain.CoilNotes;
            txtCoilsNotes.BackColor = Color.Red;
            
            objMain.Coils = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Coils";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtCoilsNotes_Leave(object sender, EventArgs e)
        {
            objMain.CoilNotes = txtCoilsNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }

        #endregion

        #region OtherPurchasedParts
        private void btnOtherPP_Ordered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }            

            DateTime transDateTime = DateTime.Now;

            btnOtherPP_Ordered.Enabled = false;
            btnOtherPP_Ordered.BackColor = Color.Gainsboro;
            btnOtherPP_InProgress.Enabled = true;
            btnOtherPP_InProgress.BackColor = Color.Yellow;
            btnOtherPP_Complete.Enabled = true;
            btnOtherPP_Complete.BackColor = Color.Red;

            lbOtherPP_CurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbOtherPP_CurStatus.BackColor = Color.Lime;

            txtOtherPP_Notes.Text = objMain.OtherPP_Notes;
            txtOtherPP_Notes.BackColor = Color.Lime;
            
            objMain.OtherPurchParts = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "OtherPurchParts";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnOtherPP_InProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }          

            DateTime transDateTime = DateTime.Now;

            btnOtherPP_Ordered.Enabled = false;
            btnOtherPP_Ordered.BackColor = Color.Gainsboro;
            btnOtherPP_InProgress.Enabled = false;
            btnOtherPP_InProgress.BackColor = Color.Gainsboro;
            btnOtherPP_Complete.Enabled = true;
            btnOtherPP_Complete.BackColor = Color.Red;

            lbOtherPP_CurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbOtherPP_CurStatus.BackColor = Color.Yellow;

            txtOtherPP_Notes.Text = objMain.OtherPP_Notes;
            txtOtherPP_Notes.BackColor = Color.Yellow;

            objMain.OtherPurchParts = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "OtherPurchParts";
            objMain.TransType = "InProgrss";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnOther_PPComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
         

            DateTime transDateTime = DateTime.Now;

            btnOtherPP_Ordered.Enabled = false;
            btnOtherPP_Ordered.BackColor = Color.Gainsboro;
            btnOtherPP_InProgress.Enabled = false;
            btnOtherPP_InProgress.BackColor = Color.Gainsboro;
            btnOtherPP_Complete.Enabled = false;
            btnOtherPP_Complete.BackColor = Color.Gainsboro;

            lbOtherPP_CurStatus.Text = "Complete: " + transDateTime.ToString();
            lbOtherPP_CurStatus.BackColor = Color.Red;

            txtOtherPP_Notes.Text = objMain.OtherPP_Notes;
            txtOtherPP_Notes.BackColor = Color.Red;

            objMain.OtherPurchParts = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "OtherPurchParts";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtOtherPP_Notes_Leave(object sender, EventArgs e)
        {
            objMain.OtherPP_Notes = txtOtherPP_Notes.Text;
            objMain.UpdateHeatPumpStatus();
        }

        #endregion

        #region PartsMadeInHouse
        private void btnPartsMadeInHouseOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }        

            DateTime transDateTime = DateTime.Now;

            btnPartsMadeInHouseOrdered.Enabled = false;
            btnPartsMadeInHouseOrdered.BackColor = Color.Gainsboro;
            btnPartsMadeInHouseInProgress.Enabled = true;
            btnPartsMadeInHouseInProgress.BackColor = Color.Yellow;
            btnPartsMadeInHouseComplete.Enabled = true;
            btnPartsMadeInHouseComplete.BackColor = Color.Red;

            lbPartsMadeInHouseStatus.Text = "Ordered: " + transDateTime.ToString();
            lbPartsMadeInHouseStatus.BackColor = Color.Lime;

            txtPartsMadeInHouseNotes.Text = objMain.PartsMadeInHouseNotes;
            txtPartsMadeInHouseNotes.BackColor = Color.Lime;            

            objMain.PartsMadeInHouse = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PartsMadeInHouse";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnPartsMadeInHouseInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }           

            DateTime transDateTime = DateTime.Now;

            btnPartsMadeInHouseOrdered.Enabled = false;
            btnPartsMadeInHouseOrdered.BackColor = Color.Gainsboro;
            btnPartsMadeInHouseInProgress.Enabled = false;
            btnPartsMadeInHouseInProgress.BackColor = Color.Gainsboro;
            btnPartsMadeInHouseComplete.Enabled = true;
            btnPartsMadeInHouseComplete.BackColor = Color.Red;

            lbPartsMadeInHouseStatus.Text = "InProgress: " + transDateTime.ToString();
            lbPartsMadeInHouseStatus.BackColor = Color.Yellow;

            txtPartsMadeInHouseNotes.Text = objMain.PartsMadeInHouseNotes;
            txtPartsMadeInHouseNotes.BackColor = Color.Yellow;

            objMain.PartsMadeInHouse = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PartsMadeInHouse";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnPartsMadeInHouseComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }            

            DateTime transDateTime = DateTime.Now;

            btnPartsMadeInHouseOrdered.Enabled = false;
            btnPartsMadeInHouseOrdered.BackColor = Color.Gainsboro;
            btnPartsMadeInHouseInProgress.Enabled = false;
            btnPartsMadeInHouseInProgress.BackColor = Color.Gainsboro;
            btnPartsMadeInHouseComplete.Enabled = false;
            btnPartsMadeInHouseComplete.BackColor = Color.Gainsboro;

            lbPartsMadeInHouseStatus.Text = "Complete: " + transDateTime.ToString();
            lbPartsMadeInHouseStatus.BackColor = Color.Red;

            txtPartsMadeInHouseNotes.Text = objMain.PartsMadeInHouseNotes;
            txtPartsMadeInHouseNotes.BackColor = Color.Red;

            objMain.PartsMadeInHouse = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PartsMadeInHouse";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtPartsMadeInHouseNotes_Leave(object sender, EventArgs e)
        {
            objMain.PartsMadeInHouseNotes = txtPartsMadeInHouseNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }

        #endregion

        #region PreFabFitBraz
        private void btnPrefabFitBrazOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnPrefabFitBrazOrdered.Enabled = false;
            btnPrefabFitBrazOrdered.BackColor = Color.Gainsboro;
            btnPrefabFitBrazInProgress.Enabled = true;
            btnPrefabFitBrazInProgress.BackColor = Color.Yellow;
            btnPrefabFitBrazComplete.Enabled = true;
            btnPrefabFitBrazComplete.BackColor = Color.Red;

            lbPrefabFitBrazCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbPrefabFitBrazCurStatus.BackColor = Color.Lime;

            txtPrefabFitBrazNotes.Text = objMain.PrefabNotes;
            txtPrefabFitBrazNotes.BackColor = Color.Lime;

            objMain.PrefabFittingBrazing = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PrefabFittingBrazing";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnPrefabFitBrazInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }            

            DateTime transDateTime = DateTime.Now;

            btnPrefabFitBrazOrdered.Enabled = false;
            btnPrefabFitBrazOrdered.BackColor = Color.Gainsboro;
            btnPrefabFitBrazInProgress.Enabled = false;
            btnPrefabFitBrazInProgress.BackColor = Color.Gainsboro;
            btnPrefabFitBrazComplete.Enabled = true;
            btnPrefabFitBrazComplete.BackColor = Color.Red;

            lbPrefabFitBrazCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbPrefabFitBrazCurStatus.BackColor = Color.Yellow;

            txtPrefabFitBrazNotes.Text = objMain.PrefabNotes;
            txtPrefabFitBrazNotes.BackColor = Color.Yellow;

            objMain.PrefabFittingBrazing = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PrefabFittingBrazing";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnPrefabFitBrazComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
            
            DateTime transDateTime = DateTime.Now;

            btnPrefabFitBrazOrdered.Enabled = false;
            btnPrefabFitBrazOrdered.BackColor = Color.Gainsboro;
            btnPrefabFitBrazInProgress.Enabled = false;
            btnPrefabFitBrazInProgress.BackColor = Color.Gainsboro;
            btnPrefabFitBrazComplete.Enabled = false;
            btnPrefabFitBrazComplete.BackColor = Color.Gainsboro;

            lbPrefabFitBrazCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbPrefabFitBrazCurStatus.BackColor = Color.Red;

            txtPrefabFitBrazNotes.Text = objMain.PrefabNotes;
            txtPrefabFitBrazNotes.BackColor = Color.Red;

            JobExist = GetHeatPumpAreaStatusData();

            objMain.PrefabFittingBrazing = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "PrefabFittingBrazing";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtPrefabFitBrazNotes_Leave(object sender, EventArgs e)
        {
            objMain.PrefabNotes = txtPrefabFitBrazNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }

        #endregion

        #region Insulation
        private void btnInsulationOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }            

            DateTime transDateTime = DateTime.Now;

            btnInsulationOrdered.Enabled = false;
            btnInsulationOrdered.BackColor = Color.Gainsboro;
            btnInsulationInProgress.Enabled = true;
            btnInsulationInProgress.BackColor = Color.Yellow;
            btnInsulationComplete.Enabled = true;
            btnInsulationComplete.BackColor = Color.Red;

            lbInsulationCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbInsulationCurStatus.BackColor = Color.Lime;

            txtInsulationNotes.Text = objMain.InsulationNotes;
            txtInsulationNotes.BackColor = Color.Lime;

            objMain.Insulation = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Insulation";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnInsulationInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
            
            DateTime transDateTime = DateTime.Now;

            btnInsulationOrdered.Enabled = false;
            btnInsulationOrdered.BackColor = Color.Gainsboro;
            btnInsulationInProgress.Enabled = false;
            btnInsulationInProgress.BackColor = Color.Gainsboro;
            btnInsulationComplete.Enabled = true;
            btnInsulationComplete.BackColor = Color.Red;

            lbInsulationCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbInsulationCurStatus.BackColor = Color.Yellow;

            txtInsulationNotes.Text = objMain.InsulationNotes;
            txtInsulationNotes.BackColor = Color.Yellow;

            objMain.Insulation = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Insulation";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnInsulationComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
           
            DateTime transDateTime = DateTime.Now;

            btnInsulationOrdered.Enabled = false;
            btnInsulationOrdered.BackColor = Color.Gainsboro;
            btnInsulationInProgress.Enabled = false;
            btnInsulationInProgress.BackColor = Color.Gainsboro;
            btnInsulationComplete.Enabled = false;
            btnInsulationComplete.BackColor = Color.Gainsboro;

            lbInsulationCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbInsulationCurStatus.BackColor = Color.Red;

            txtInsulationNotes.Text = objMain.InsulationNotes;
            txtInsulationNotes.BackColor = Color.Red;

            objMain.Insulation = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Insulation";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtInsulationNotes_Leave(object sender, EventArgs e)
        {
            objMain.InsulationNotes = txtInsulationNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }

        #endregion

        #region Storage
        private void btnStorageOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
          
            DateTime transDateTime = DateTime.Now;

            btnStorageOrdered.Enabled = false;
            btnStorageOrdered.BackColor = Color.Gainsboro;
            btnStorageInProgress.Enabled = true;
            btnStorageInProgress.BackColor = Color.Yellow;
            btnStorageComplete.Enabled = true;
            btnStorageComplete.BackColor = Color.Red;

            lbStorageCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbStorageCurStatus.BackColor = Color.Lime;

            txtStorageNotes.Text = objMain.StorageNotes;
            txtStorageNotes.BackColor = Color.Lime;

            objMain.Storage = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Storage";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnStorageInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
          
            DateTime transDateTime = DateTime.Now;

            btnStorageOrdered.Enabled = false;
            btnStorageOrdered.BackColor = Color.Gainsboro;
            btnStorageInProgress.Enabled = false;
            btnStorageInProgress.BackColor = Color.Gainsboro;
            btnStorageComplete.Enabled = true;
            btnStorageComplete.BackColor = Color.Red;

            lbStorageCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbStorageCurStatus.BackColor = Color.Yellow;

            txtStorageNotes.Text = objMain.StorageNotes;
            txtStorageNotes.BackColor = Color.Yellow;

            objMain.Storage = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Storage";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnStorageComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }         

            DateTime transDateTime = DateTime.Now;

            btnStorageOrdered.Enabled = false;
            btnStorageOrdered.BackColor = Color.Gainsboro;
            btnStorageInProgress.Enabled = false;
            btnStorageInProgress.BackColor = Color.Gainsboro;
            btnStorageComplete.Enabled = false;
            btnStorageComplete.BackColor = Color.Gainsboro;

            lbStorageCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbStorageCurStatus.BackColor = Color.Red;

            txtStorageNotes.Text = objMain.StorageNotes;
            txtStorageNotes.BackColor = Color.Red;

            objMain.Storage = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "Storage";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtStorageNotes_Leave(object sender, EventArgs e)
        {
            objMain.StorageNotes = txtStorageNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }
        #endregion

        #region DeliveredTo
        private void btnDeliveredToLineOrdered_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }      

            DateTime transDateTime = DateTime.Now;

            btnDeliveredToLineOrdered.Enabled = false;
            btnDeliveredToLineOrdered.BackColor = Color.Gainsboro;
            btnDeliveredToLineInProgress.Enabled = true;
            btnDeliveredToLineInProgress.BackColor = Color.Yellow;
            btnDeliveredToLineComplete.Enabled = true;
            btnDeliveredToLineComplete.BackColor = Color.Red;

            lbDeliveredToLineCurStatus.Text = "Ordered: " + transDateTime.ToString();
            lbDeliveredToLineCurStatus.BackColor = Color.Lime;

            txtDeliveredToLineNotes.Text = objMain.DeliveredToLineNotes;
            txtDeliveredToLineNotes.BackColor = Color.Lime;

            objMain.DeliveredToLine = 4;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "DeliveredToLine";
            objMain.TransType = "Ordered";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnDeliveredToLineInProgress_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }         

            DateTime transDateTime = DateTime.Now;

            btnDeliveredToLineOrdered.Enabled = false;
            btnDeliveredToLineOrdered.BackColor = Color.Gainsboro;
            btnDeliveredToLineInProgress.Enabled = false;
            btnDeliveredToLineInProgress.BackColor = Color.Gainsboro;
            btnDeliveredToLineComplete.Enabled = true;
            btnDeliveredToLineComplete.BackColor = Color.Red;

            lbDeliveredToLineCurStatus.Text = "InProgress: " + transDateTime.ToString();
            lbDeliveredToLineCurStatus.BackColor = Color.Yellow;

            txtDeliveredToLineNotes.Text = objMain.DeliveredToLineNotes;
            txtDeliveredToLineNotes.BackColor = Color.Yellow;

            objMain.DeliveredToLine = 5;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "DeliveredToLine";
            objMain.TransType = "InProgress";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnDeliveredToLineComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnDeliveredToLineOrdered.Enabled = false;
            btnDeliveredToLineOrdered.BackColor = Color.Gainsboro;
            btnDeliveredToLineInProgress.Enabled = false;
            btnDeliveredToLineInProgress.BackColor = Color.Gainsboro;
            btnDeliveredToLineComplete.Enabled = false;
            btnDeliveredToLineComplete.BackColor = Color.Gainsboro;

            lbDeliveredToLineCurStatus.Text = "Complete: " + transDateTime.ToString();
            lbDeliveredToLineCurStatus.BackColor = Color.Red;

            txtDeliveredToLineNotes.Text = objMain.DeliveredToLineNotes;
            txtDeliveredToLineNotes.BackColor = Color.Red;

            objMain.DeliveredToLine = 3;

            if (JobExist == false)
            {
                objMain.InsertHeatPumpStatus();
                JobExist = true;
            }
            else
            {
                objMain.UpdateHeatPumpStatus();
            }

            objMain.SubAssemblyName = "DeliveredToLine";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void txtDeliveredToLineNotes_Leave(object sender, EventArgs e)
        {
            objMain.DeliveredToLineNotes = txtDeliveredToLineNotes.Text;
            objMain.UpdateHeatPumpStatus();
        }
        #endregion

        private bool GetHeatPumpAreaStatusData()
        {
            bool jobFound = false;

            DataTable dt = objMain.GetHeatPumpAreaStatusByJob();

            if (dt.Rows.Count == 0)
            {
                objMain.Metal = 0;
                objMain.MetalNotes = "";
                objMain.Coils = 0;
                objMain.CoilNotes = "";
                objMain.OtherPurchParts = 0;
                objMain.OtherPP_Notes = "";
                objMain.PartsMadeInHouse = 0;
                objMain.PartsMadeInHouseNotes = "";
                objMain.PrefabFittingBrazing = 0;
                objMain.PrefabNotes = "";
                objMain.Insulation = 0;
                objMain.InsulationNotes = "";
                objMain.Storage = 0;
                objMain.StorageNotes = "";
                objMain.DeliveredToLine = 0;
                objMain.DeliveredToLineNotes = "";
            }
            else
            {
                DataRow dr = dt.Rows[0];
                objMain.Metal = (int)dr["Metal"];
                objMain.MetalNotes = dr["MetalNotes"].ToString();
                objMain.Coils = (int)dr["Coils"];
                objMain.CoilNotes = dr["CoilNotes"].ToString();
                objMain.OtherPurchParts = (int)dr["OtherPurchParts"];
                objMain.OtherPP_Notes = dr["OtherPP_Notes"].ToString();
                objMain.PartsMadeInHouse = (int)dr["PartsMadeInHouse"];
                objMain.PartsMadeInHouseNotes = dr["PartsMadeInHouseNotes"].ToString();
                objMain.PrefabFittingBrazing = (int)dr["PrefabFittingBrazing"];
                objMain.PrefabNotes = dr["PrefabNotes"].ToString();
                objMain.Insulation = (int)dr["Insulation"];
                objMain.InsulationNotes = dr["InsulationNotes"].ToString();
                objMain.Storage = (int)dr["Storage"];
                objMain.StorageNotes = dr["StorageNotes"].ToString();
                objMain.DeliveredToLine = (int)dr["DeliveredToLine"];
                objMain.DeliveredToLineNotes = dr["DeliveredToLineNotes"].ToString();
                jobFound = true;
            }

            return jobFound;
        }
                                                    
    }
}

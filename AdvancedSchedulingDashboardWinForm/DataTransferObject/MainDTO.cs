﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvSchedDashWinForm
{
    public class MainDTO
    {
        public string GuidID { get; set; }
        public string JobNum { get; set; }
        public string Other { get; set; }
        public string PartNum { get; set; }
        public string PartDescription { get; set; }               
        public string PartQty { get; set; }
        public string RevisionNum { get; set; }
        public string SubAssemblyName { get; set; }        

        public string TransBy { get; set; }
        public string TransType { get; set; }

        public DateTime TransDate { get; set; }

        public decimal UnitOfCost { get; set; }        

        public int AssemblySeq { get; set; }
        public int AuxBox { get; set; }
        public int Breaker { get; set; }
        public int CondenserFanMotor { get; set; }
        public int CoolingWalls { get; set; }       
        public int DoorPanels { get; set; }
        public int ElectricalKit { get; set; }
        public int Heater { get; set; }
        public int IndoorFanMotor { get; set; }
        public int Intake { get; set; }
        public int PartQtyInt { get; set; }
        public int PoweredExhaustMotor { get; set; }
        public int RefrigerationKit { get; set; }
        public int SeqNum { get; set; }
        public int Transformer { get; set; }

        public decimal PartQtyDec { get; set; }


        public string MetalNotes { get; set; }
        public string CoilNotes { get; set; }
        public string OtherPP_Notes { get; set; }
        public string PartsMadeInHouseNotes { get; set; }
        public string PrefabNotes { get; set; }
        public string InsulationNotes { get; set; }
        public string StorageNotes { get; set; }
        public string DeliveredToLineNotes { get; set; }

        public int Metal { get; set; }
        public int Coils { get; set; }
        public int OtherPurchParts { get; set; }
        public int PartsMadeInHouse { get; set; }
        public int PrefabFittingBrazing { get; set; }
        public int Insulation { get; set; }
        public int Storage { get; set; }
        public int DeliveredToLine { get; set; }

    }
}

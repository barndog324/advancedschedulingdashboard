﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvSchedDashWinForm
{
    public class MainBO : MainDTO
    {
        #region Main SQL 
        public DataTable GetHeatPumpJobs()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;
                //MessageBox.Show("ConStr -> " + connectionString);
                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetHeatPumpJobs", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
             throw ex;
            }
            return dt;
        }

        public DataTable GetAuxBoxJobs()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetAuxBoxJobs", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPolyIsoJobs()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetPolyIsoJobs", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable Get65SCCRJobs()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_Get65SCCRJobs", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAssemblyList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetSubAssemblyList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetLineSize(string digit3, string digit4, string coolingCap, string digit11)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetLineSizeList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Digit3", digit3);
                        cmd.Parameters.AddWithValue("@Digit4", digit4);
                        cmd.Parameters.AddWithValue("@CoolingCap", coolingCap);
                        cmd.Parameters.AddWithValue("@Digit11", digit11);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetRev5ModelNoDigitDesc(int digitNo, string digitVal, string heatType, string OAUTypeCode)
        {
            DataTable dt = new DataTable();
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetRev5ModelNoDigitDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNo", digitNo);
                        cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@OAUTypeCode", OAUTypeCode);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetModelNumDesc(int digit, string heatType, string digitVal)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetModelNumDesc", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@DigitVal", digitVal);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAssemblyStatusByJob()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetSubAssemblyStatusByJob", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAssemblyHistoryByJob()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetSubAssemblyHistoryByJob", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@SubAssemblyName", SubAssemblyName);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertSubAssemblyHistory()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_InsertSubAssemblyHistory", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@SubAsmName", SubAssemblyName);
                        cmd.Parameters.AddWithValue("@TransType", TransType);
                        cmd.Parameters.AddWithValue("@TransDate", TransDate);
                        cmd.Parameters.AddWithValue("@TransBy", TransBy);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertSubAssemblyStatus()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_InsertSubAssemblyStatus", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@CoolingWalls", CoolingWalls);
                        cmd.Parameters.AddWithValue("@Heater", Heater);
                        cmd.Parameters.AddWithValue("@DoorPanels", DoorPanels);
                        cmd.Parameters.AddWithValue("@AuxBox", AuxBox);
                        cmd.Parameters.AddWithValue("@Intake", Intake);
                        cmd.Parameters.AddWithValue("@Breaker", Breaker);
                        cmd.Parameters.AddWithValue("@Transformer", Transformer);
                        cmd.Parameters.AddWithValue("@IndoorFanMotor", IndoorFanMotor);
                        cmd.Parameters.AddWithValue("@PoweredExhaustMotor", PoweredExhaustMotor);
                        cmd.Parameters.AddWithValue("@CondenserFanMotor", CondenserFanMotor);
                        cmd.Parameters.AddWithValue("@ElectricalKit", ElectricalKit);
                        cmd.Parameters.AddWithValue("@RefrigerationKit", RefrigerationKit);
                        cmd.Parameters.AddWithValue("@Other", Other);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSubAssemblyStatus()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_UpdateSubAssemblyStatus", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@CoolingWalls", CoolingWalls);
                        cmd.Parameters.AddWithValue("@Heater", Heater);
                        cmd.Parameters.AddWithValue("@DoorPanels", DoorPanels);
                        cmd.Parameters.AddWithValue("@AuxBox", AuxBox);
                        cmd.Parameters.AddWithValue("@Intake", Intake);
                        cmd.Parameters.AddWithValue("@Breaker", Breaker);
                        cmd.Parameters.AddWithValue("@Transformer", Transformer);
                        cmd.Parameters.AddWithValue("@IndoorFanMotor", IndoorFanMotor);
                        cmd.Parameters.AddWithValue("@PoweredExhaustMotor", PoweredExhaustMotor);
                        cmd.Parameters.AddWithValue("@CondenserFanMotor", CondenserFanMotor);
                        cmd.Parameters.AddWithValue("@ElectricalKit", ElectricalKit);
                        cmd.Parameters.AddWithValue("@RefrigerationKit", RefrigerationKit);
                        cmd.Parameters.AddWithValue("@Other", Other);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetHeatPumpAreaStatusByJob()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_GetHeatPumpAreaStatusByJob", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertHeatPumpStatus()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_InsertHeatPumpStatus", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@Metal", Metal);
                        cmd.Parameters.AddWithValue("@MetalNotes", MetalNotes);
                        cmd.Parameters.AddWithValue("@Coils", Coils);
                        cmd.Parameters.AddWithValue("@CoilNotes", CoilNotes);
                        cmd.Parameters.AddWithValue("@OtherPurchParts", OtherPurchParts);
                        cmd.Parameters.AddWithValue("@OtherPP_Notes", OtherPP_Notes);
                        cmd.Parameters.AddWithValue("@PartsMadeInHouse", PartsMadeInHouse);
                        cmd.Parameters.AddWithValue("@PartsMadeInHouseNotes", PartsMadeInHouseNotes);
                        cmd.Parameters.AddWithValue("@PrefabFittingBrazing", PrefabFittingBrazing);
                        cmd.Parameters.AddWithValue("@PrefabNotes", PrefabNotes);
                        cmd.Parameters.AddWithValue("@Insulation", Insulation);
                        cmd.Parameters.AddWithValue("@InsulationNotes", InsulationNotes);
                        cmd.Parameters.AddWithValue("@Storage", Storage);
                        cmd.Parameters.AddWithValue("@StorageNotes", StorageNotes);
                        cmd.Parameters.AddWithValue("@DeliveredToLine", DeliveredToLine);
                        cmd.Parameters.AddWithValue("@DeliveredToLineNotes", DeliveredToLineNotes);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateHeatPumpStatus()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.AdvSchd_UpdateHeatPumpStatus", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@Metal", Metal);
                        cmd.Parameters.AddWithValue("@MetalNotes", MetalNotes);
                        cmd.Parameters.AddWithValue("@Coils", Coils);
                        cmd.Parameters.AddWithValue("@CoilNotes", CoilNotes);
                        cmd.Parameters.AddWithValue("@OtherPurchParts", OtherPurchParts);
                        cmd.Parameters.AddWithValue("@OtherPP_Notes", OtherPP_Notes);
                        cmd.Parameters.AddWithValue("@PartsMadeInHouse", PartsMadeInHouse);
                        cmd.Parameters.AddWithValue("@PartsMadeInHouseNotes", PartsMadeInHouseNotes);
                        cmd.Parameters.AddWithValue("@PrefabFittingBrazing", PrefabFittingBrazing);
                        cmd.Parameters.AddWithValue("@PrefabNotes", PrefabNotes);
                        cmd.Parameters.AddWithValue("@Insulation", Insulation);
                        cmd.Parameters.AddWithValue("@InsulationNotes", InsulationNotes);
                        cmd.Parameters.AddWithValue("@Storage", Storage);
                        cmd.Parameters.AddWithValue("@StorageNotes", StorageNotes);
                        cmd.Parameters.AddWithValue("@DeliveredToLine", DeliveredToLine);
                        cmd.Parameters.AddWithValue("@DeliveredToLineNotes", DeliveredToLineNotes);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetOAU_ModelNoValues(int digit, string heatType, string oauTypeCode)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_ModelNoValues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DigitNum", digit);
                        cmd.Parameters.AddWithValue("@HeatType", heatType);
                        cmd.Parameters.AddWithValue("@OAUTypeCode", oauTypeCode);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        
        #endregion
    }
}

﻿namespace AdvSchedDashWinForm
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBoxCompLogo = new System.Windows.Forms.PictureBox();
            this.lbMainTitle = new System.Windows.Forms.Label();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageHeatPump = new System.Windows.Forms.TabPage();
            this.btnDisplayRfgCompRpt = new System.Windows.Forms.Button();
            this.lbHeatPumpTonage = new System.Windows.Forms.Label();
            this.lbHeatPumpModelNo2 = new System.Windows.Forms.Label();
            this.lbHeatPumpCurrentStatus = new System.Windows.Forms.Label();
            this.lbHeatPumpProdLine = new System.Windows.Forms.Label();
            this.lbHeatPumpStartDate = new System.Windows.Forms.Label();
            this.lbHeatPumpType = new System.Windows.Forms.Label();
            this.lbHeatPumpModelNo1 = new System.Windows.Forms.Label();
            this.lbHeatPumpJobNum = new System.Windows.Forms.Label();
            this.dgvHeatPump = new System.Windows.Forms.DataGridView();
            this.tabPageAuxBox = new System.Windows.Forms.TabPage();
            this.lbAuxBoxCustName = new System.Windows.Forms.Label();
            this.lbAuxBoxUnitType = new System.Windows.Forms.Label();
            this.btnAuxBoxDispCritCompRpt = new System.Windows.Forms.Button();
            this.lbAuxBoxCurStatus = new System.Windows.Forms.Label();
            this.lbAuxBoxProdLine = new System.Windows.Forms.Label();
            this.lbAuxBoxStartDate = new System.Windows.Forms.Label();
            this.lbAuxBoxSecMod = new System.Windows.Forms.Label();
            this.lbAuxBoxModelNo1 = new System.Windows.Forms.Label();
            this.lbAuxBoxJobNum = new System.Windows.Forms.Label();
            this.dgvAuxBox = new System.Windows.Forms.DataGridView();
            this.tabPagePolyIso = new System.Windows.Forms.TabPage();
            this.lbPolyIsoUnitType = new System.Windows.Forms.Label();
            this.lbPolyIsoAuxBox = new System.Windows.Forms.Label();
            this.lbPolyIsoModelNo2 = new System.Windows.Forms.Label();
            this.lbPolyIsoModelNo1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbPolyIsoATotal = new System.Windows.Forms.Label();
            this.lbPolyIsoTotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbPolyIsoCurStatus = new System.Windows.Forms.Label();
            this.lbPolyIsoProdLine = new System.Windows.Forms.Label();
            this.lbPolyIsoStartDate = new System.Windows.Forms.Label();
            this.lbPolyIsoJobNum = new System.Windows.Forms.Label();
            this.dgvPolyIso = new System.Windows.Forms.DataGridView();
            this.tabPageSpecialUnits = new System.Windows.Forms.TabPage();
            this.lbSpecialModelNo2 = new System.Windows.Forms.Label();
            this.lbSpecialCurStatus = new System.Windows.Forms.Label();
            this.lbSpecialProdLine = new System.Windows.Forms.Label();
            this.lbSpecialStartDate = new System.Windows.Forms.Label();
            this.lbSpecial65SCCR = new System.Windows.Forms.Label();
            this.lbSpecialModelNo1 = new System.Windows.Forms.Label();
            this.lbSpecialJobNum = new System.Windows.Forms.Label();
            this.dgvSpecial = new System.Windows.Forms.DataGridView();
            this.tabPageSubAsmSched = new System.Windows.Forms.TabPage();
            this.dgvSubAsmSchd = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.lbArea = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompLogo)).BeginInit();
            this.tabControlMain.SuspendLayout();
            this.tabPageHeatPump.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHeatPump)).BeginInit();
            this.tabPageAuxBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuxBox)).BeginInit();
            this.tabPagePolyIso.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPolyIso)).BeginInit();
            this.tabPageSpecialUnits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpecial)).BeginInit();
            this.tabPageSubAsmSched.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubAsmSchd)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCompLogo
            // 
            this.pictureBoxCompLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCompLogo.Image")));
            this.pictureBoxCompLogo.Location = new System.Drawing.Point(1, 2);
            this.pictureBoxCompLogo.Name = "pictureBoxCompLogo";
            this.pictureBoxCompLogo.Size = new System.Drawing.Size(300, 85);
            this.pictureBoxCompLogo.TabIndex = 0;
            this.pictureBoxCompLogo.TabStop = false;
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.ForeColor = System.Drawing.Color.Navy;
            this.lbMainTitle.Location = new System.Drawing.Point(425, 11);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(466, 31);
            this.lbMainTitle.TabIndex = 1;
            this.lbMainTitle.Text = "Advanced Scheduling Dashboard";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageHeatPump);
            this.tabControlMain.Controls.Add(this.tabPageAuxBox);
            this.tabControlMain.Controls.Add(this.tabPagePolyIso);
            this.tabControlMain.Controls.Add(this.tabPageSpecialUnits);
            this.tabControlMain.Controls.Add(this.tabPageSubAsmSched);
            this.tabControlMain.Location = new System.Drawing.Point(1, 94);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1262, 556);
            this.tabControlMain.TabIndex = 2;
            this.tabControlMain.SelectedIndexChanged += new System.EventHandler(this.tabControlMain_SelectedIndexChanged);
            // 
            // tabPageHeatPump
            // 
            this.tabPageHeatPump.Controls.Add(this.btnDisplayRfgCompRpt);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpTonage);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpModelNo2);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpCurrentStatus);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpProdLine);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpStartDate);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpType);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpModelNo1);
            this.tabPageHeatPump.Controls.Add(this.lbHeatPumpJobNum);
            this.tabPageHeatPump.Controls.Add(this.dgvHeatPump);
            this.tabPageHeatPump.Location = new System.Drawing.Point(4, 22);
            this.tabPageHeatPump.Name = "tabPageHeatPump";
            this.tabPageHeatPump.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHeatPump.Size = new System.Drawing.Size(1254, 530);
            this.tabPageHeatPump.TabIndex = 0;
            this.tabPageHeatPump.Text = "Heat Pumps";
            this.tabPageHeatPump.UseVisualStyleBackColor = true;
            // 
            // btnDisplayRfgCompRpt
            // 
            this.btnDisplayRfgCompRpt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisplayRfgCompRpt.ForeColor = System.Drawing.Color.Blue;
            this.btnDisplayRfgCompRpt.Location = new System.Drawing.Point(973, 382);
            this.btnDisplayRfgCompRpt.Name = "btnDisplayRfgCompRpt";
            this.btnDisplayRfgCompRpt.Size = new System.Drawing.Size(186, 49);
            this.btnDisplayRfgCompRpt.TabIndex = 15;
            this.btnDisplayRfgCompRpt.Text = "Display \r\nRfg Component Rpt";
            this.btnDisplayRfgCompRpt.UseVisualStyleBackColor = true;
            this.btnDisplayRfgCompRpt.Click += new System.EventHandler(this.btnDisplayRfgCompRpt_Click);
            // 
            // lbHeatPumpTonage
            // 
            this.lbHeatPumpTonage.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpTonage.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpTonage.Location = new System.Drawing.Point(869, 79);
            this.lbHeatPumpTonage.Name = "lbHeatPumpTonage";
            this.lbHeatPumpTonage.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpTonage.TabIndex = 14;
            this.lbHeatPumpTonage.Text = "Tonage";
            this.lbHeatPumpTonage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpModelNo2
            // 
            this.lbHeatPumpModelNo2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpModelNo2.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpModelNo2.Location = new System.Drawing.Point(869, 147);
            this.lbHeatPumpModelNo2.Name = "lbHeatPumpModelNo2";
            this.lbHeatPumpModelNo2.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpModelNo2.TabIndex = 13;
            this.lbHeatPumpModelNo2.Text = "ModelNo";
            this.lbHeatPumpModelNo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpCurrentStatus
            // 
            this.lbHeatPumpCurrentStatus.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpCurrentStatus.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpCurrentStatus.Location = new System.Drawing.Point(869, 322);
            this.lbHeatPumpCurrentStatus.Name = "lbHeatPumpCurrentStatus";
            this.lbHeatPumpCurrentStatus.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpCurrentStatus.TabIndex = 12;
            this.lbHeatPumpCurrentStatus.Text = "Current Status";
            this.lbHeatPumpCurrentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpProdLine
            // 
            this.lbHeatPumpProdLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpProdLine.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpProdLine.Location = new System.Drawing.Point(869, 279);
            this.lbHeatPumpProdLine.Name = "lbHeatPumpProdLine";
            this.lbHeatPumpProdLine.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpProdLine.TabIndex = 10;
            this.lbHeatPumpProdLine.Text = "Prod Line";
            this.lbHeatPumpProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpStartDate
            // 
            this.lbHeatPumpStartDate.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpStartDate.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpStartDate.Location = new System.Drawing.Point(869, 235);
            this.lbHeatPumpStartDate.Name = "lbHeatPumpStartDate";
            this.lbHeatPumpStartDate.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpStartDate.TabIndex = 8;
            this.lbHeatPumpStartDate.Text = "Start Date";
            this.lbHeatPumpStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpType
            // 
            this.lbHeatPumpType.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpType.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpType.Location = new System.Drawing.Point(869, 191);
            this.lbHeatPumpType.Name = "lbHeatPumpType";
            this.lbHeatPumpType.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpType.TabIndex = 6;
            this.lbHeatPumpType.Text = "Heat Pump Type";
            this.lbHeatPumpType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpModelNo1
            // 
            this.lbHeatPumpModelNo1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpModelNo1.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpModelNo1.Location = new System.Drawing.Point(869, 123);
            this.lbHeatPumpModelNo1.Name = "lbHeatPumpModelNo1";
            this.lbHeatPumpModelNo1.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpModelNo1.TabIndex = 4;
            this.lbHeatPumpModelNo1.Text = "ModelNo";
            this.lbHeatPumpModelNo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbHeatPumpJobNum
            // 
            this.lbHeatPumpJobNum.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeatPumpJobNum.ForeColor = System.Drawing.Color.Blue;
            this.lbHeatPumpJobNum.Location = new System.Drawing.Point(869, 35);
            this.lbHeatPumpJobNum.Name = "lbHeatPumpJobNum";
            this.lbHeatPumpJobNum.Size = new System.Drawing.Size(375, 23);
            this.lbHeatPumpJobNum.TabIndex = 2;
            this.lbHeatPumpJobNum.Text = "Job Num";
            this.lbHeatPumpJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvHeatPump
            // 
            this.dgvHeatPump.AllowUserToAddRows = false;
            this.dgvHeatPump.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvHeatPump.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHeatPump.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHeatPump.Location = new System.Drawing.Point(7, 6);
            this.dgvHeatPump.MultiSelect = false;
            this.dgvHeatPump.Name = "dgvHeatPump";
            this.dgvHeatPump.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHeatPump.Size = new System.Drawing.Size(856, 518);
            this.dgvHeatPump.TabIndex = 0;
            this.dgvHeatPump.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvHeatPump_CellMouseDoubleClick);
            this.dgvHeatPump.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvHeatPump_DataBindingComplete);
            // 
            // tabPageAuxBox
            // 
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxCustName);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxUnitType);
            this.tabPageAuxBox.Controls.Add(this.btnAuxBoxDispCritCompRpt);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxCurStatus);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxProdLine);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxStartDate);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxSecMod);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxModelNo1);
            this.tabPageAuxBox.Controls.Add(this.lbAuxBoxJobNum);
            this.tabPageAuxBox.Controls.Add(this.dgvAuxBox);
            this.tabPageAuxBox.Location = new System.Drawing.Point(4, 22);
            this.tabPageAuxBox.Name = "tabPageAuxBox";
            this.tabPageAuxBox.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAuxBox.Size = new System.Drawing.Size(1238, 530);
            this.tabPageAuxBox.TabIndex = 1;
            this.tabPageAuxBox.Text = "Aux Box";
            this.tabPageAuxBox.UseVisualStyleBackColor = true;
            // 
            // lbAuxBoxCustName
            // 
            this.lbAuxBoxCustName.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxCustName.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxCustName.Location = new System.Drawing.Point(880, 102);
            this.lbAuxBoxCustName.Name = "lbAuxBoxCustName";
            this.lbAuxBoxCustName.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxCustName.TabIndex = 37;
            this.lbAuxBoxCustName.Text = "Customer Name";
            this.lbAuxBoxCustName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxUnitType
            // 
            this.lbAuxBoxUnitType.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxUnitType.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxUnitType.Location = new System.Drawing.Point(880, 139);
            this.lbAuxBoxUnitType.Name = "lbAuxBoxUnitType";
            this.lbAuxBoxUnitType.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxUnitType.TabIndex = 36;
            this.lbAuxBoxUnitType.Text = "UnitType";
            this.lbAuxBoxUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAuxBoxDispCritCompRpt
            // 
            this.btnAuxBoxDispCritCompRpt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuxBoxDispCritCompRpt.ForeColor = System.Drawing.Color.Blue;
            this.btnAuxBoxDispCritCompRpt.Location = new System.Drawing.Point(927, 356);
            this.btnAuxBoxDispCritCompRpt.Name = "btnAuxBoxDispCritCompRpt";
            this.btnAuxBoxDispCritCompRpt.Size = new System.Drawing.Size(186, 49);
            this.btnAuxBoxDispCritCompRpt.TabIndex = 21;
            this.btnAuxBoxDispCritCompRpt.Text = "Display \r\nCritical Component Rpt";
            this.btnAuxBoxDispCritCompRpt.UseVisualStyleBackColor = true;
            this.btnAuxBoxDispCritCompRpt.Click += new System.EventHandler(this.btnAuxBoxDispCritCompRpt_Click);
            // 
            // lbAuxBoxCurStatus
            // 
            this.lbAuxBoxCurStatus.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxCurStatus.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxCurStatus.Location = new System.Drawing.Point(880, 307);
            this.lbAuxBoxCurStatus.Name = "lbAuxBoxCurStatus";
            this.lbAuxBoxCurStatus.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxCurStatus.TabIndex = 19;
            this.lbAuxBoxCurStatus.Text = "Current Status";
            this.lbAuxBoxCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxProdLine
            // 
            this.lbAuxBoxProdLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxProdLine.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxProdLine.Location = new System.Drawing.Point(880, 264);
            this.lbAuxBoxProdLine.Name = "lbAuxBoxProdLine";
            this.lbAuxBoxProdLine.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxProdLine.TabIndex = 18;
            this.lbAuxBoxProdLine.Text = "Prod Line";
            this.lbAuxBoxProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxStartDate
            // 
            this.lbAuxBoxStartDate.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxStartDate.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxStartDate.Location = new System.Drawing.Point(880, 220);
            this.lbAuxBoxStartDate.Name = "lbAuxBoxStartDate";
            this.lbAuxBoxStartDate.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxStartDate.TabIndex = 17;
            this.lbAuxBoxStartDate.Text = "Start Date";
            this.lbAuxBoxStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxSecMod
            // 
            this.lbAuxBoxSecMod.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxSecMod.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxSecMod.Location = new System.Drawing.Point(880, 176);
            this.lbAuxBoxSecMod.Name = "lbAuxBoxSecMod";
            this.lbAuxBoxSecMod.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxSecMod.TabIndex = 16;
            this.lbAuxBoxSecMod.Text = "SecondaryMod";
            this.lbAuxBoxSecMod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxModelNo1
            // 
            this.lbAuxBoxModelNo1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxModelNo1.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxModelNo1.Location = new System.Drawing.Point(817, 72);
            this.lbAuxBoxModelNo1.Name = "lbAuxBoxModelNo1";
            this.lbAuxBoxModelNo1.Size = new System.Drawing.Size(409, 23);
            this.lbAuxBoxModelNo1.TabIndex = 15;
            this.lbAuxBoxModelNo1.Text = "ModelNo";
            this.lbAuxBoxModelNo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAuxBoxJobNum
            // 
            this.lbAuxBoxJobNum.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAuxBoxJobNum.ForeColor = System.Drawing.Color.Blue;
            this.lbAuxBoxJobNum.Location = new System.Drawing.Point(880, 35);
            this.lbAuxBoxJobNum.Name = "lbAuxBoxJobNum";
            this.lbAuxBoxJobNum.Size = new System.Drawing.Size(281, 23);
            this.lbAuxBoxJobNum.TabIndex = 14;
            this.lbAuxBoxJobNum.Text = "Job Num";
            this.lbAuxBoxJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvAuxBox
            // 
            this.dgvAuxBox.AllowUserToAddRows = false;
            this.dgvAuxBox.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvAuxBox.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAuxBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAuxBox.Location = new System.Drawing.Point(6, 6);
            this.dgvAuxBox.MultiSelect = false;
            this.dgvAuxBox.Name = "dgvAuxBox";
            this.dgvAuxBox.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAuxBox.Size = new System.Drawing.Size(805, 518);
            this.dgvAuxBox.TabIndex = 1;
            this.dgvAuxBox.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvAuxBox_CellMouseDoubleClick);
            this.dgvAuxBox.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvAuxBox_DataBindingComplete);
            // 
            // tabPagePolyIso
            // 
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoUnitType);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoAuxBox);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoModelNo2);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoModelNo1);
            this.tabPagePolyIso.Controls.Add(this.label3);
            this.tabPagePolyIso.Controls.Add(this.label2);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoATotal);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoTotal);
            this.tabPagePolyIso.Controls.Add(this.label1);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoCurStatus);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoProdLine);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoStartDate);
            this.tabPagePolyIso.Controls.Add(this.lbPolyIsoJobNum);
            this.tabPagePolyIso.Controls.Add(this.dgvPolyIso);
            this.tabPagePolyIso.Location = new System.Drawing.Point(4, 22);
            this.tabPagePolyIso.Name = "tabPagePolyIso";
            this.tabPagePolyIso.Size = new System.Drawing.Size(1238, 530);
            this.tabPagePolyIso.TabIndex = 2;
            this.tabPagePolyIso.Text = "Poly Iso";
            this.tabPagePolyIso.UseVisualStyleBackColor = true;
            // 
            // lbPolyIsoUnitType
            // 
            this.lbPolyIsoUnitType.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoUnitType.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoUnitType.Location = new System.Drawing.Point(879, 124);
            this.lbPolyIsoUnitType.Name = "lbPolyIsoUnitType";
            this.lbPolyIsoUnitType.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoUnitType.TabIndex = 35;
            this.lbPolyIsoUnitType.Text = "UnitType";
            this.lbPolyIsoUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoAuxBox
            // 
            this.lbPolyIsoAuxBox.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoAuxBox.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoAuxBox.Location = new System.Drawing.Point(879, 157);
            this.lbPolyIsoAuxBox.Name = "lbPolyIsoAuxBox";
            this.lbPolyIsoAuxBox.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoAuxBox.TabIndex = 34;
            this.lbPolyIsoAuxBox.Text = "SecondaryMod";
            this.lbPolyIsoAuxBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoModelNo2
            // 
            this.lbPolyIsoModelNo2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoModelNo2.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoModelNo2.Location = new System.Drawing.Point(807, 93);
            this.lbPolyIsoModelNo2.Name = "lbPolyIsoModelNo2";
            this.lbPolyIsoModelNo2.Size = new System.Drawing.Size(423, 23);
            this.lbPolyIsoModelNo2.TabIndex = 33;
            this.lbPolyIsoModelNo2.Text = "ModelNo";
            this.lbPolyIsoModelNo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoModelNo1
            // 
            this.lbPolyIsoModelNo1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoModelNo1.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoModelNo1.Location = new System.Drawing.Point(807, 69);
            this.lbPolyIsoModelNo1.Name = "lbPolyIsoModelNo1";
            this.lbPolyIsoModelNo1.Size = new System.Drawing.Size(423, 23);
            this.lbPolyIsoModelNo1.TabIndex = 32;
            this.lbPolyIsoModelNo1.Text = "ModelNo";
            this.lbPolyIsoModelNo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(837, 449);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(368, 23);
            this.label3.TabIndex = 31;
            this.label3.Text = "INS2.00X2.10LBS-PIC-A  (4x10) Sheet";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(850, 357);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(345, 23);
            this.label2.TabIndex = 30;
            this.label2.Text = "INS2.00X2.10LBS-PIC (4x7 Sheet)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoATotal
            // 
            this.lbPolyIsoATotal.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoATotal.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoATotal.Location = new System.Drawing.Point(879, 488);
            this.lbPolyIsoATotal.Name = "lbPolyIsoATotal";
            this.lbPolyIsoATotal.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoATotal.TabIndex = 29;
            this.lbPolyIsoATotal.Text = "Poly IsoA Total";
            this.lbPolyIsoATotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoTotal
            // 
            this.lbPolyIsoTotal.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoTotal.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoTotal.Location = new System.Drawing.Point(879, 403);
            this.lbPolyIsoTotal.Name = "lbPolyIsoTotal";
            this.lbPolyIsoTotal.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoTotal.TabIndex = 28;
            this.lbPolyIsoTotal.Text = "PolyIsoTotal";
            this.lbPolyIsoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(879, 313);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(281, 29);
            this.label1.TabIndex = 27;
            this.label1.Text = "3 Day Totals";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoCurStatus
            // 
            this.lbPolyIsoCurStatus.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoCurStatus.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoCurStatus.Location = new System.Drawing.Point(879, 257);
            this.lbPolyIsoCurStatus.Name = "lbPolyIsoCurStatus";
            this.lbPolyIsoCurStatus.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoCurStatus.TabIndex = 26;
            this.lbPolyIsoCurStatus.Text = "Current Status";
            this.lbPolyIsoCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoProdLine
            // 
            this.lbPolyIsoProdLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoProdLine.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoProdLine.Location = new System.Drawing.Point(879, 224);
            this.lbPolyIsoProdLine.Name = "lbPolyIsoProdLine";
            this.lbPolyIsoProdLine.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoProdLine.TabIndex = 25;
            this.lbPolyIsoProdLine.Text = "Prod Line";
            this.lbPolyIsoProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoStartDate
            // 
            this.lbPolyIsoStartDate.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoStartDate.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoStartDate.Location = new System.Drawing.Point(879, 190);
            this.lbPolyIsoStartDate.Name = "lbPolyIsoStartDate";
            this.lbPolyIsoStartDate.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoStartDate.TabIndex = 24;
            this.lbPolyIsoStartDate.Text = "Start Date";
            this.lbPolyIsoStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPolyIsoJobNum
            // 
            this.lbPolyIsoJobNum.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPolyIsoJobNum.ForeColor = System.Drawing.Color.Blue;
            this.lbPolyIsoJobNum.Location = new System.Drawing.Point(879, 35);
            this.lbPolyIsoJobNum.Name = "lbPolyIsoJobNum";
            this.lbPolyIsoJobNum.Size = new System.Drawing.Size(281, 23);
            this.lbPolyIsoJobNum.TabIndex = 21;
            this.lbPolyIsoJobNum.Text = "Job Num";
            this.lbPolyIsoJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvPolyIso
            // 
            this.dgvPolyIso.AllowUserToAddRows = false;
            this.dgvPolyIso.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvPolyIso.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPolyIso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPolyIso.Location = new System.Drawing.Point(6, 6);
            this.dgvPolyIso.MultiSelect = false;
            this.dgvPolyIso.Name = "dgvPolyIso";
            this.dgvPolyIso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPolyIso.Size = new System.Drawing.Size(800, 518);
            this.dgvPolyIso.TabIndex = 1;
            this.dgvPolyIso.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPolyIso_CellMouseDoubleClick);
            this.dgvPolyIso.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPolyIso_DataBindingComplete);
            // 
            // tabPageSpecialUnits
            // 
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialModelNo2);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialCurStatus);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialProdLine);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialStartDate);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecial65SCCR);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialModelNo1);
            this.tabPageSpecialUnits.Controls.Add(this.lbSpecialJobNum);
            this.tabPageSpecialUnits.Controls.Add(this.dgvSpecial);
            this.tabPageSpecialUnits.Location = new System.Drawing.Point(4, 22);
            this.tabPageSpecialUnits.Name = "tabPageSpecialUnits";
            this.tabPageSpecialUnits.Size = new System.Drawing.Size(1238, 530);
            this.tabPageSpecialUnits.TabIndex = 3;
            this.tabPageSpecialUnits.Text = "Special Units";
            this.tabPageSpecialUnits.UseVisualStyleBackColor = true;
            // 
            // lbSpecialModelNo2
            // 
            this.lbSpecialModelNo2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialModelNo2.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialModelNo2.Location = new System.Drawing.Point(809, 103);
            this.lbSpecialModelNo2.Name = "lbSpecialModelNo2";
            this.lbSpecialModelNo2.Size = new System.Drawing.Size(423, 23);
            this.lbSpecialModelNo2.TabIndex = 21;
            this.lbSpecialModelNo2.Text = "ModelNo";
            this.lbSpecialModelNo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecialCurStatus
            // 
            this.lbSpecialCurStatus.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialCurStatus.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialCurStatus.Location = new System.Drawing.Point(874, 278);
            this.lbSpecialCurStatus.Name = "lbSpecialCurStatus";
            this.lbSpecialCurStatus.Size = new System.Drawing.Size(281, 23);
            this.lbSpecialCurStatus.TabIndex = 20;
            this.lbSpecialCurStatus.Text = "Current Status";
            this.lbSpecialCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecialProdLine
            // 
            this.lbSpecialProdLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialProdLine.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialProdLine.Location = new System.Drawing.Point(874, 235);
            this.lbSpecialProdLine.Name = "lbSpecialProdLine";
            this.lbSpecialProdLine.Size = new System.Drawing.Size(281, 23);
            this.lbSpecialProdLine.TabIndex = 19;
            this.lbSpecialProdLine.Text = "Prod Line";
            this.lbSpecialProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecialStartDate
            // 
            this.lbSpecialStartDate.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialStartDate.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialStartDate.Location = new System.Drawing.Point(874, 191);
            this.lbSpecialStartDate.Name = "lbSpecialStartDate";
            this.lbSpecialStartDate.Size = new System.Drawing.Size(281, 23);
            this.lbSpecialStartDate.TabIndex = 18;
            this.lbSpecialStartDate.Text = "Start Date";
            this.lbSpecialStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecial65SCCR
            // 
            this.lbSpecial65SCCR.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecial65SCCR.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecial65SCCR.Location = new System.Drawing.Point(813, 147);
            this.lbSpecial65SCCR.Name = "lbSpecial65SCCR";
            this.lbSpecial65SCCR.Size = new System.Drawing.Size(424, 23);
            this.lbSpecial65SCCR.TabIndex = 17;
            this.lbSpecial65SCCR.Text = "65 SCCR ";
            this.lbSpecial65SCCR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecialModelNo1
            // 
            this.lbSpecialModelNo1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialModelNo1.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialModelNo1.Location = new System.Drawing.Point(809, 79);
            this.lbSpecialModelNo1.Name = "lbSpecialModelNo1";
            this.lbSpecialModelNo1.Size = new System.Drawing.Size(423, 23);
            this.lbSpecialModelNo1.TabIndex = 16;
            this.lbSpecialModelNo1.Text = "ModelNo";
            this.lbSpecialModelNo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSpecialJobNum
            // 
            this.lbSpecialJobNum.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSpecialJobNum.ForeColor = System.Drawing.Color.Blue;
            this.lbSpecialJobNum.Location = new System.Drawing.Point(874, 35);
            this.lbSpecialJobNum.Name = "lbSpecialJobNum";
            this.lbSpecialJobNum.Size = new System.Drawing.Size(281, 23);
            this.lbSpecialJobNum.TabIndex = 15;
            this.lbSpecialJobNum.Text = "Job Num";
            this.lbSpecialJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvSpecial
            // 
            this.dgvSpecial.AllowUserToAddRows = false;
            this.dgvSpecial.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSpecial.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvSpecial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSpecial.Location = new System.Drawing.Point(6, 6);
            this.dgvSpecial.MultiSelect = false;
            this.dgvSpecial.Name = "dgvSpecial";
            this.dgvSpecial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSpecial.Size = new System.Drawing.Size(800, 518);
            this.dgvSpecial.TabIndex = 2;
            this.dgvSpecial.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSpecial_CellMouseDoubleClick);
            // 
            // tabPageSubAsmSched
            // 
            this.tabPageSubAsmSched.Controls.Add(this.dgvSubAsmSchd);
            this.tabPageSubAsmSched.Location = new System.Drawing.Point(4, 22);
            this.tabPageSubAsmSched.Name = "tabPageSubAsmSched";
            this.tabPageSubAsmSched.Size = new System.Drawing.Size(1238, 530);
            this.tabPageSubAsmSched.TabIndex = 4;
            this.tabPageSubAsmSched.Text = "SubAsm Schedule";
            this.tabPageSubAsmSched.UseVisualStyleBackColor = true;
            // 
            // dgvSubAsmSchd
            // 
            this.dgvSubAsmSchd.AllowUserToAddRows = false;
            this.dgvSubAsmSchd.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvSubAsmSchd.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvSubAsmSchd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSubAsmSchd.Location = new System.Drawing.Point(6, 6);
            this.dgvSubAsmSchd.MultiSelect = false;
            this.dgvSubAsmSchd.Name = "dgvSubAsmSchd";
            this.dgvSubAsmSchd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSubAsmSchd.Size = new System.Drawing.Size(1225, 518);
            this.dgvSubAsmSchd.TabIndex = 1;
            this.dgvSubAsmSchd.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSubAsmSchd_CellMouseDoubleClick);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1169, 42);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lbArea
            // 
            this.lbArea.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArea.ForeColor = System.Drawing.Color.Navy;
            this.lbArea.Location = new System.Drawing.Point(425, 48);
            this.lbArea.Name = "lbArea";
            this.lbArea.Size = new System.Drawing.Size(466, 31);
            this.lbArea.TabIndex = 4;
            this.lbArea.Text = "Heat Pump - Sub Assembly";
            this.lbArea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 662);
            this.Controls.Add(this.lbArea);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.pictureBoxCompLogo);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Advanced Scheduling Dashboard";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompLogo)).EndInit();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageHeatPump.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHeatPump)).EndInit();
            this.tabPageAuxBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuxBox)).EndInit();
            this.tabPagePolyIso.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPolyIso)).EndInit();
            this.tabPageSpecialUnits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpecial)).EndInit();
            this.tabPageSubAsmSched.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSubAsmSchd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCompLogo;
        private System.Windows.Forms.Label lbMainTitle;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageHeatPump;
        private System.Windows.Forms.DataGridView dgvHeatPump;
        private System.Windows.Forms.TabPage tabPageAuxBox;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TabPage tabPagePolyIso;
        private System.Windows.Forms.TabPage tabPageSpecialUnits;
        public System.Windows.Forms.Label lbArea;
        private System.Windows.Forms.Label lbHeatPumpCurrentStatus;
        private System.Windows.Forms.Label lbHeatPumpProdLine;
        private System.Windows.Forms.Label lbHeatPumpStartDate;
        private System.Windows.Forms.Label lbHeatPumpType;
        private System.Windows.Forms.Label lbHeatPumpModelNo1;
        private System.Windows.Forms.Label lbHeatPumpJobNum;
        private System.Windows.Forms.Label lbHeatPumpModelNo2;
        private System.Windows.Forms.Label lbAuxBoxCurStatus;
        private System.Windows.Forms.Label lbAuxBoxProdLine;
        private System.Windows.Forms.Label lbAuxBoxStartDate;
        private System.Windows.Forms.Label lbAuxBoxSecMod;
        private System.Windows.Forms.Label lbAuxBoxModelNo1;
        private System.Windows.Forms.Label lbAuxBoxJobNum;
        private System.Windows.Forms.DataGridView dgvAuxBox;
        private System.Windows.Forms.DataGridView dgvPolyIso;
        private System.Windows.Forms.Label lbPolyIsoATotal;
        private System.Windows.Forms.Label lbPolyIsoTotal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbPolyIsoCurStatus;
        private System.Windows.Forms.Label lbPolyIsoProdLine;
        private System.Windows.Forms.Label lbPolyIsoStartDate;
        private System.Windows.Forms.Label lbPolyIsoJobNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbHeatPumpTonage;
        private System.Windows.Forms.Label lbPolyIsoModelNo2;
        private System.Windows.Forms.Label lbPolyIsoModelNo1;
        private System.Windows.Forms.Label lbPolyIsoAuxBox;
        private System.Windows.Forms.DataGridView dgvSpecial;
        private System.Windows.Forms.Label lbSpecialModelNo2;
        private System.Windows.Forms.Label lbSpecialCurStatus;
        private System.Windows.Forms.Label lbSpecialProdLine;
        private System.Windows.Forms.Label lbSpecialStartDate;
        private System.Windows.Forms.Label lbSpecial65SCCR;
        private System.Windows.Forms.Label lbSpecialModelNo1;
        private System.Windows.Forms.Label lbSpecialJobNum;
        private System.Windows.Forms.Button btnDisplayRfgCompRpt;
        private System.Windows.Forms.Label lbPolyIsoUnitType;
        private System.Windows.Forms.Label lbAuxBoxUnitType;
        private System.Windows.Forms.Button btnAuxBoxDispCritCompRpt;
        private System.Windows.Forms.Label lbAuxBoxCustName;
        private System.Windows.Forms.TabPage tabPageSubAsmSched;
        private System.Windows.Forms.DataGridView dgvSubAsmSchd;
    }
}


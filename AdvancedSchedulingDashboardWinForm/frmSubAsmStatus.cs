﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvSchedDashWinForm
{
    public partial class frmSubAsmStatus : Form
    {
        MainBO objMain = new MainBO();

        public frmSubAsmStatus()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnModelNoConfig_Click(object sender, EventArgs e)
        {
            string jobNumStr = lbJobNum.Text; ;
            string modelNoStr = lbModelNo.Text;
            string custNameStr = lbCustName.Text;

            if ((modelNoStr.Length == 39) && (modelNoStr.IndexOf(" ") == -1))
            {
                parseOldModelNoDisplayConfigForm(modelNoStr, jobNumStr, custNameStr);
            }
            else if ((modelNoStr.Length == 69) && (modelNoStr.IndexOf(" ") == -1))
            {
                parseModelNoDisplayConfigForm(modelNoStr, jobNumStr, custNameStr, true);
            }
            else
            {
                MessageBox.Show("ERROR - Invalid ModelNo length.");
            }
        }

        #region CoolingWalls Events
        private void btnCoolingWallsStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
            
            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnCoolingWallsStart.Enabled = false;
            btnCoolingWallsStart.BackColor = Color.Gainsboro;           
            btnCoolingWallsPause.Enabled = true;
            btnCoolingWallsPause.BackColor = Color.Yellow;
            btnCoolingWallsComplete.Enabled = true;
            btnCoolingWallsComplete.BackColor = Color.Red;
            lbCoolingWallsCurStatus.Text = "Started: " + transDateTime.ToString();
            lbCoolingWallsCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;
            jobFound = GetSubAssemblyStatusData();

            objMain.CoolingWalls = 1;

            if (jobFound == false)
            {
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "CoolingWalls";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnCoolingWallsPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }
          
            DateTime transDateTime = DateTime.Now;
            
            btnCoolingWallsStart.Enabled = true;
            btnCoolingWallsStart.BackColor = Color.Lime;
            btnCoolingWallsPause.Enabled = false;
            btnCoolingWallsPause.BackColor = Color.Gainsboro;
            btnCoolingWallsComplete.Enabled = false;
            btnCoolingWallsComplete.BackColor = Color.Gainsboro;
            lbCoolingWallsCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbCoolingWallsCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.CoolingWalls = 2;
            objMain.UpdateSubAssemblyStatus();            

            objMain.SubAssemblyName = "CoolingWalls";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnCoolingWallsComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnCoolingWallsStart.Enabled = false;
            btnCoolingWallsStart.BackColor = Color.Gainsboro;
            btnCoolingWallsPause.Enabled = false;
            btnCoolingWallsPause.BackColor = Color.Gainsboro;
            btnCoolingWallsComplete.Enabled = false;
            btnCoolingWallsComplete.BackColor = Color.Gainsboro;
            lbCoolingWallsCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbCoolingWallsCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.CoolingWalls = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "CoolingWalls";
            objMain.TransType = "Complete" +
                "";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion              

        #region Heater Events
        private void btnHeaterStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnHeaterStart.Enabled = false;
            btnHeaterStart.BackColor = Color.Gainsboro;            
            btnHeaterPause.Enabled = true;
            btnHeaterPause.BackColor = Color.Yellow;
            btnHeaterComplete.Enabled = true;
            btnHeaterComplete.BackColor = Color.Red;
            lbHeaterCurStatus.Text = "Started: " + transDateTime.ToString();
            lbHeaterCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.Heater = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "Heater";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnHeaterPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnHeaterStart.Enabled = true;
            btnHeaterStart.BackColor = Color.Lime;
            btnHeaterPause.Enabled = false;
            btnHeaterPause.BackColor = Color.Gainsboro;
            btnHeaterComplete.Enabled = false;
            btnHeaterComplete.BackColor = Color.Gainsboro;
            lbHeaterCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbHeaterCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Heater = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Heater";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnHeaterComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnHeaterStart.Enabled = false;
            btnHeaterStart.BackColor = Color.Gainsboro;
            btnHeaterPause.Enabled = false;
            btnHeaterPause.BackColor = Color.Gainsboro;
            btnHeaterComplete.Enabled = false;
            btnHeaterComplete.BackColor = Color.Gainsboro;
            lbHeaterCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbHeaterCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Heater = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Heater";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion

        #region DoorPanels Events
        private void btnDoorPanelStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnDoorPanelStart.Enabled = false;
            btnDoorPanelStart.BackColor = Color.Gainsboro;            
            btnDoorPanelPause.Enabled = true;
            btnDoorPanelPause.BackColor = Color.Yellow;
            btnDoorPanelComplete.Enabled = true;
            btnDoorPanelComplete.BackColor = Color.Red;
            lbDoorPanelsCurStatus.Text = "Started: " + transDateTime.ToString();
            lbDoorPanelsCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.DoorPanels = 1;

            if (jobFound == false)
            {               
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "DoorPanels";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnDoorPanelPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnDoorPanelStart.Enabled = true;
            btnDoorPanelStart.BackColor = Color.Lime;
            btnDoorPanelPause.Enabled = false;
            btnDoorPanelPause.BackColor = Color.Gainsboro;
            btnDoorPanelComplete.Enabled = false;
            btnDoorPanelComplete.BackColor = Color.Gainsboro;
            lbDoorPanelsCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbDoorPanelsCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.DoorPanels = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "DoorPanels";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnDoorPanelComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnDoorPanelStart.Enabled = false;
            btnDoorPanelStart.BackColor = Color.Gainsboro;
            btnDoorPanelPause.Enabled = false;
            btnDoorPanelPause.BackColor = Color.Gainsboro;
            btnDoorPanelComplete.Enabled = false;
            btnDoorPanelComplete.BackColor = Color.Gainsboro;
            lbDoorPanelsCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbDoorPanelsCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.DoorPanels = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "DoorPanels";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion
               
        #region AuxBox Events
        private void btnAuxBoxStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnAuxBoxStart.Enabled = false;
            btnAuxBoxStart.BackColor = Color.Gainsboro;            
            btnAuxBoxPause.Enabled = true;
            btnAuxBoxPause.BackColor = Color.Yellow;
            btnAuxBoxComplete.Enabled = true;
            btnAuxBoxComplete.BackColor = Color.Red;
            lbAuxBoxCurStatus.Text = "Started: " + transDateTime.ToString();
            lbAuxBoxCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.AuxBox = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }
            objMain.SubAssemblyName = "AuxBox";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnAuxBoxPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnAuxBoxStart.Enabled = true;
            btnAuxBoxStart.BackColor = Color.Lime;
            btnAuxBoxPause.Enabled = false;
            btnAuxBoxPause.BackColor = Color.Gainsboro;
            btnAuxBoxComplete.Enabled = false;
            btnAuxBoxComplete.BackColor = Color.Gainsboro;
            lbAuxBoxCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbAuxBoxCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.AuxBox = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "AuxBox";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnAuxBoxComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnAuxBoxStart.Enabled = false;
            btnAuxBoxStart.BackColor = Color.Gainsboro;
            btnAuxBoxPause.Enabled = false;
            btnAuxBoxPause.BackColor = Color.Gainsboro;
            btnAuxBoxComplete.Enabled = false;
            btnAuxBoxComplete.BackColor = Color.Gainsboro;
            lbAuxBoxCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbAuxBoxCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.AuxBox = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "AuxBox";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion

        #region Intake Events
        private void btnIntakeStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnIntakeStart.Enabled = false;
            btnIntakeStart.BackColor = Color.Gainsboro;            
            btnIntakePause.Enabled = true;
            btnIntakePause.BackColor = Color.Yellow;
            btnIntakeComplete.Enabled = true;
            btnIntakeComplete.BackColor = Color.Red;
            lbIntakeCurStatus.Text = "Started: " + transDateTime.ToString();
            lbIntakeCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.Intake = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "Intake";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnIntakePause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnIntakeStart.Enabled = true;
            btnIntakeStart.BackColor = Color.Lime;
            btnIntakePause.Enabled = false;
            btnIntakePause.BackColor = Color.Gainsboro;
            btnIntakeComplete.Enabled = false;
            btnIntakeComplete.BackColor = Color.Gainsboro;
            lbIntakeCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbIntakeCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Intake = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Intake";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnIntakeComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnIntakeStart.Enabled = true;
            btnIntakeStart.BackColor = Color.Gainsboro;
            btnIntakePause.Enabled = false;
            btnIntakePause.BackColor = Color.Gainsboro;
            btnIntakeComplete.Enabled = false;
            btnIntakeComplete.BackColor = Color.Gainsboro;
            lbIntakeCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbIntakeCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Intake = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Intake";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion
               
        #region Breaker Events
        private void btnBreakerStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnBreakerStart.Enabled = false;
            btnBreakerStart.BackColor = Color.Gainsboro;            
            btnBreakerPause.Enabled = true;
            btnBreakerPause.BackColor = Color.Yellow;
            btnBreakerComplete.Enabled = true;
            btnBreakerComplete.BackColor = Color.Red;
            lbBreakerCurStatus.Text = "Started: " + transDateTime.ToString();
            lbBreakerCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.Breaker = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "Breaker";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnBreakerPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnBreakerStart.Enabled = true;
            btnBreakerStart.BackColor = Color.Lime;
            btnBreakerPause.Enabled = false;
            btnBreakerPause.BackColor = Color.Gainsboro;
            btnBreakerComplete.Enabled = false;
            btnBreakerComplete.BackColor = Color.Gainsboro;
            lbBreakerCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbBreakerCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Breaker = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Breaker";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnBreakerComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnBreakerStart.Enabled = false;
            btnBreakerStart.BackColor = Color.Gainsboro;
            btnBreakerPause.Enabled = false;
            btnBreakerPause.BackColor = Color.Gainsboro;
            btnBreakerComplete.Enabled = false;
            btnBreakerComplete.BackColor = Color.Gainsboro;
            lbBreakerCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbBreakerCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Breaker = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Breaker";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion

        #region Transformer Events
        private void btnTransStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnTransStart.Enabled = false;
            btnTransStart.BackColor = Color.Gainsboro;           
            btnTransPause.Enabled = true;
            btnTransPause.BackColor = Color.Yellow;
            btnTransComplete.Enabled = true;
            btnTransComplete.BackColor = Color.Red;
            lbTransformerCurStatus.Text = "Started: " + transDateTime.ToString();
            lbTransformerCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.Transformer = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "Transformer";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnTransPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnTransStart.Enabled = true;
            btnTransStart.BackColor = Color.Lime;
            btnTransPause.Enabled = false;
            btnTransPause.BackColor = Color.Gainsboro;
            btnTransComplete.Enabled = false;
            btnTransComplete.BackColor = Color.Gainsboro;
            lbTransformerCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbTransformerCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Transformer = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Transformer";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnTransComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnTransStart.Enabled = false;
            btnTransStart.BackColor = Color.Gainsboro;
            btnTransPause.Enabled = false;
            btnTransPause.BackColor = Color.Gainsboro;
            btnTransComplete.Enabled = false;
            btnTransComplete.BackColor = Color.Gainsboro;
            lbTransformerCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbTransformerCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Transformer = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "Transformer";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion

        #region Indoor Fan Motor Events
        private void btnInFanMtrStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnInFanMtrStart.Enabled = false;
            btnInFanMtrStart.BackColor = Color.Gainsboro;            
            btnInFanMtrPause.Enabled = true;
            btnInFanMtrPause.BackColor = Color.Yellow;
            btnInFanMtrComplete.Enabled = true;
            btnInFanMtrComplete.BackColor = Color.Red;
            lbIndoorFanMtrCurStatus.Text = "Started: " + transDateTime.ToString();
            lbIndoorFanMtrCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.IndoorFanMotor = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "IndoorFanMotor";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnInFanMtrPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnInFanMtrStart.Enabled = true;
            btnInFanMtrStart.BackColor = Color.Lime;
            btnInFanMtrPause.Enabled = false;
            btnInFanMtrPause.BackColor = Color.Gainsboro;
            btnInFanMtrComplete.Enabled = false;
            btnInFanMtrComplete.BackColor = Color.Gainsboro;
            lbIndoorFanMtrCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbIndoorFanMtrCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.IndoorFanMotor = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "IndoorFanMotor";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnInFanMtrComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnInFanMtrStart.Enabled = false;
            btnInFanMtrStart.BackColor = Color.Gainsboro;
            btnInFanMtrPause.Enabled = false;
            btnInFanMtrPause.BackColor = Color.Gainsboro;
            btnInFanMtrComplete.Enabled = false;
            btnInFanMtrComplete.BackColor = Color.Gainsboro;
            lbIndoorFanMtrCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbIndoorFanMtrCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.IndoorFanMotor = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "IndoorFanMotor";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion
       
        #region Powered Exhaust Motor Events
        private void btnPwrExMotorStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnPwrExMotorStart.Enabled = false;
            btnPwrExMotorStart.BackColor = Color.Gainsboro;            
            btnPwrExMotorPause.Enabled = true;
            btnPwrExMotorPause.BackColor = Color.Yellow;
            btnPwrExMotorComplete.Enabled = true;
            btnPwrExMotorComplete.BackColor = Color.Red;
            lbPwrExMotorCurStatus.Text = "Started: " + transDateTime.ToString();
            lbPwrExMotorCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.PoweredExhaustMotor = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }


            objMain.SubAssemblyName = "PoweredExhaustMotor";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnPwrExMotorPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnPwrExMotorStart.Enabled = true;
            btnPwrExMotorStart.BackColor = Color.Lime;
            btnPwrExMotorPause.Enabled = false;
            btnPwrExMotorPause.BackColor = Color.Gainsboro;
            btnPwrExMotorComplete.Enabled = false;
            btnPwrExMotorComplete.BackColor = Color.Gainsboro;
            lbPwrExMotorCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbPwrExMotorCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.PoweredExhaustMotor = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "PoweredExhaustMotor";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnPwrExMotorComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnPwrExMotorStart.Enabled = false;
            btnPwrExMotorStart.BackColor = Color.Lime;
            btnPwrExMotorPause.Enabled = false;
            btnPwrExMotorPause.BackColor = Color.Gainsboro;
            btnPwrExMotorComplete.Enabled = false;
            btnPwrExMotorComplete.BackColor = Color.Gainsboro;
            lbPwrExMotorCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbPwrExMotorCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.PoweredExhaustMotor = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "PoweredExhaustMotor";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion

        #region Condenser Fan Motor Events
        private void btnCondFanMotorStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnCondFanMotorStart.Enabled = false;
            btnCondFanMotorStart.BackColor = Color.Gainsboro;            
            btnCondFanMotorPause.Enabled = true;
            btnCondFanMotorPause.BackColor = Color.Yellow;
            btnCondFanMotorComplete.Enabled = true;
            btnCondFanMotorComplete.BackColor = Color.Red;
            lbCondFanMotorCurStatus.Text = "Started: " + transDateTime.ToString();
            lbCondFanMotorCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.CondenserFanMotor = 1;

            if (jobFound == false)
            {               
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "CondenserFanMotor";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnCondFanMotorPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnCondFanMotorStart.Enabled = true;
            btnCondFanMotorStart.BackColor = Color.Lime;
            btnCondFanMotorPause.Enabled = false;
            btnCondFanMotorPause.BackColor = Color.Gainsboro;
            btnCondFanMotorComplete.Enabled = false;
            btnCondFanMotorComplete.BackColor = Color.Gainsboro;
            lbCondFanMotorCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbCondFanMotorCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.CondenserFanMotor = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "CondenserFanMotor";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnCondFanMotorComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnCondFanMotorStart.Enabled = false;
            btnCondFanMotorStart.BackColor = Color.Gainsboro;
            btnCondFanMotorPause.Enabled = false;
            btnCondFanMotorPause.BackColor = Color.Gainsboro;
            btnCondFanMotorComplete.Enabled = false;
            btnCondFanMotorComplete.BackColor = Color.Gainsboro;
            lbCondFanMotorCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbCondFanMotorCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.CondenserFanMotor = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "CondenserFanMotor";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        #endregion
       
        #region Electrical Kits Events
        private void btnElecKitStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnElecKitStart.Enabled = false;
            btnElecKitStart.BackColor = Color.Gainsboro;            
            btnElecKitPause.Enabled = true;
            btnElecKitPause.BackColor = Color.Yellow;
            btnElecKitComplete.Enabled = true;
            btnElecKitComplete.BackColor = Color.Red;
            lbElecKitsCurStatus.Text = "Started: " + transDateTime.ToString();
            lbElecKitsCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.ElectricalKit = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "ElectricalKit";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnElecKitPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnElecKitStart.Enabled = true;
            btnElecKitStart.BackColor = Color.Lime;
            btnElecKitPause.Enabled = false;
            btnElecKitPause.BackColor = Color.Gainsboro;
            btnElecKitComplete.Enabled = false;
            btnElecKitComplete.BackColor = Color.Gainsboro;
            lbElecKitsCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbElecKitsCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.ElectricalKit = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "ElectricalKit";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }

        private void btnElecKitComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnElecKitStart.Enabled = false;
            btnElecKitStart.BackColor = Color.Gainsboro;
            btnElecKitPause.Enabled = false;
            btnElecKitPause.BackColor = Color.Gainsboro;
            btnElecKitComplete.Enabled = false;
            btnElecKitComplete.BackColor = Color.Gainsboro;
            lbElecKitsCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbElecKitsCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.ElectricalKit = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "ElectricalKit";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion

        #region Refrigeration Kits Events
        private void btnRfgKitStart_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            bool jobFound = false;

            DateTime transDateTime = DateTime.Now;

            btnRfgKitStart.Enabled = false;
            btnRfgKitStart.BackColor = Color.Gainsboro;           
            btnRfgKitPause.Enabled = true;
            btnRfgKitPause.BackColor = Color.Yellow;
            btnRfgKitComplete.Enabled = true;
            btnRfgKitComplete.BackColor = Color.Red;
            lbRfgKitsCurStatus.Text = "Started: " + transDateTime.ToString();
            lbRfgKitsCurStatus.BackColor = Color.Lime;

            objMain.JobNum = lbJobNum.Text;

            jobFound = GetSubAssemblyStatusData();

            objMain.RefrigerationKit = 1;

            if (jobFound == false)
            {                
                objMain.InsertSubAssemblyStatus();
            }
            else
            {
                objMain.UpdateSubAssemblyStatus();
            }

            objMain.SubAssemblyName = "RefrigerationKit";
            objMain.TransType = "Start";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnRfgKitPause_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnRfgKitStart.Enabled = true;
            btnRfgKitStart.BackColor = Color.Lime;
            btnRfgKitPause.Enabled = false;
            btnRfgKitPause.BackColor = Color.Gainsboro;
            btnRfgKitComplete.Enabled = false;
            btnRfgKitComplete.BackColor = Color.Gainsboro;
            lbRfgKitsCurStatus.Text = "Paused: " + transDateTime.ToString();
            lbRfgKitsCurStatus.BackColor = Color.Yellow;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.RefrigerationKit = 2;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "RefrigerationKit";
            objMain.TransType = "Pause";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        private void btnRfgKitComplete_Click(object sender, EventArgs e)
        {
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int sPos = -1;

            sPos = modByStr.LastIndexOf("\\");
            if (sPos > 0)
            {
                modByStr = modByStr.Substring((sPos + 1), (modByStr.Length - (sPos + 1)));
            }

            DateTime transDateTime = DateTime.Now;

            btnRfgKitStart.Enabled = false;
            btnRfgKitStart.BackColor = Color.Gainsboro;
            btnRfgKitPause.Enabled = false;
            btnRfgKitPause.BackColor = Color.Gainsboro;
            btnRfgKitComplete.Enabled = false;
            btnRfgKitComplete.BackColor = Color.Gainsboro;
            lbRfgKitsCurStatus.Text = "Completed: " + transDateTime.ToString();
            lbRfgKitsCurStatus.BackColor = Color.Red;

            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.RefrigerationKit = 3;
            objMain.UpdateSubAssemblyStatus();

            objMain.SubAssemblyName = "RefrigerationKit";
            objMain.TransType = "Complete";
            objMain.TransBy = modByStr;
            objMain.TransDate = DateTime.Now;
            objMain.InsertSubAssemblyHistory();
        }
        #endregion

        #region OtherText Event
        private void rtbOtherComments_Leave(object sender, EventArgs e)
        {
            objMain.JobNum = lbJobNum.Text;
            bool jobFound = GetSubAssemblyStatusData();

            objMain.Other = rtbOtherComments.Text;
            if (jobFound == true)
            {                
                objMain.UpdateSubAssemblyStatus();
            }
            else
            {
                objMain.InsertSubAssemblyStatus();
            }
        }

        #endregion

        

        #region Other Methods

        private bool GetSubAssemblyStatusData()
        {
            bool jobFound = false;

            DataTable dt = objMain.GetSubAssemblyStatusByJob();
            if (dt.Rows.Count == 0)
            {
                objMain.CoolingWalls = 0;
                objMain.Heater = 0;
                objMain.DoorPanels = 0;
                objMain.AuxBox = 0;
                objMain.Intake = 0;
                objMain.Breaker = 0;
                objMain.Transformer = 0;
                objMain.IndoorFanMotor = 0;
                objMain.PoweredExhaustMotor = 0;
                objMain.CondenserFanMotor = 0;
                objMain.ElectricalKit = 0;
                objMain.RefrigerationKit = 0;
                objMain.Other = "";               
            }
            else
            {
                DataRow dr = dt.Rows[0];
                objMain.CoolingWalls = (int) dr["CoolingWalls"];
                objMain.Heater = (int)dr["Heater"]; 
                objMain.DoorPanels = (int)dr["DoorPanels"];
                objMain.AuxBox = (int)dr["AuxBox"];
                objMain.Intake = (int)dr["Intake"];
                objMain.Breaker = (int)dr["Breaker"];
                objMain.Transformer = (int)dr["Transformer"];
                objMain.IndoorFanMotor = (int)dr["IndoorFanMotor"];
                objMain.PoweredExhaustMotor = (int)dr["PoweredExhaustMotor"];
                objMain.CondenserFanMotor = (int)dr["CondenserFanMotor"];
                objMain.ElectricalKit = (int)dr["ElectricalKit"];
                objMain.RefrigerationKit = (int)dr["RefrigerationKit"];
                objMain.Other = dr["Other"].ToString();
                jobFound = true;
            }

            return jobFound;
        }

        private void parseOldModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";
            string secHeatTypeStr = "NA";
            string oauTypeCode = "OAU123DKN";

            frmOldModelNoConfig frmConfig = new frmOldModelNoConfig();

            frmConfig.Text = modelNoStr;

            if (modelNoStr.Substring(0, 3) == "OAB" || modelNoStr.Substring(0, 3) == "OAG")
            {
                oauTypeCode = "OALBG";
            }

            //frmConfig.labelConfigModelNo.Text = modelNoStr;    

            frmConfig.lbJobNum.Text = jobNumStr;            
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName;

            // Digit 3 - Cabinet Size
            digitValStr = modelNoStr.Substring(2, 1);
            DataTable dt = objMain.GetOAU_ModelNoValues(3, "NA", oauTypeCode);
            frmConfig.cbCabinet.DataSource = dt;
            frmConfig.cbCabinet.DisplayMember = "DigitDescription";
            frmConfig.cbCabinet.ValueMember = "DigitVal";
            frmConfig.cbCabinet.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 4 - Major Design
            digitValStr = modelNoStr.Substring(3, 1);
            dt = objMain.GetOAU_ModelNoValues(4, "NA", oauTypeCode);
            frmConfig.cbMajorDesign.DataSource = dt;
            frmConfig.cbMajorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesign.ValueMember = "DigitVal";
            frmConfig.cbMajorDesign.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 567 - Cooling Capacity               
            digitValStr = modelNoStr.Substring(4, 3);
            dt = objMain.GetOAU_ModelNoValues(567, "NA", oauTypeCode);
            frmConfig.cbCoolingCapacity.DataSource = dt;
            frmConfig.cbCoolingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingCapacity.ValueMember = "DigitVal";
            frmConfig.cbCoolingCapacity.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 8 - Minor Design Sequence               
            digitValStr = modelNoStr.Substring(7, 1);
            dt = objMain.GetOAU_ModelNoValues(8, "NA", oauTypeCode);
            frmConfig.cbMinorDesign.DataSource = dt;
            frmConfig.cbMinorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMinorDesign.ValueMember = "DigitVal";
            frmConfig.cbMinorDesign.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 9 - Voltage
            digitValStr = modelNoStr.Substring(8, 1);
            dt = objMain.GetOAU_ModelNoValues(9, "NA", oauTypeCode);
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            frmConfig.cbVoltage.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 11 - Indoor Coil Type
            digitValStr = modelNoStr.Substring(10, 1);
            dt = objMain.GetOAU_ModelNoValues(11, "NA", oauTypeCode);
            frmConfig.cbEvapType.DataSource = dt;
            frmConfig.cbEvapType.DisplayMember = "DigitDescription";
            frmConfig.cbEvapType.ValueMember = "DigitVal";
            frmConfig.cbEvapType.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 12 - Hot Gas Reheat
            digitValStr = modelNoStr.Substring(11, 1);
            dt = objMain.GetOAU_ModelNoValues(12, "NA", oauTypeCode);
            frmConfig.cbHotGasReheat.DataSource = dt;
            frmConfig.cbHotGasReheat.DisplayMember = "DigitDescription";
            frmConfig.cbHotGasReheat.ValueMember = "DigitVal";
            frmConfig.cbHotGasReheat.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 13 - Compressor
            digitValStr = modelNoStr.Substring(12, 1);
            dt = objMain.GetOAU_ModelNoValues(13, "NA", oauTypeCode);
            frmConfig.cbCompressor.DataSource = dt;
            frmConfig.cbCompressor.DisplayMember = "DigitDescription";
            frmConfig.cbCompressor.ValueMember = "DigitVal";
            frmConfig.cbCompressor.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 14 - Outdoor Coil Type
            digitValStr = modelNoStr.Substring(13, 1);
            dt = objMain.GetOAU_ModelNoValues(14, "NA", oauTypeCode);
            frmConfig.cbCondenser.DataSource = dt;
            frmConfig.cbCondenser.DisplayMember = "DigitDescription";
            frmConfig.cbCondenser.ValueMember = "DigitVal";
            frmConfig.cbCondenser.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 15 - Refridgerant Capacity Control
            digitValStr = modelNoStr.Substring(14, 1);
            dt = objMain.GetOAU_ModelNoValues(15, "NA", oauTypeCode);
            frmConfig.cbRefridgerantCapacityControl.DataSource = dt;
            frmConfig.cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
            frmConfig.cbRefridgerantCapacityControl.ValueMember = "DigitVal";
            frmConfig.cbRefridgerantCapacityControl.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 16 - Indoor Fan Motor
            digitValStr = modelNoStr.Substring(15, 1);
            dt = objMain.GetOAU_ModelNoValues(16, "NA", oauTypeCode);
            frmConfig.cbIndoorFanMotor.DataSource = dt;
            frmConfig.cbIndoorFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanMotor.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 17 -Indoor Fan Wheel Size
            digitValStr = modelNoStr.Substring(16, 1);
            dt = objMain.GetOAU_ModelNoValues(17, "NA", oauTypeCode);
            frmConfig.cbIndoorFanWheel.DataSource = dt;
            frmConfig.cbIndoorFanWheel.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanWheel.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanWheel.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 18 - Indoor Fan Motor HP
            digitValStr = modelNoStr.Substring(17, 1);
            dt = objMain.GetOAU_ModelNoValues(18, "NA", oauTypeCode);
            frmConfig.cbIndoorFanMotorHP.DataSource = dt;
            frmConfig.cbIndoorFanMotorHP.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorFanMotorHP.ValueMember = "DigitVal";
            frmConfig.cbIndoorFanMotorHP.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 20 - Heat Type
            digitValStr = modelNoStr.Substring(19, 1);
            dt = objMain.GetOAU_ModelNoValues(20, "NA", oauTypeCode);
            frmConfig.cbHeatType.DataSource = dt;
            frmConfig.cbHeatType.DisplayMember = "DigitDescription";
            frmConfig.cbHeatType.ValueMember = "DigitVal";
            frmConfig.cbHeatType.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 21 - Fuel Type
            digitValStr = modelNoStr.Substring(20, 1);
            dt = objMain.GetOAU_ModelNoValues(21, "NA", oauTypeCode);
            frmConfig.cbFuelType.DataSource = dt;
            frmConfig.cbFuelType.DisplayMember = "DigitDescription";
            frmConfig.cbFuelType.ValueMember = "DigitVal";
            frmConfig.cbFuelType.SelectedIndex = GetRowIndex(dt, digitValStr);

            //Digit 22 - Heat Source Primary
            digitValStr = modelNoStr.Substring(21, 1);
            dt = objMain.GetOAU_ModelNoValues(22, heatingTypeStr, oauTypeCode);
            frmConfig.cbHeatSourcePrimary.DataSource = dt;
            frmConfig.cbHeatSourcePrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatSourcePrimary.ValueMember = "DigitVal";
            frmConfig.cbHeatSourcePrimary.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 23 - Heat Source Secondary
            digitValStr = modelNoStr.Substring(22, 1);
            dt = objMain.GetOAU_ModelNoValues(23, secHeatTypeStr, oauTypeCode);
            frmConfig.cbHeatSourceSecondary.DataSource = dt;
            frmConfig.cbHeatSourceSecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatSourceSecondary.ValueMember = "DigitVal";
            frmConfig.cbHeatSourceSecondary.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 24 - Corrosive Env Package
            digitValStr = modelNoStr.Substring(23, 1);
            dt = objMain.GetOAU_ModelNoValues(24, "NA", oauTypeCode);
            frmConfig.cbCorrosiveEnvPackage.DataSource = dt;
            frmConfig.cbCorrosiveEnvPackage.DisplayMember = "DigitDescription";
            frmConfig.cbCorrosiveEnvPackage.ValueMember = "DigitVal";
            frmConfig.cbCorrosiveEnvPackage.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 25,26 - Unit Controls
            digitValStr = modelNoStr.Substring(24, 2);
            dt = objMain.GetOAU_ModelNoValues(2526, "NA", oauTypeCode);
            frmConfig.cbUnitControls.DataSource = dt;
            frmConfig.cbUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbUnitControls.ValueMember = "DigitVal";
            frmConfig.cbUnitControls.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 27 - Powered Exhaust Fan Motor
            digitValStr = modelNoStr.Substring(26, 1);
            dt = objMain.GetOAU_ModelNoValues(27, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanMotor.DataSource = dt;
            frmConfig.cbPoweredExhaustFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanMotor.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 28 - Powered Exhaust Fan Wheel
            digitValStr = modelNoStr.Substring(27, 1);
            dt = objMain.GetOAU_ModelNoValues(28, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanWheel.DataSource = dt;
            frmConfig.cbPoweredExhaustFanWheel.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanWheel.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanWheel.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 29 - Powered Exhaust Fan Motor HP
            digitValStr = modelNoStr.Substring(28, 1);
            dt = objMain.GetOAU_ModelNoValues(29, "NA", oauTypeCode);
            frmConfig.cbPoweredExhaustFanMotorHP.DataSource = dt;
            frmConfig.cbPoweredExhaustFanMotorHP.DisplayMember = "DigitDescription";
            frmConfig.cbPoweredExhaustFanMotorHP.ValueMember = "DigitVal";
            frmConfig.cbPoweredExhaustFanMotorHP.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 30 - Trane PPS Compatibility
            digitValStr = modelNoStr.Substring(29, 1);
            dt = objMain.GetOAU_ModelNoValues(30, "NA", oauTypeCode);
            frmConfig.cbTranePPS_Compatibility.DataSource = dt;
            frmConfig.cbTranePPS_Compatibility.DisplayMember = "DigitDescription";
            frmConfig.cbTranePPS_Compatibility.ValueMember = "DigitVal";
            frmConfig.cbTranePPS_Compatibility.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 31 - ERV_HRV
            digitValStr = modelNoStr.Substring(30, 1);
            dt = objMain.GetOAU_ModelNoValues(31, "NA", oauTypeCode);
            frmConfig.cbERV_HRV.DataSource = dt;
            frmConfig.cbERV_HRV.DisplayMember = "DigitDescription";
            frmConfig.cbERV_HRV.ValueMember = "DigitVal";
            frmConfig.cbERV_HRV.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 32 - ERV Size
            digitValStr = modelNoStr.Substring(31, 1);
            dt = objMain.GetOAU_ModelNoValues(32, "NA", oauTypeCode);
            frmConfig.cbERVSize.DataSource = dt;
            frmConfig.cbERVSize.DisplayMember = "DigitDescription";
            frmConfig.cbERVSize.ValueMember = "DigitVal";
            frmConfig.cbERVSize.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 33 - Economizer
            digitValStr = modelNoStr.Substring(32, 1);
            dt = objMain.GetOAU_ModelNoValues(33, "NA", oauTypeCode);
            frmConfig.cbEconomizer.DataSource = dt;
            frmConfig.cbEconomizer.DisplayMember = "DigitDescription";
            frmConfig.cbEconomizer.ValueMember = "DigitVal";
            frmConfig.cbEconomizer.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 34 - Filtration Options
            digitValStr = modelNoStr.Substring(33, 1);
            dt = objMain.GetOAU_ModelNoValues(34, "NA", oauTypeCode);
            frmConfig.cbFiltrationOptions.DataSource = dt;
            frmConfig.cbFiltrationOptions.DisplayMember = "DigitDescription";
            frmConfig.cbFiltrationOptions.ValueMember = "DigitVal";
            frmConfig.cbFiltrationOptions.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 35 - Energy Recovery Wheel Size
            digitValStr = modelNoStr.Substring(34, 1);
            dt = objMain.GetOAU_ModelNoValues(35, "NA", oauTypeCode);
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            frmConfig.cbSmokeDetector.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 36 - Electrical Options
            digitValStr = modelNoStr.Substring(35, 1);
            dt = objMain.GetOAU_ModelNoValues(36, "NA", oauTypeCode);
            frmConfig.cbElectricalOptions.DataSource = dt;
            frmConfig.cbElectricalOptions.DisplayMember = "DigitDescription";
            frmConfig.cbElectricalOptions.ValueMember = "DigitVal";
            frmConfig.cbElectricalOptions.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 37 - Damper Options
            digitValStr = modelNoStr.Substring(36, 1);
            dt = objMain.GetOAU_ModelNoValues(37, "NA", oauTypeCode);
            frmConfig.cbAirFlowMonitoring.DataSource = dt;
            frmConfig.cbAirFlowMonitoring.DisplayMember = "DigitDescription";
            frmConfig.cbAirFlowMonitoring.ValueMember = "DigitVal";
            frmConfig.cbAirFlowMonitoring.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 38 - Hailguard 
            digitValStr = modelNoStr.Substring(37, 1);
            dt = objMain.GetOAU_ModelNoValues(38, "NA", oauTypeCode);
            frmConfig.cbHailguard.DataSource = dt;
            frmConfig.cbHailguard.DisplayMember = "DigitDescription";
            frmConfig.cbHailguard.ValueMember = "DigitVal";
            frmConfig.cbHailguard.SelectedIndex = GetRowIndex(dt, digitValStr);

            // Digit 39 - Altitude 
            digitValStr = modelNoStr.Substring(38, 1);
            dt = objMain.GetOAU_ModelNoValues(39, "NA", oauTypeCode);
            frmConfig.cbAltitude.DataSource = dt;
            frmConfig.cbAltitude.DisplayMember = "DigitDescription";
            frmConfig.cbAltitude.ValueMember = "DigitVal";
            frmConfig.cbAltitude.SelectedIndex = GetRowIndex(dt, digitValStr);

            frmConfig.ShowDialog();
        }

        private void parseModelNoDisplayConfigForm(string modelNoStr, string jobNumStr, string custName, bool modelNoValuesPresent)
        {
            string digitValStr = "";
            string heatingTypeStr = "NA";

            frmModelNoConfig frmConfig = new frmModelNoConfig();            

            frmConfig.lbJobNum.Text = jobNumStr;            
            frmConfig.lbModelNo.Text = modelNoStr;
            frmConfig.lbCustName.Text = custName;

            // Digit 3 - Cabinet Size

            DataTable dt = objMain.GetModelNumDesc(3, "NA", null);
            frmConfig.cbCabinet.DataSource = dt;
            frmConfig.cbCabinet.DisplayMember = "DigitDescription";
            frmConfig.cbCabinet.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(2, 1);
                frmConfig.cbCabinet.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 4 - Major Design

            dt = objMain.GetModelNumDesc(4, heatingTypeStr, null);
            frmConfig.cbMajorDesign.DataSource = dt;
            frmConfig.cbMajorDesign.DisplayMember = "DigitDescription";
            frmConfig.cbMajorDesign.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(3, 1);
                frmConfig.cbMajorDesign.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 567 - Cooling Capacity               

            dt = objMain.GetModelNumDesc(567, "NA", null);
            frmConfig.cbCoolingCapacity.DataSource = dt;
            frmConfig.cbCoolingCapacity.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingCapacity.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(4, 3);
                frmConfig.cbCoolingCapacity.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 8 - Airflow Configuration               

            dt = objMain.GetModelNumDesc(8, "NA", null);
            frmConfig.cbAirflowConfig.DataSource = dt;
            frmConfig.cbAirflowConfig.DisplayMember = "DigitDescription";
            frmConfig.cbAirflowConfig.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(7, 1);
                frmConfig.cbAirflowConfig.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 9 - Voltage

            dt = objMain.GetModelNumDesc(9, "NA", null);
            frmConfig.cbVoltage.DataSource = dt;
            frmConfig.cbVoltage.DisplayMember = "DigitDescription";
            frmConfig.cbVoltage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(8, 1);
                frmConfig.cbVoltage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 11 - Indoor Coil Type

            dt = objMain.GetModelNumDesc(11, "NA", null);
            frmConfig.cbIndoorCoilType.DataSource = dt;
            frmConfig.cbIndoorCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbIndoorCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(10, 1);
                frmConfig.cbIndoorCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 12 - Hot Gas Reheat

            dt = objMain.GetModelNumDesc(12, "NA", null);
            frmConfig.cbHotGasReheat.DataSource = dt;
            frmConfig.cbHotGasReheat.DisplayMember = "DigitDescription";
            frmConfig.cbHotGasReheat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(11, 1);
                frmConfig.cbHotGasReheat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 13 - Compressor

            dt = objMain.GetModelNumDesc(13, "NA", null);
            frmConfig.cbCompressor.DataSource = dt;
            frmConfig.cbCompressor.DisplayMember = "DigitDescription";
            frmConfig.cbCompressor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(12, 1);
                frmConfig.cbCompressor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 14 - Outdoor Coil Type

            dt = objMain.GetModelNumDesc(14, "NA", null);
            frmConfig.cbOutdoorCoilType.DataSource = dt;
            frmConfig.cbOutdoorCoilType.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorCoilType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(13, 1);
                frmConfig.cbOutdoorCoilType.SelectedIndex = GetRowIndex(dt, digitValStr);
                string tempDesc = frmConfig.cbOutdoorCoilType.Text;
               
            }

            // Digit 15 - Refridgerant Capacity Control

            dt = objMain.GetModelNumDesc(15, "NA", null);
            frmConfig.cbRefridgerantCapacityControl.DataSource = dt;
            frmConfig.cbRefridgerantCapacityControl.DisplayMember = "DigitDescription";
            frmConfig.cbRefridgerantCapacityControl.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(14, 1);
                frmConfig.cbRefridgerantCapacityControl.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 16 - Heat Type Primary

            dt = objMain.GetModelNumDesc(16, "NA", null);
            frmConfig.cbHeatTypePrimary.DataSource = dt;
            frmConfig.cbHeatTypePrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatTypePrimary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(15, 1);
                frmConfig.cbHeatTypePrimary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            if (digitValStr == "A" || digitValStr == "B" || digitValStr == "C" || digitValStr == "D" || digitValStr == "E" || digitValStr == "F")
            {
                heatingTypeStr = "IF";
            }
            else if (digitValStr == "G")
            {
                heatingTypeStr = "HOTWATER";
            }
            else if (digitValStr == "H" || digitValStr == "J")
            {
                heatingTypeStr = "ELEC";
            }
            else
            {
                heatingTypeStr = "IF";
            }

            // Digit 17 - Heat Capacity Primary

            dt = objMain.GetModelNumDesc(17, heatingTypeStr, null);
            frmConfig.cbHeatCapacityPrimary.DataSource = dt;
            frmConfig.cbHeatCapacityPrimary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatCapacityPrimary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(16, 1);
                frmConfig.cbHeatCapacityPrimary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 18 - Heat Type Secondary

            dt = objMain.GetModelNumDesc(18, "NA", null);
            frmConfig.cbHeatTypeSecondary.DataSource = dt;
            frmConfig.cbHeatTypeSecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatTypeSecondary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(17, 1);
                frmConfig.cbHeatTypeSecondary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            if (digitValStr == "1" || digitValStr == "2")
            {
                heatingTypeStr = "DF";
            }
            else if (digitValStr == "3")
            {
                heatingTypeStr = "HOTWATER";
            }
            else if (digitValStr == "4" || digitValStr == "5")
            {
                heatingTypeStr = "ELEC";
            }
            else
            {
                heatingTypeStr = "NA";
            }

            // Digit 19 - Heat Capacity Secondary           

            dt = objMain.GetModelNumDesc(19, heatingTypeStr, null);
            frmConfig.cbHeatCapacitySecondary.DataSource = dt;
            frmConfig.cbHeatCapacitySecondary.DisplayMember = "DigitDescription";
            frmConfig.cbHeatCapacitySecondary.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(18, 1);
                frmConfig.cbHeatCapacitySecondary.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 21 - Supply Fan Motor

            dt = objMain.GetModelNumDesc(21, "NA", null);
            frmConfig.cbSupplyFanMotor.DataSource = dt;
            frmConfig.cbSupplyFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(20, 1);
                frmConfig.cbSupplyFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 22 - Supply Fan Motor Type

            dt = objMain.GetModelNumDesc(22, "NA", null);
            frmConfig.cbSupplyFanMotorType.DataSource = dt;
            frmConfig.cbSupplyFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanMotorType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(21, 1);
                frmConfig.cbSupplyFanMotorType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 23 & 24 - Supply Fan Wheel Diameter

            dt = objMain.GetModelNumDesc(2324, "NA", null);
            frmConfig.cbSupplyFanWheelDiameter.DataSource = dt;
            frmConfig.cbSupplyFanWheelDiameter.DisplayMember = "DigitDescription";
            frmConfig.cbSupplyFanWheelDiameter.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(22, 2);
                frmConfig.cbSupplyFanWheelDiameter.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 25 - Exhaust Fan Motor

            dt = objMain.GetModelNumDesc(25, "NA", null);
            frmConfig.cbExhaustFanMotor.DataSource = dt;
            frmConfig.cbExhaustFanMotor.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(24, 1);
                frmConfig.cbExhaustFanMotor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 26 - Exhaust Fan Motor Type

            dt = objMain.GetModelNumDesc(26, "NA", null);
            frmConfig.cbExhaustFanMotorType.DataSource = dt;
            frmConfig.cbExhaustFanMotorType.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanMotorType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(25, 1);
                frmConfig.cbExhaustFanMotorType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 27 & 28 - Exhaust Fan Wheel Diameter

            dt = objMain.GetModelNumDesc(2728, "NA", null);
            frmConfig.cbExhaustFanWheelDiamater.DataSource = dt;
            frmConfig.cbExhaustFanWheelDiamater.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustFanWheelDiamater.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(26, 2);
                frmConfig.cbExhaustFanWheelDiamater.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 29 - Exhaust Fan Piezo Ring

            dt = objMain.GetModelNumDesc(29, "NA", null);
            frmConfig.cbFanPiezoRing.DataSource = dt;
            frmConfig.cbFanPiezoRing.DisplayMember = "DigitDescription";
            frmConfig.cbFanPiezoRing.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(28, 1);
                frmConfig.cbFanPiezoRing.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 31 - Unit Controls

            dt = objMain.GetModelNumDesc(31, "NA", null);
            frmConfig.cbUnitControls.DataSource = dt;
            frmConfig.cbUnitControls.DisplayMember = "DigitDescription";
            frmConfig.cbUnitControls.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(30, 1);
                frmConfig.cbUnitControls.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 32 - Build Interface

            dt = objMain.GetModelNumDesc(32, "NA", null);
            frmConfig.cbBuildingInterface.DataSource = dt;
            frmConfig.cbBuildingInterface.DisplayMember = "DigitDescription";
            frmConfig.cbBuildingInterface.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(31, 1);
                frmConfig.cbBuildingInterface.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 33 - Filter Options

            dt = objMain.GetModelNumDesc(33, "NA", null);
            frmConfig.cbFilterOptions.DataSource = dt;
            frmConfig.cbFilterOptions.DisplayMember = "DigitDescription";
            frmConfig.cbFilterOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(32, 1);
                frmConfig.cbFilterOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 34 - ERV-Composite Construction with Bypass

            dt = objMain.GetModelNumDesc(34, "NA", null);
            frmConfig.cbEnergyRecovery.DataSource = dt;
            frmConfig.cbEnergyRecovery.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecovery.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(33, 1);
                frmConfig.cbEnergyRecovery.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 35 - Energy Recovery Wheel Options

            dt = objMain.GetModelNumDesc(35, "NA", null);
            frmConfig.cbEnergyRecoveryWheelOptions.DataSource = dt;
            frmConfig.cbEnergyRecoveryWheelOptions.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyRecoveryWheelOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(34, 1);
                frmConfig.cbEnergyRecoveryWheelOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 36 - Energy Recovery Wheel Size

            dt = objMain.GetModelNumDesc(36, "NA", null);
            frmConfig.cbEnergyWheelRecoverySize.DataSource = dt;
            frmConfig.cbEnergyWheelRecoverySize.DisplayMember = "DigitDescription";
            frmConfig.cbEnergyWheelRecoverySize.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(35, 1);
                frmConfig.cbEnergyWheelRecoverySize.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 37 ERV Rotation Sensor

            dt = objMain.GetModelNumDesc(37, "NA", null);
            frmConfig.cbERV_RotationSensor.DataSource = dt;
            frmConfig.cbERV_RotationSensor.DisplayMember = "DigitDescription";
            frmConfig.cbERV_RotationSensor.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(36, 1);
                frmConfig.cbERV_RotationSensor.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 38 - Damper Options

            dt = objMain.GetModelNumDesc(38, "NA", null);
            frmConfig.cbDamperOptions.DataSource = dt;
            frmConfig.cbDamperOptions.DisplayMember = "DigitDescription";
            frmConfig.cbDamperOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(37, 1);
                frmConfig.cbDamperOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 39 - Exhaust Dampers 

            dt = objMain.GetModelNumDesc(39, "NA", null);
            frmConfig.cbExhaustDampers.DataSource = dt;
            frmConfig.cbExhaustDampers.DisplayMember = "DigitDescription";
            frmConfig.cbExhaustDampers.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(38, 1);
                frmConfig.cbExhaustDampers.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 41 - Electrical Options

            dt = objMain.GetModelNumDesc(41, "NA", null);
            frmConfig.cbElectricalOptions.DataSource = dt;
            frmConfig.cbElectricalOptions.DisplayMember = "DigitDescription";
            frmConfig.cbElectricalOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(40, 1);
                frmConfig.cbElectricalOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 42 - Corrosive Environment Package

            dt = objMain.GetModelNumDesc(42, "NA", null);
            frmConfig.cbCorrisiveEnvironmentPackage.DataSource = dt;
            frmConfig.cbCorrisiveEnvironmentPackage.DisplayMember = "DigitDescription";
            frmConfig.cbCorrisiveEnvironmentPackage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(41, 1);
                frmConfig.cbCorrisiveEnvironmentPackage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 43 - Outdoor Air Monitoring

            dt = objMain.GetModelNumDesc(43, "NA", null);
            frmConfig.cbOutdoorAirMonitoring.DataSource = dt;
            frmConfig.cbOutdoorAirMonitoring.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorAirMonitoring.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(42, 1);
                frmConfig.cbOutdoorAirMonitoring.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 44 - Condenser Fan Options

            dt = objMain.GetModelNumDesc(44, "NA", null);
            frmConfig.cbCondenserFanOptions.DataSource = dt;
            frmConfig.cbCondenserFanOptions.DisplayMember = "DigitDescription";
            frmConfig.cbCondenserFanOptions.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(43, 1);
                frmConfig.cbCondenserFanOptions.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 45 - Compressor Sound Blankets 

            dt = objMain.GetModelNumDesc(45, "NA", null);
            frmConfig.cbSoundAttenuationPackage.DataSource = dt;
            frmConfig.cbSoundAttenuationPackage.DisplayMember = "DigitDescription";
            frmConfig.cbSoundAttenuationPackage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(44, 1);
                frmConfig.cbSoundAttenuationPackage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 46 - Smoke Detector 

            dt = objMain.GetModelNumDesc(46, "NA", null);
            frmConfig.cbSmokeDetector.DataSource = dt;
            frmConfig.cbSmokeDetector.DisplayMember = "DigitDescription";
            frmConfig.cbSmokeDetector.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(45, 1);
                frmConfig.cbSmokeDetector.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 47 - Hailguards

            dt = objMain.GetModelNumDesc(47, "NA", null);
            frmConfig.cbHailguards.DataSource = dt;
            frmConfig.cbHailguards.DisplayMember = "DigitDescription";
            frmConfig.cbHailguards.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(46, 1);
                frmConfig.cbHailguards.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 48 - Service Lights

            dt = objMain.GetModelNumDesc(48, "NA", null);
            frmConfig.cbServiceLights.DataSource = dt;
            frmConfig.cbServiceLights.DisplayMember = "DigitDescription";
            frmConfig.cbServiceLights.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(47, 1);
                frmConfig.cbServiceLights.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 49 - UV Lights 

            dt = objMain.GetModelNumDesc(49, "NA", null);
            frmConfig.cbUV_Lights.DataSource = dt;
            frmConfig.cbUV_Lights.DisplayMember = "DigitDescription";
            frmConfig.cbUV_Lights.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(48, 1);
                frmConfig.cbUV_Lights.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 51 - Installation

            dt = objMain.GetModelNumDesc(51, "NA", null);
            frmConfig.cbInstallation.DataSource = dt;
            frmConfig.cbInstallation.DisplayMember = "DigitDescription";
            frmConfig.cbInstallation.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(50, 1);
                frmConfig.cbInstallation.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 52 - Convenience Outlet 

            dt = objMain.GetModelNumDesc(52, "NA", null);
            frmConfig.cbConvenienceOutlet.DataSource = dt;
            frmConfig.cbConvenienceOutlet.DisplayMember = "DigitDescription";
            frmConfig.cbConvenienceOutlet.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(51, 1);
                frmConfig.cbConvenienceOutlet.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 53 - Controls Display 

            dt = objMain.GetModelNumDesc(53, "NA", null);
            frmConfig.cbControlsDisplay.DataSource = dt;
            frmConfig.cbControlsDisplay.DisplayMember = "DigitDescription";
            frmConfig.cbControlsDisplay.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(52, 1);
                frmConfig.cbControlsDisplay.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 54 - Reliatel 

            dt = objMain.GetModelNumDesc(54, "NA", null);
            frmConfig.cbCoolingControls.DataSource = dt;
            frmConfig.cbCoolingControls.DisplayMember = "DigitDescription";
            frmConfig.cbCoolingControls.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(53, 1);
                frmConfig.cbCoolingControls.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 55 - Face and Bypass Evap 

            dt = objMain.GetModelNumDesc(55, "NA", null);
            frmConfig.cbFaceAndBypassEvap.DataSource = dt;
            frmConfig.cbFaceAndBypassEvap.DisplayMember = "DigitDescription";
            frmConfig.cbFaceAndBypassEvap.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(54, 1);
                frmConfig.cbFaceAndBypassEvap.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 56 - Thermostat 

            dt = objMain.GetModelNumDesc(56, "NA", null);
            frmConfig.cbThermostat.DataSource = dt;
            frmConfig.cbThermostat.DisplayMember = "DigitDescription";
            frmConfig.cbThermostat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(55, 1);
                frmConfig.cbThermostat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 57 - Altitude 

            dt = objMain.GetModelNumDesc(57, "NA", null);
            frmConfig.cbAltitude.DataSource = dt;
            frmConfig.cbAltitude.DisplayMember = "DigitDescription";
            frmConfig.cbAltitude.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(56, 1);
                frmConfig.cbAltitude.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 58 - Condensate Overflow Switch 

            dt = objMain.GetModelNumDesc(58, "NA", null);
            frmConfig.cbCondensateOverflowSwitch.DataSource = dt;
            frmConfig.cbCondensateOverflowSwitch.DisplayMember = "DigitDescription";
            frmConfig.cbCondensateOverflowSwitch.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(57, 1);
                frmConfig.cbCondensateOverflowSwitch.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 59 - Freezstat 

            dt = objMain.GetModelNumDesc(59, "NA", null);
            frmConfig.cbFreezstat.DataSource = dt;
            frmConfig.cbFreezstat.DisplayMember = "DigitDescription";
            frmConfig.cbFreezstat.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(58, 1);
                frmConfig.cbFreezstat.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 61 - Chilled Water Type

            dt = objMain.GetModelNumDesc(61, "NA", null);
            frmConfig.cbOutdoorCoilFluidType.DataSource = dt;
            frmConfig.cbOutdoorCoilFluidType.DisplayMember = "DigitDescription";
            frmConfig.cbOutdoorCoilFluidType.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(60, 1);
                frmConfig.cbOutdoorCoilFluidType.SelectedIndex = GetRowIndex(dt, digitValStr);
            }

            // Digit 62 - Minumum Damper Leakage

            dt = objMain.GetModelNumDesc(62, "NA", null);
            frmConfig.cbMinimumDamperLeakage.DataSource = dt;
            frmConfig.cbMinimumDamperLeakage.DisplayMember = "DigitDescription";
            frmConfig.cbMinimumDamperLeakage.ValueMember = "DigitVal";
            if (modelNoValuesPresent)
            {
                digitValStr = modelNoStr.Substring(61, 1);
                frmConfig.cbMinimumDamperLeakage.SelectedIndex = GetRowIndex(dt, digitValStr);
            }
           
            frmConfig.ShowDialog();
        }

        public int GetRowIndex(DataTable dt, string digitVal)
        {
            int retRowIdx = 0;

            foreach (DataRow dr in dt.Rows)
            {
                if (digitVal == dr["DigitVal"].ToString())
                {
                    retRowIdx = Int32.Parse(dr["RowIndex"].ToString()) - 1;
                    break;
                }
            }

            if (retRowIdx == -1)
            {
                retRowIdx = 0;
            }

            return retRowIdx;
        }

        #endregion        
    }
}

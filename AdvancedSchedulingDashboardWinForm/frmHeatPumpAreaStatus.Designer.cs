﻿namespace AdvSchedDashWinForm
{
    partial class frmHeatPumpAreaStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHeatPumpAreaStatus));
            this.label26 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDeliveredToLineComplete = new System.Windows.Forms.Button();
            this.btnDeliveredToLineInProgress = new System.Windows.Forms.Button();
            this.btnDeliveredToLineOrdered = new System.Windows.Forms.Button();
            this.btnStorageComplete = new System.Windows.Forms.Button();
            this.btnStorageInProgress = new System.Windows.Forms.Button();
            this.btnStorageOrdered = new System.Windows.Forms.Button();
            this.btnInsulationComplete = new System.Windows.Forms.Button();
            this.btnInsulationInProgress = new System.Windows.Forms.Button();
            this.btnInsulationOrdered = new System.Windows.Forms.Button();
            this.btnPrefabFitBrazComplete = new System.Windows.Forms.Button();
            this.btnPrefabFitBrazInProgress = new System.Windows.Forms.Button();
            this.btnPrefabFitBrazOrdered = new System.Windows.Forms.Button();
            this.btnPartsMadeInHouseComplete = new System.Windows.Forms.Button();
            this.btnPartsMadeInHouseInProgress = new System.Windows.Forms.Button();
            this.btnPartsMadeInHouseOrdered = new System.Windows.Forms.Button();
            this.btnOtherPP_Complete = new System.Windows.Forms.Button();
            this.btnOtherPP_InProgress = new System.Windows.Forms.Button();
            this.btnOtherPP_Ordered = new System.Windows.Forms.Button();
            this.btnCoilsComplete = new System.Windows.Forms.Button();
            this.btnCoilsInProgress = new System.Windows.Forms.Button();
            this.btnCoilsOrdered = new System.Windows.Forms.Button();
            this.lbProdLine = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnMetalComplete = new System.Windows.Forms.Button();
            this.btnMetalInProgress = new System.Windows.Forms.Button();
            this.btnMetalOrdered = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.txtMetalNotes = new System.Windows.Forms.TextBox();
            this.txtCoilsNotes = new System.Windows.Forms.TextBox();
            this.txtPartsMadeInHouseNotes = new System.Windows.Forms.TextBox();
            this.txtOtherPP_Notes = new System.Windows.Forms.TextBox();
            this.txtDeliveredToLineNotes = new System.Windows.Forms.TextBox();
            this.txtStorageNotes = new System.Windows.Forms.TextBox();
            this.txtInsulationNotes = new System.Windows.Forms.TextBox();
            this.txtPrefabFitBrazNotes = new System.Windows.Forms.TextBox();
            this.lbDeliveredToLineCurStatus = new System.Windows.Forms.Label();
            this.lbStorageCurStatus = new System.Windows.Forms.Label();
            this.lbInsulationCurStatus = new System.Windows.Forms.Label();
            this.lbPrefabFitBrazCurStatus = new System.Windows.Forms.Label();
            this.lbPartsMadeInHouseStatus = new System.Windows.Forms.Label();
            this.lbOtherPP_CurStatus = new System.Windows.Forms.Label();
            this.lbCoilsCurStatus = new System.Windows.Forms.Label();
            this.lbMetalCurStatus = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBoxCompLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(348, 3);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(546, 39);
            this.label26.TabIndex = 138;
            this.label26.Text = "Heat Pump Area Status";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(1140, 12);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 137;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDeliveredToLineComplete
            // 
            this.btnDeliveredToLineComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnDeliveredToLineComplete.Enabled = false;
            this.btnDeliveredToLineComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveredToLineComplete.ForeColor = System.Drawing.Color.Black;
            this.btnDeliveredToLineComplete.Location = new System.Drawing.Point(483, 424);
            this.btnDeliveredToLineComplete.Name = "btnDeliveredToLineComplete";
            this.btnDeliveredToLineComplete.Size = new System.Drawing.Size(90, 25);
            this.btnDeliveredToLineComplete.TabIndex = 135;
            this.btnDeliveredToLineComplete.Text = "Complete";
            this.btnDeliveredToLineComplete.UseVisualStyleBackColor = false;
            this.btnDeliveredToLineComplete.Click += new System.EventHandler(this.btnDeliveredToLineComplete_Click);
            // 
            // btnDeliveredToLineInProgress
            // 
            this.btnDeliveredToLineInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnDeliveredToLineInProgress.Enabled = false;
            this.btnDeliveredToLineInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveredToLineInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnDeliveredToLineInProgress.Location = new System.Drawing.Point(358, 424);
            this.btnDeliveredToLineInProgress.Name = "btnDeliveredToLineInProgress";
            this.btnDeliveredToLineInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnDeliveredToLineInProgress.TabIndex = 134;
            this.btnDeliveredToLineInProgress.Text = "In Progress";
            this.btnDeliveredToLineInProgress.UseVisualStyleBackColor = false;
            this.btnDeliveredToLineInProgress.Click += new System.EventHandler(this.btnDeliveredToLineInProgress_Click);
            // 
            // btnDeliveredToLineOrdered
            // 
            this.btnDeliveredToLineOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnDeliveredToLineOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeliveredToLineOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnDeliveredToLineOrdered.Location = new System.Drawing.Point(233, 424);
            this.btnDeliveredToLineOrdered.Name = "btnDeliveredToLineOrdered";
            this.btnDeliveredToLineOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnDeliveredToLineOrdered.TabIndex = 133;
            this.btnDeliveredToLineOrdered.Text = "Ordered";
            this.btnDeliveredToLineOrdered.UseVisualStyleBackColor = false;
            this.btnDeliveredToLineOrdered.Click += new System.EventHandler(this.btnDeliveredToLineOrdered_Click);
            // 
            // btnStorageComplete
            // 
            this.btnStorageComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnStorageComplete.Enabled = false;
            this.btnStorageComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStorageComplete.ForeColor = System.Drawing.Color.Black;
            this.btnStorageComplete.Location = new System.Drawing.Point(483, 384);
            this.btnStorageComplete.Name = "btnStorageComplete";
            this.btnStorageComplete.Size = new System.Drawing.Size(90, 25);
            this.btnStorageComplete.TabIndex = 131;
            this.btnStorageComplete.Text = "Complete";
            this.btnStorageComplete.UseVisualStyleBackColor = false;
            this.btnStorageComplete.Click += new System.EventHandler(this.btnStorageComplete_Click);
            // 
            // btnStorageInProgress
            // 
            this.btnStorageInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnStorageInProgress.Enabled = false;
            this.btnStorageInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStorageInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnStorageInProgress.Location = new System.Drawing.Point(358, 384);
            this.btnStorageInProgress.Name = "btnStorageInProgress";
            this.btnStorageInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnStorageInProgress.TabIndex = 130;
            this.btnStorageInProgress.Text = "In Progress";
            this.btnStorageInProgress.UseVisualStyleBackColor = false;
            this.btnStorageInProgress.Click += new System.EventHandler(this.btnStorageInProgress_Click);
            // 
            // btnStorageOrdered
            // 
            this.btnStorageOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnStorageOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStorageOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnStorageOrdered.Location = new System.Drawing.Point(233, 384);
            this.btnStorageOrdered.Name = "btnStorageOrdered";
            this.btnStorageOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnStorageOrdered.TabIndex = 129;
            this.btnStorageOrdered.Text = "Ordered";
            this.btnStorageOrdered.UseVisualStyleBackColor = false;
            this.btnStorageOrdered.Click += new System.EventHandler(this.btnStorageOrdered_Click);
            // 
            // btnInsulationComplete
            // 
            this.btnInsulationComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnInsulationComplete.Enabled = false;
            this.btnInsulationComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsulationComplete.ForeColor = System.Drawing.Color.Black;
            this.btnInsulationComplete.Location = new System.Drawing.Point(483, 344);
            this.btnInsulationComplete.Name = "btnInsulationComplete";
            this.btnInsulationComplete.Size = new System.Drawing.Size(90, 25);
            this.btnInsulationComplete.TabIndex = 127;
            this.btnInsulationComplete.Text = "Complete";
            this.btnInsulationComplete.UseVisualStyleBackColor = false;
            this.btnInsulationComplete.Click += new System.EventHandler(this.btnInsulationComplete_Click);
            // 
            // btnInsulationInProgress
            // 
            this.btnInsulationInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnInsulationInProgress.Enabled = false;
            this.btnInsulationInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsulationInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnInsulationInProgress.Location = new System.Drawing.Point(358, 344);
            this.btnInsulationInProgress.Name = "btnInsulationInProgress";
            this.btnInsulationInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnInsulationInProgress.TabIndex = 126;
            this.btnInsulationInProgress.Text = "In Progress";
            this.btnInsulationInProgress.UseVisualStyleBackColor = false;
            this.btnInsulationInProgress.Click += new System.EventHandler(this.btnInsulationInProgress_Click);
            // 
            // btnInsulationOrdered
            // 
            this.btnInsulationOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnInsulationOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsulationOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnInsulationOrdered.Location = new System.Drawing.Point(233, 344);
            this.btnInsulationOrdered.Name = "btnInsulationOrdered";
            this.btnInsulationOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnInsulationOrdered.TabIndex = 125;
            this.btnInsulationOrdered.Text = "Ordered";
            this.btnInsulationOrdered.UseVisualStyleBackColor = false;
            this.btnInsulationOrdered.Click += new System.EventHandler(this.btnInsulationOrdered_Click);
            // 
            // btnPrefabFitBrazComplete
            // 
            this.btnPrefabFitBrazComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPrefabFitBrazComplete.Enabled = false;
            this.btnPrefabFitBrazComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrefabFitBrazComplete.ForeColor = System.Drawing.Color.Black;
            this.btnPrefabFitBrazComplete.Location = new System.Drawing.Point(483, 304);
            this.btnPrefabFitBrazComplete.Name = "btnPrefabFitBrazComplete";
            this.btnPrefabFitBrazComplete.Size = new System.Drawing.Size(90, 25);
            this.btnPrefabFitBrazComplete.TabIndex = 123;
            this.btnPrefabFitBrazComplete.Text = "Complete";
            this.btnPrefabFitBrazComplete.UseVisualStyleBackColor = false;
            this.btnPrefabFitBrazComplete.Click += new System.EventHandler(this.btnPrefabFitBrazComplete_Click);
            // 
            // btnPrefabFitBrazInProgress
            // 
            this.btnPrefabFitBrazInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPrefabFitBrazInProgress.Enabled = false;
            this.btnPrefabFitBrazInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrefabFitBrazInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnPrefabFitBrazInProgress.Location = new System.Drawing.Point(358, 304);
            this.btnPrefabFitBrazInProgress.Name = "btnPrefabFitBrazInProgress";
            this.btnPrefabFitBrazInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnPrefabFitBrazInProgress.TabIndex = 122;
            this.btnPrefabFitBrazInProgress.Text = "In Progress";
            this.btnPrefabFitBrazInProgress.UseVisualStyleBackColor = false;
            this.btnPrefabFitBrazInProgress.Click += new System.EventHandler(this.btnPrefabFitBrazInProgress_Click);
            // 
            // btnPrefabFitBrazOrdered
            // 
            this.btnPrefabFitBrazOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnPrefabFitBrazOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrefabFitBrazOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnPrefabFitBrazOrdered.Location = new System.Drawing.Point(233, 304);
            this.btnPrefabFitBrazOrdered.Name = "btnPrefabFitBrazOrdered";
            this.btnPrefabFitBrazOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnPrefabFitBrazOrdered.TabIndex = 121;
            this.btnPrefabFitBrazOrdered.Text = "Ordered";
            this.btnPrefabFitBrazOrdered.UseVisualStyleBackColor = false;
            this.btnPrefabFitBrazOrdered.Click += new System.EventHandler(this.btnPrefabFitBrazOrdered_Click);
            // 
            // btnPartsMadeInHouseComplete
            // 
            this.btnPartsMadeInHouseComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPartsMadeInHouseComplete.Enabled = false;
            this.btnPartsMadeInHouseComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartsMadeInHouseComplete.ForeColor = System.Drawing.Color.Black;
            this.btnPartsMadeInHouseComplete.Location = new System.Drawing.Point(483, 264);
            this.btnPartsMadeInHouseComplete.Name = "btnPartsMadeInHouseComplete";
            this.btnPartsMadeInHouseComplete.Size = new System.Drawing.Size(90, 25);
            this.btnPartsMadeInHouseComplete.TabIndex = 119;
            this.btnPartsMadeInHouseComplete.Text = "Complete";
            this.btnPartsMadeInHouseComplete.UseVisualStyleBackColor = false;
            this.btnPartsMadeInHouseComplete.Click += new System.EventHandler(this.btnPartsMadeInHouseComplete_Click);
            // 
            // btnPartsMadeInHouseInProgress
            // 
            this.btnPartsMadeInHouseInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnPartsMadeInHouseInProgress.Enabled = false;
            this.btnPartsMadeInHouseInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartsMadeInHouseInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnPartsMadeInHouseInProgress.Location = new System.Drawing.Point(358, 264);
            this.btnPartsMadeInHouseInProgress.Name = "btnPartsMadeInHouseInProgress";
            this.btnPartsMadeInHouseInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnPartsMadeInHouseInProgress.TabIndex = 118;
            this.btnPartsMadeInHouseInProgress.Text = "In Progress";
            this.btnPartsMadeInHouseInProgress.UseVisualStyleBackColor = false;
            this.btnPartsMadeInHouseInProgress.Click += new System.EventHandler(this.btnPartsMadeInHouseInProgress_Click);
            // 
            // btnPartsMadeInHouseOrdered
            // 
            this.btnPartsMadeInHouseOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnPartsMadeInHouseOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartsMadeInHouseOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnPartsMadeInHouseOrdered.Location = new System.Drawing.Point(233, 264);
            this.btnPartsMadeInHouseOrdered.Name = "btnPartsMadeInHouseOrdered";
            this.btnPartsMadeInHouseOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnPartsMadeInHouseOrdered.TabIndex = 117;
            this.btnPartsMadeInHouseOrdered.Text = "Ordered";
            this.btnPartsMadeInHouseOrdered.UseVisualStyleBackColor = false;
            this.btnPartsMadeInHouseOrdered.Click += new System.EventHandler(this.btnPartsMadeInHouseOrdered_Click);
            // 
            // btnOtherPP_Complete
            // 
            this.btnOtherPP_Complete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnOtherPP_Complete.Enabled = false;
            this.btnOtherPP_Complete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtherPP_Complete.ForeColor = System.Drawing.Color.Black;
            this.btnOtherPP_Complete.Location = new System.Drawing.Point(483, 224);
            this.btnOtherPP_Complete.Name = "btnOtherPP_Complete";
            this.btnOtherPP_Complete.Size = new System.Drawing.Size(90, 25);
            this.btnOtherPP_Complete.TabIndex = 115;
            this.btnOtherPP_Complete.Text = "Complete";
            this.btnOtherPP_Complete.UseVisualStyleBackColor = false;
            this.btnOtherPP_Complete.Click += new System.EventHandler(this.btnOther_PPComplete_Click);
            // 
            // btnOtherPP_InProgress
            // 
            this.btnOtherPP_InProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnOtherPP_InProgress.Enabled = false;
            this.btnOtherPP_InProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtherPP_InProgress.ForeColor = System.Drawing.Color.Black;
            this.btnOtherPP_InProgress.Location = new System.Drawing.Point(358, 224);
            this.btnOtherPP_InProgress.Name = "btnOtherPP_InProgress";
            this.btnOtherPP_InProgress.Size = new System.Drawing.Size(90, 25);
            this.btnOtherPP_InProgress.TabIndex = 114;
            this.btnOtherPP_InProgress.Text = "In Progress";
            this.btnOtherPP_InProgress.UseVisualStyleBackColor = false;
            this.btnOtherPP_InProgress.Click += new System.EventHandler(this.btnOtherPP_InProgress_Click);
            // 
            // btnOtherPP_Ordered
            // 
            this.btnOtherPP_Ordered.BackColor = System.Drawing.Color.Lime;
            this.btnOtherPP_Ordered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtherPP_Ordered.ForeColor = System.Drawing.Color.Black;
            this.btnOtherPP_Ordered.Location = new System.Drawing.Point(233, 224);
            this.btnOtherPP_Ordered.Name = "btnOtherPP_Ordered";
            this.btnOtherPP_Ordered.Size = new System.Drawing.Size(90, 25);
            this.btnOtherPP_Ordered.TabIndex = 113;
            this.btnOtherPP_Ordered.Text = "Ordered";
            this.btnOtherPP_Ordered.UseVisualStyleBackColor = false;
            this.btnOtherPP_Ordered.Click += new System.EventHandler(this.btnOtherPP_Ordered_Click);
            // 
            // btnCoilsComplete
            // 
            this.btnCoilsComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCoilsComplete.Enabled = false;
            this.btnCoilsComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoilsComplete.ForeColor = System.Drawing.Color.Black;
            this.btnCoilsComplete.Location = new System.Drawing.Point(483, 184);
            this.btnCoilsComplete.Name = "btnCoilsComplete";
            this.btnCoilsComplete.Size = new System.Drawing.Size(90, 25);
            this.btnCoilsComplete.TabIndex = 111;
            this.btnCoilsComplete.Text = "Complete";
            this.btnCoilsComplete.UseVisualStyleBackColor = false;
            this.btnCoilsComplete.Click += new System.EventHandler(this.btnCoilsComplete_Click);
            // 
            // btnCoilsInProgress
            // 
            this.btnCoilsInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCoilsInProgress.Enabled = false;
            this.btnCoilsInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoilsInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnCoilsInProgress.Location = new System.Drawing.Point(358, 184);
            this.btnCoilsInProgress.Name = "btnCoilsInProgress";
            this.btnCoilsInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnCoilsInProgress.TabIndex = 110;
            this.btnCoilsInProgress.Text = "In Progress";
            this.btnCoilsInProgress.UseVisualStyleBackColor = false;
            this.btnCoilsInProgress.Click += new System.EventHandler(this.btnCoilsInProgress_Click);
            // 
            // btnCoilsOrdered
            // 
            this.btnCoilsOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnCoilsOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCoilsOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnCoilsOrdered.Location = new System.Drawing.Point(233, 184);
            this.btnCoilsOrdered.Name = "btnCoilsOrdered";
            this.btnCoilsOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnCoilsOrdered.TabIndex = 109;
            this.btnCoilsOrdered.Text = "Ordered";
            this.btnCoilsOrdered.UseVisualStyleBackColor = false;
            this.btnCoilsOrdered.Click += new System.EventHandler(this.btnCoilsOrdered_Click);
            // 
            // lbProdLine
            // 
            this.lbProdLine.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProdLine.Location = new System.Drawing.Point(193, 96);
            this.lbProdLine.Name = "lbProdLine";
            this.lbProdLine.Size = new System.Drawing.Size(161, 33);
            this.lbProdLine.TabIndex = 108;
            this.lbProdLine.Text = "Prod Line";
            this.lbProdLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(16, 96);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(184, 33);
            this.label14.TabIndex = 107;
            this.label14.Text = "Production Line:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(614, 117);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 23);
            this.label13.TabIndex = 106;
            this.label13.Text = "Current Status ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnMetalComplete
            // 
            this.btnMetalComplete.BackColor = System.Drawing.Color.Gainsboro;
            this.btnMetalComplete.Enabled = false;
            this.btnMetalComplete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMetalComplete.ForeColor = System.Drawing.Color.Black;
            this.btnMetalComplete.Location = new System.Drawing.Point(483, 144);
            this.btnMetalComplete.Name = "btnMetalComplete";
            this.btnMetalComplete.Size = new System.Drawing.Size(90, 25);
            this.btnMetalComplete.TabIndex = 104;
            this.btnMetalComplete.Text = "Complete";
            this.btnMetalComplete.UseVisualStyleBackColor = false;
            this.btnMetalComplete.Click += new System.EventHandler(this.btnMetalComplete_Click);
            // 
            // btnMetalInProgress
            // 
            this.btnMetalInProgress.BackColor = System.Drawing.Color.Gainsboro;
            this.btnMetalInProgress.Enabled = false;
            this.btnMetalInProgress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMetalInProgress.ForeColor = System.Drawing.Color.Black;
            this.btnMetalInProgress.Location = new System.Drawing.Point(358, 144);
            this.btnMetalInProgress.Name = "btnMetalInProgress";
            this.btnMetalInProgress.Size = new System.Drawing.Size(90, 25);
            this.btnMetalInProgress.TabIndex = 103;
            this.btnMetalInProgress.Text = "In Progress";
            this.btnMetalInProgress.UseVisualStyleBackColor = false;
            this.btnMetalInProgress.Click += new System.EventHandler(this.btnMetalInProgress_Click);
            // 
            // btnMetalOrdered
            // 
            this.btnMetalOrdered.BackColor = System.Drawing.Color.Lime;
            this.btnMetalOrdered.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMetalOrdered.ForeColor = System.Drawing.Color.Black;
            this.btnMetalOrdered.Location = new System.Drawing.Point(233, 144);
            this.btnMetalOrdered.Name = "btnMetalOrdered";
            this.btnMetalOrdered.Size = new System.Drawing.Size(90, 25);
            this.btnMetalOrdered.TabIndex = 102;
            this.btnMetalOrdered.Text = "Ordered";
            this.btnMetalOrdered.UseVisualStyleBackColor = false;
            this.btnMetalOrdered.Click += new System.EventHandler(this.btnMetalOrdered_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(15, 424);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 22);
            this.label5.TabIndex = 101;
            this.label5.Text = "Delivered To Line:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(15, 384);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 22);
            this.label6.TabIndex = 100;
            this.label6.Text = "Storage:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(15, 344);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 22);
            this.label7.TabIndex = 99;
            this.label7.Text = "Insulation:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(15, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(210, 22);
            this.label8.TabIndex = 98;
            this.label8.Text = "Prefab / Fittings / Brazing:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(15, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 22);
            this.label3.TabIndex = 97;
            this.label3.Text = "Parts  Made In House:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(15, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 22);
            this.label4.TabIndex = 96;
            this.label4.Text = "Other Purchase Parts:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(15, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 22);
            this.label2.TabIndex = 95;
            this.label2.Text = "Coils:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(15, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 22);
            this.label1.TabIndex = 94;
            this.label1.Text = "Metal:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbJobNum.Location = new System.Drawing.Point(502, 44);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(236, 39);
            this.lbJobNum.TabIndex = 93;
            this.lbJobNum.Text = "Job Number";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMetalNotes
            // 
            this.txtMetalNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMetalNotes.Location = new System.Drawing.Point(826, 144);
            this.txtMetalNotes.Multiline = true;
            this.txtMetalNotes.Name = "txtMetalNotes";
            this.txtMetalNotes.Size = new System.Drawing.Size(389, 25);
            this.txtMetalNotes.TabIndex = 139;
            this.txtMetalNotes.Leave += new System.EventHandler(this.txtMetalNotes_Leave);
            // 
            // txtCoilsNotes
            // 
            this.txtCoilsNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCoilsNotes.Location = new System.Drawing.Point(826, 184);
            this.txtCoilsNotes.Multiline = true;
            this.txtCoilsNotes.Name = "txtCoilsNotes";
            this.txtCoilsNotes.Size = new System.Drawing.Size(389, 25);
            this.txtCoilsNotes.TabIndex = 140;
            this.txtCoilsNotes.Leave += new System.EventHandler(this.txtCoilsNotes_Leave);
            // 
            // txtPartsMadeInHouseNotes
            // 
            this.txtPartsMadeInHouseNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPartsMadeInHouseNotes.Location = new System.Drawing.Point(826, 264);
            this.txtPartsMadeInHouseNotes.Multiline = true;
            this.txtPartsMadeInHouseNotes.Name = "txtPartsMadeInHouseNotes";
            this.txtPartsMadeInHouseNotes.Size = new System.Drawing.Size(389, 25);
            this.txtPartsMadeInHouseNotes.TabIndex = 142;
            this.txtPartsMadeInHouseNotes.Leave += new System.EventHandler(this.txtPartsMadeInHouseNotes_Leave);
            // 
            // txtOtherPP_Notes
            // 
            this.txtOtherPP_Notes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtherPP_Notes.Location = new System.Drawing.Point(826, 224);
            this.txtOtherPP_Notes.Multiline = true;
            this.txtOtherPP_Notes.Name = "txtOtherPP_Notes";
            this.txtOtherPP_Notes.Size = new System.Drawing.Size(389, 25);
            this.txtOtherPP_Notes.TabIndex = 141;
            this.txtOtherPP_Notes.Leave += new System.EventHandler(this.txtOtherPP_Notes_Leave);
            // 
            // txtDeliveredToLineNotes
            // 
            this.txtDeliveredToLineNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeliveredToLineNotes.Location = new System.Drawing.Point(826, 424);
            this.txtDeliveredToLineNotes.Multiline = true;
            this.txtDeliveredToLineNotes.Name = "txtDeliveredToLineNotes";
            this.txtDeliveredToLineNotes.Size = new System.Drawing.Size(389, 25);
            this.txtDeliveredToLineNotes.TabIndex = 146;
            this.txtDeliveredToLineNotes.Leave += new System.EventHandler(this.txtDeliveredToLineNotes_Leave);
            // 
            // txtStorageNotes
            // 
            this.txtStorageNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStorageNotes.Location = new System.Drawing.Point(826, 384);
            this.txtStorageNotes.Multiline = true;
            this.txtStorageNotes.Name = "txtStorageNotes";
            this.txtStorageNotes.Size = new System.Drawing.Size(389, 25);
            this.txtStorageNotes.TabIndex = 145;
            this.txtStorageNotes.Leave += new System.EventHandler(this.txtStorageNotes_Leave);
            // 
            // txtInsulationNotes
            // 
            this.txtInsulationNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInsulationNotes.Location = new System.Drawing.Point(826, 344);
            this.txtInsulationNotes.Multiline = true;
            this.txtInsulationNotes.Name = "txtInsulationNotes";
            this.txtInsulationNotes.Size = new System.Drawing.Size(389, 25);
            this.txtInsulationNotes.TabIndex = 144;
            this.txtInsulationNotes.Leave += new System.EventHandler(this.txtInsulationNotes_Leave);
            // 
            // txtPrefabFitBrazNotes
            // 
            this.txtPrefabFitBrazNotes.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrefabFitBrazNotes.Location = new System.Drawing.Point(826, 304);
            this.txtPrefabFitBrazNotes.Multiline = true;
            this.txtPrefabFitBrazNotes.Name = "txtPrefabFitBrazNotes";
            this.txtPrefabFitBrazNotes.Size = new System.Drawing.Size(389, 25);
            this.txtPrefabFitBrazNotes.TabIndex = 143;
            this.txtPrefabFitBrazNotes.Leave += new System.EventHandler(this.txtPrefabFitBrazNotes_Leave);
            // 
            // lbDeliveredToLineCurStatus
            // 
            this.lbDeliveredToLineCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbDeliveredToLineCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbDeliveredToLineCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDeliveredToLineCurStatus.Location = new System.Drawing.Point(608, 424);
            this.lbDeliveredToLineCurStatus.Name = "lbDeliveredToLineCurStatus";
            this.lbDeliveredToLineCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbDeliveredToLineCurStatus.TabIndex = 154;
            this.lbDeliveredToLineCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbStorageCurStatus
            // 
            this.lbStorageCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbStorageCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbStorageCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStorageCurStatus.Location = new System.Drawing.Point(608, 384);
            this.lbStorageCurStatus.Name = "lbStorageCurStatus";
            this.lbStorageCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbStorageCurStatus.TabIndex = 153;
            this.lbStorageCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbInsulationCurStatus
            // 
            this.lbInsulationCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbInsulationCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbInsulationCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInsulationCurStatus.Location = new System.Drawing.Point(608, 344);
            this.lbInsulationCurStatus.Name = "lbInsulationCurStatus";
            this.lbInsulationCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbInsulationCurStatus.TabIndex = 152;
            this.lbInsulationCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbPrefabFitBrazCurStatus
            // 
            this.lbPrefabFitBrazCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbPrefabFitBrazCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPrefabFitBrazCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrefabFitBrazCurStatus.Location = new System.Drawing.Point(608, 304);
            this.lbPrefabFitBrazCurStatus.Name = "lbPrefabFitBrazCurStatus";
            this.lbPrefabFitBrazCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbPrefabFitBrazCurStatus.TabIndex = 151;
            this.lbPrefabFitBrazCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbPartsMadeInHouseStatus
            // 
            this.lbPartsMadeInHouseStatus.BackColor = System.Drawing.Color.Silver;
            this.lbPartsMadeInHouseStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbPartsMadeInHouseStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPartsMadeInHouseStatus.Location = new System.Drawing.Point(608, 264);
            this.lbPartsMadeInHouseStatus.Name = "lbPartsMadeInHouseStatus";
            this.lbPartsMadeInHouseStatus.Size = new System.Drawing.Size(212, 23);
            this.lbPartsMadeInHouseStatus.TabIndex = 150;
            this.lbPartsMadeInHouseStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOtherPP_CurStatus
            // 
            this.lbOtherPP_CurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbOtherPP_CurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbOtherPP_CurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOtherPP_CurStatus.Location = new System.Drawing.Point(608, 224);
            this.lbOtherPP_CurStatus.Name = "lbOtherPP_CurStatus";
            this.lbOtherPP_CurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbOtherPP_CurStatus.TabIndex = 149;
            this.lbOtherPP_CurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCoilsCurStatus
            // 
            this.lbCoilsCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbCoilsCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCoilsCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCoilsCurStatus.Location = new System.Drawing.Point(608, 184);
            this.lbCoilsCurStatus.Name = "lbCoilsCurStatus";
            this.lbCoilsCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbCoilsCurStatus.TabIndex = 148;
            this.lbCoilsCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMetalCurStatus
            // 
            this.lbMetalCurStatus.BackColor = System.Drawing.Color.Silver;
            this.lbMetalCurStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbMetalCurStatus.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMetalCurStatus.Location = new System.Drawing.Point(608, 144);
            this.lbMetalCurStatus.Name = "lbMetalCurStatus";
            this.lbMetalCurStatus.Size = new System.Drawing.Size(212, 23);
            this.lbMetalCurStatus.TabIndex = 147;
            this.lbMetalCurStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(832, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 23);
            this.label9.TabIndex = 155;
            this.label9.Text = "Notes";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxCompLogo
            // 
            this.pictureBoxCompLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCompLogo.Image")));
            this.pictureBoxCompLogo.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCompLogo.Name = "pictureBoxCompLogo";
            this.pictureBoxCompLogo.Size = new System.Drawing.Size(300, 85);
            this.pictureBoxCompLogo.TabIndex = 156;
            this.pictureBoxCompLogo.TabStop = false;
            // 
            // frmHeatPumpAreaStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 472);
            this.Controls.Add(this.pictureBoxCompLogo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lbDeliveredToLineCurStatus);
            this.Controls.Add(this.lbStorageCurStatus);
            this.Controls.Add(this.lbInsulationCurStatus);
            this.Controls.Add(this.lbPrefabFitBrazCurStatus);
            this.Controls.Add(this.lbPartsMadeInHouseStatus);
            this.Controls.Add(this.lbOtherPP_CurStatus);
            this.Controls.Add(this.lbCoilsCurStatus);
            this.Controls.Add(this.lbMetalCurStatus);
            this.Controls.Add(this.txtDeliveredToLineNotes);
            this.Controls.Add(this.txtStorageNotes);
            this.Controls.Add(this.txtInsulationNotes);
            this.Controls.Add(this.txtPrefabFitBrazNotes);
            this.Controls.Add(this.txtPartsMadeInHouseNotes);
            this.Controls.Add(this.txtOtherPP_Notes);
            this.Controls.Add(this.txtCoilsNotes);
            this.Controls.Add(this.txtMetalNotes);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDeliveredToLineComplete);
            this.Controls.Add(this.btnDeliveredToLineInProgress);
            this.Controls.Add(this.btnDeliveredToLineOrdered);
            this.Controls.Add(this.btnStorageComplete);
            this.Controls.Add(this.btnStorageInProgress);
            this.Controls.Add(this.btnStorageOrdered);
            this.Controls.Add(this.btnInsulationComplete);
            this.Controls.Add(this.btnInsulationInProgress);
            this.Controls.Add(this.btnInsulationOrdered);
            this.Controls.Add(this.btnPrefabFitBrazComplete);
            this.Controls.Add(this.btnPrefabFitBrazInProgress);
            this.Controls.Add(this.btnPrefabFitBrazOrdered);
            this.Controls.Add(this.btnPartsMadeInHouseComplete);
            this.Controls.Add(this.btnPartsMadeInHouseInProgress);
            this.Controls.Add(this.btnPartsMadeInHouseOrdered);
            this.Controls.Add(this.btnOtherPP_Complete);
            this.Controls.Add(this.btnOtherPP_InProgress);
            this.Controls.Add(this.btnOtherPP_Ordered);
            this.Controls.Add(this.btnCoilsComplete);
            this.Controls.Add(this.btnCoilsInProgress);
            this.Controls.Add(this.btnCoilsOrdered);
            this.Controls.Add(this.lbProdLine);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnMetalComplete);
            this.Controls.Add(this.btnMetalInProgress);
            this.Controls.Add(this.btnMetalOrdered);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbJobNum);
            this.Name = "frmHeatPumpAreaStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmHeatPumpAreaStatus";
            this.Load += new System.EventHandler(this.frmHeatPumpAreaStatus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.Button btnDeliveredToLineComplete;
        public System.Windows.Forms.Button btnDeliveredToLineInProgress;
        public System.Windows.Forms.Button btnDeliveredToLineOrdered;
        public System.Windows.Forms.Button btnStorageComplete;
        public System.Windows.Forms.Button btnStorageInProgress;
        public System.Windows.Forms.Button btnStorageOrdered;
        public System.Windows.Forms.Button btnInsulationComplete;
        public System.Windows.Forms.Button btnInsulationInProgress;
        public System.Windows.Forms.Button btnInsulationOrdered;
        public System.Windows.Forms.Button btnPrefabFitBrazComplete;
        public System.Windows.Forms.Button btnPrefabFitBrazInProgress;
        public System.Windows.Forms.Button btnPrefabFitBrazOrdered;
        public System.Windows.Forms.Button btnPartsMadeInHouseComplete;
        public System.Windows.Forms.Button btnPartsMadeInHouseInProgress;
        public System.Windows.Forms.Button btnPartsMadeInHouseOrdered;
        public System.Windows.Forms.Button btnOtherPP_Complete;
        public System.Windows.Forms.Button btnOtherPP_InProgress;
        public System.Windows.Forms.Button btnOtherPP_Ordered;
        public System.Windows.Forms.Button btnCoilsComplete;
        public System.Windows.Forms.Button btnCoilsInProgress;
        public System.Windows.Forms.Button btnCoilsOrdered;
        private System.Windows.Forms.Label lbProdLine;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Button btnMetalComplete;
        public System.Windows.Forms.Button btnMetalInProgress;
        public System.Windows.Forms.Button btnMetalOrdered;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lbJobNum;
        public System.Windows.Forms.TextBox txtMetalNotes;
        public System.Windows.Forms.TextBox txtCoilsNotes;
        public System.Windows.Forms.TextBox txtPartsMadeInHouseNotes;
        public System.Windows.Forms.TextBox txtOtherPP_Notes;
        public System.Windows.Forms.TextBox txtDeliveredToLineNotes;
        public System.Windows.Forms.TextBox txtStorageNotes;
        public System.Windows.Forms.TextBox txtInsulationNotes;
        public System.Windows.Forms.TextBox txtPrefabFitBrazNotes;
        public System.Windows.Forms.Label lbDeliveredToLineCurStatus;
        public System.Windows.Forms.Label lbStorageCurStatus;
        public System.Windows.Forms.Label lbInsulationCurStatus;
        public System.Windows.Forms.Label lbPrefabFitBrazCurStatus;
        public System.Windows.Forms.Label lbPartsMadeInHouseStatus;
        public System.Windows.Forms.Label lbOtherPP_CurStatus;
        public System.Windows.Forms.Label lbCoilsCurStatus;
        public System.Windows.Forms.Label lbMetalCurStatus;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBoxCompLogo;
    }
}
# Advanced Scheduling Dashboard

This windows forms application breaks down the schedule of units that require an auxiliary  box, so the assembly team knows when they need to build which auxiliary box types and in what priority order.

## Project Structure

### AdvSchedDashWinForm
This project is a .NET Framework 4.5 windows application project.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures. The following stored procedures are used:

* AdvSchd_GetHeatPumpJobs
* AdvSchd_GetAuxBoxJobs
* AdvSchd_GetPolyIsoJobs
* AdvSchd_Get65SCCRJobs
* AdvSchd_GetSubAssemblyList
* R6_OA_GetLineSizeList
* R6_OA_GetRev5ModelNoDigitDesc
* R6_OA_GetModelNumDesc
* AdvSchd_GetSubAssemblyStatusByJob
* AdvSchd_GetSubAssemblyHistoryByJob
* AdvSchd_InsertSubAssemblyHistory
* AdvSchd_InsertSubAssemblyStatus
* AdvSchd_UpdateSubAssemblyStatus
* AdvSchd_GetHeatPumpAreaStatusByJob
* AdvSchd_InsertHeatPumpStatus
* AdvSchd_UpdateHeatPumpStatus
* GetOAU_ModelNoValues

## Reports
The following Crystal Reports are used:

* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\R6_OA_RefrigerationComp.v2.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\CriticalCompBOM.rpt

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection strings in frmMain.cs #if Debug sections to point to the appropriate local or dev KCC database.
Make sure Crystal Reports for Visual Studio is installed, and fix all broken references to point to the installed Crystal Reports .dlls. Disable manifest signing. Build the solution and run it. 

## How to deploy
One-click publish: [https://docs.microsoft.com/en-us/visualstudio/deployment/how-to-publish-a-clickonce-application-using-the-publish-wizard?view=vs-2019]

## Author/Devs
Tony Thoman